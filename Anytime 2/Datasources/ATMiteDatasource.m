//
//  ATMiteDatasource.h
//  anytime
//
//  Created by Josef Materi on 12.09.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATMiteDatasource.h"
#import "MiteAPIClient.h"
#import "MiteDateTransformers.h"
#import "MiteTimeEntry.h"
#import "MiteProject.h"
#import "MiteService.h"
#import "NSDate+Additions.h"
#import "NSURL+Query.h"
#import <RestKit.h>
#import "ATCategoryConfiguration.h"
#import "UIImage+Tint.h"

@implementation ATMiteDatasource

+ (instancetype)sharedDatasource {
    
    static dispatch_once_t once;
    static ATMiteDatasource *singleton;
    dispatch_once(&once, ^ {
        singleton = [[ATMiteDatasource alloc] initWithHTTPClient:[MiteAPIClient sharedClient]];
    });
    return singleton;
    
}

- (instancetype) initWithHTTPClient:(AFHTTPClient *)client {
    self = [super initWithHTTPClient:client];
    if (self) {
          
        NSError *error = nil;
        
        NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"anytime" withExtension:@"momd"];
        
        NSManagedObjectModel *managedObjectModel =  [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
        
        RKManagedObjectStore *managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
        
        BOOL success = RKEnsureDirectoryExistsAtPath(RKApplicationDataDirectory(), &error);
        
        if (! success) {
            RKLogError(@"Failed to create Application Data Directory at path '%@': %@", RKApplicationDataDirectory(), error);
        }
        
        NSString *path = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"anytime.sqlite"];
        
        NSPersistentStore *persistentStore = [managedObjectStore addSQLitePersistentStoreAtPath:path fromSeedDatabaseAtPath:nil withConfiguration:nil options:nil error:&error];
        
        if (! persistentStore) {
            RKLogError(@"Failed adding persistent store at path '%@': %@", path, error);
        }
        [managedObjectStore createManagedObjectContexts];
        
        
        self.managedObjectStore = managedObjectStore;
        
        
        
        //
        //    RKObjectMapping *errorMapping = [RKObjectMapping mappingForClass:[RKErrorMessage class]];
        //    // The entire value at the source key path containing the errors maps to the message
        //    [errorMapping addPropertyMapping:[RKAttributeMapping attributeMappingFromKeyPath:nil toKeyPath:@"time_entry"]];
        //
        //    statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassClientError);
        //    // Any response in the 4xx status code range with an "errors" key path uses this mapping
        //
        //    RKResponseDescriptor *errorDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodAny pathPattern:nil keyPath:@"errors" statusCodes:statusCodes];
        
        [self configureTimeEntryMapping];
        
        [self configureProjectMapping];
        [self configureServiceMapping];
        
    }
    
    return self;
}

- (void)configureTimeEntryMapping {
    RKEntityMapping *mapping = [RKEntityMapping mappingForEntityForName:@"MiteTimeEntry" inManagedObjectStore:self.managedObjectStore];
    
    mapping.identificationAttributes = @[@"remote_id"];
    
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"service_name" : @"service_name",
                                                  @"project_name": @"project_name",
                                                  @"service_id": @"service_id",
                                                  @"project_id":@"project_id",
                                                  @"updated_at":@"updated_at",
                                                  @"created_at":@"created_at",
                                                  @"id":@"remote_id",
                                                  @"minutes":@"minutes",
                                                  @"date_at": @"date_at",
                                                  @"tracking.minutes": @"trackingMinutes",
                                                  @"tracking.since": @"trackingSince"
                                                  }];
    
    [mapping setSetDefaultValueForMissingAttributes:YES];
    [mapping setNilForMissingRelationships];
    
    NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful); // Anything in 2xx
    
    RKResponseDescriptor *articleDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodAny pathPattern:@"/time_entries.json" keyPath:@"time_entry" statusCodes:statusCodes];
    
    RKResponseDescriptor *singleTimeEntryDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodGET pathPattern:@"/time_entries/:id" keyPath:@"time_entry" statusCodes:statusCodes];
    
    
    RKResponseDescriptor *createResponse = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodPOST pathPattern:@"/time_entries" keyPath:@"time_entry" statusCodes:statusCodes];
    
    
    RKRequestDescriptor * requestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[mapping inverseMapping] objectClass:[MiteTimeEntry class] rootKeyPath:@"time_entry" method:RKRequestMethodFromString(@"POST")];
    
    [self addRequestDescriptor:requestDescriptor];
    
    [self addResponseDescriptorsFromArray:@[ articleDescriptor, singleTimeEntryDescriptor, createResponse ]];
    
    __weak ATMiteDatasource *weakSelf = self;
    
    [self addFetchRequestBlock:^NSFetchRequest *(NSURL *URL) {
        
        __strong ATMiteDatasource *strongSelf = weakSelf;
        
        RKPathMatcher *pathMatcher = [RKPathMatcher pathMatcherWithPattern:@"/time_entries.json"];
        
        NSDictionary *argsDict = nil;
        
        BOOL match = [pathMatcher matchesPath:[URL relativePath] tokenizeQueryStrings:YES parsedArguments:&argsDict];
        
        NSString *date_string;
        
        NSDictionary *query = [URL queryDictionary];
        
        if (match) {
            
            date_string = [query objectForKey:@"at"];
            
            NSDate *startDate = [[NSValueTransformer valueTransformerForName:MiteDateTransformerDateOnly] transformedValue:date_string];
            NSDate *endDate = [startDate dateByAddingDays:1];
            
            return [strongSelf fetchRequestForTimeEntriesWithStartDate:startDate endDate:endDate];
        }
        
        return nil;
    }];

}

- (void)configureProjectMapping {
    
    RKEntityMapping *mapping = [RKEntityMapping mappingForEntityForName:@"MiteProject" inManagedObjectStore:self.managedObjectStore];
    
    mapping.identificationAttributes = @[@"remote_id"];
    
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"name": @"name",
                                                  @"updated_at":@"updated_at",
                                                  @"created_at":@"created_at",
                                                  @"id":@"remote_id",
                                                  }];
    
    [mapping setSetDefaultValueForMissingAttributes:YES];
    [mapping setNilForMissingRelationships];
    
    NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful); // Anything in 2xx
    
    RKResponseDescriptor *articleDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodAny pathPattern:@"/projects.json" keyPath:@"project" statusCodes:statusCodes];
    
    RKResponseDescriptor *singleTimeEntryDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodGET pathPattern:@"/projects/:id" keyPath:@"project" statusCodes:statusCodes];
    
    
    RKResponseDescriptor *createResponse = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodPOST pathPattern:@"/projects" keyPath:@"project" statusCodes:statusCodes];
    
    
    RKRequestDescriptor * requestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[mapping inverseMapping] objectClass:[MiteProject class] rootKeyPath:@"project" method:RKRequestMethodFromString(@"POST")];
    
    [self addRequestDescriptor:requestDescriptor];
    
    [self addResponseDescriptorsFromArray:@[ articleDescriptor, singleTimeEntryDescriptor, createResponse ]];
    
    __weak ATMiteDatasource *weakSelf = self;
    
   [self addFetchRequestBlock:^NSFetchRequest *(NSURL *URL) {
       
       __strong ATMiteDatasource *strongSelf = weakSelf;
       
       RKPathMatcher *pathMatcher = [RKPathMatcher pathMatcherWithPattern:@"/projects.json"];
       
       NSDictionary *argsDict = nil;
       
       BOOL match = [pathMatcher matchesPath:[URL relativePath] tokenizeQueryStrings:YES parsedArguments:&argsDict];
       
       
       if (match) {
           NSLog(@"Match!!!!!");
           return [strongSelf fetchRequestForProjects];
       } else {
           NSLog(@"No match!!!!");
       }
       
       return nil;
   }];
    
}

- (void)configureServiceMapping {
    
    RKEntityMapping *mapping = [RKEntityMapping mappingForEntityForName:@"MiteService" inManagedObjectStore:self.managedObjectStore];
    
    mapping.identificationAttributes = @[@"remote_id"];
    
    [mapping addAttributeMappingsFromDictionary:@{
                                                  @"name": @"name",
                                                  @"updated_at":@"updated_at",
                                                  @"created_at":@"created_at",
                                                  @"id":@"remote_id",
                                                  }];
    
    [mapping setSetDefaultValueForMissingAttributes:YES];
    [mapping setNilForMissingRelationships];
    
    NSIndexSet *statusCodes = RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful); // Anything in 2xx
    
    RKResponseDescriptor *articleDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodAny pathPattern:@"/services.json" keyPath:@"service" statusCodes:statusCodes];
    
    RKResponseDescriptor *singleTimeEntryDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodGET pathPattern:@"/services/:id" keyPath:@"service" statusCodes:statusCodes];
    
    
    RKResponseDescriptor *createResponse = [RKResponseDescriptor responseDescriptorWithMapping:mapping method:RKRequestMethodPOST pathPattern:@"/services" keyPath:@"service" statusCodes:statusCodes];
    
    
    RKRequestDescriptor * requestDescriptor = [RKRequestDescriptor requestDescriptorWithMapping:[mapping inverseMapping] objectClass:[MiteService class] rootKeyPath:@"service" method:RKRequestMethodFromString(@"POST")];
    
    [self addRequestDescriptor:requestDescriptor];
    
    [self addResponseDescriptorsFromArray:@[ articleDescriptor, singleTimeEntryDescriptor, createResponse ]];
    
    __weak ATMiteDatasource *weakSelf = self;
    
    [self addFetchRequestBlock:^NSFetchRequest *(NSURL *URL) {
        
        __strong ATMiteDatasource *strongSelf = weakSelf;
        
        RKPathMatcher *pathMatcher = [RKPathMatcher pathMatcherWithPattern:@"/services.json"];
        
        NSDictionary *argsDict = nil;
        
        BOOL match = [pathMatcher matchesPath:[URL relativePath] tokenizeQueryStrings:YES parsedArguments:&argsDict];
        
        
        if (match) {
            NSLog(@"Match!!!!!");
            return [strongSelf fetchRequestForServices];
        } else {
            NSLog(@"No match!!!!");
        }
        
        return nil;
    }];
    
}

- (NSManagedObjectContext *)mainQueueManagedObjectContext {
    return self.managedObjectStore.mainQueueManagedObjectContext;
}


#pragma mark - Time Entries Protocol

- (NSFetchRequest *)fetchRequestForTimeEntriesWithDateComponents:(NSDateComponents *)dateComponents {

    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    NSDate *startDate = [currentCalendar dateFromComponents:dateComponents];
    NSDate *endDate = [startDate dateByAddingDays:1];
    
    return [self fetchRequestForTimeEntriesWithStartDate:startDate endDate:endDate];
}

- (NSFetchRequest *)fetchRequestForTimeEntriesWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate {

    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"MiteTimeEntry"];
    
    NSMutableString *predicateFormat = [[NSMutableString alloc] init];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:startDate forKey:@"STARTDATE"];
    [dict setObject:endDate forKey:@"ENDDATE"];
    
    [predicateFormat appendFormat:@"date_at < $ENDDATE AND date_at >= $STARTDATE"];
    
    NSPredicate *__predicate = [NSPredicate predicateWithFormat:predicateFormat];
    __predicate = [__predicate predicateWithSubstitutionVariables:dict];
    
    fetchRequest.predicate = __predicate;

    return fetchRequest;
}

- (void)getTimeEntriesWithDateComponents:(NSDateComponents *)dateComponents complete:(void (^)(NSArray *results))completeBlock failed:(void (^)(NSError *failed))failedBlock {
    
    NSDate *startDate   =  [[NSCalendar currentCalendar] dateFromComponents:dateComponents];
    
    NSString *date_at = [[NSValueTransformer valueTransformerForName:MiteDateTransformerDateOnly] reverseTransformedValue:startDate];
    NSString *path = [NSString stringWithFormat:@"/time_entries.json?at=%@", date_at];
    
    [self getObjectsAtPath:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        completeBlock([mappingResult array]);
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        failedBlock(error);
        
    }];
    
}



#pragma mark - Categories

- (NSFetchRequest *)fetchRequestForMainCategory {
    return [self fetchRequestForProjects];
}

- (NSFetchRequest *)fetchRequestForSecondaryCategory {
    return [self fetchRequestForServices];
}

- (ATCategoryConfiguration *)configurationForMainCategory {
    return [ATCategoryConfiguration categoryWithName:@"Project" shortDescription:@"" icons:@{@"tableViewCell":[[UIImage imageNamed:@"project_small"] imageTintedWithColor:[UIColor whiteColor]]} listViewController:nil modelObject:nil];
}

- (ATCategoryConfiguration *)configurationForSecondaryCategory {
    return [ATCategoryConfiguration categoryWithName:@"Service" shortDescription:@"" icons:@{@"tableViewCell":[[UIImage imageNamed:@"tag_small"] imageTintedWithColor:[UIColor whiteColor]]} listViewController:nil modelObject:nil];
}

- (void)getMainCategoryObjectsComplete:(void (^)(NSArray *results))completeBlock failed:(void (^)(NSError *failed))failedBlock {
    [self getProjectsComplete:completeBlock failed:failedBlock];
}

- (void)getSecondaryCategoryObjectsComplete:(void (^)(NSArray *results))completeBlock failed:(void (^)(NSError *failed))failedBlock {
    [self getServicesComplete:completeBlock failed:failedBlock];
}


#pragma mark - Specific Category Implementation

- (NSFetchRequest *)fetchRequestForProjects {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"MiteProject"];
    return fetchRequest;
}

- (void)getProjectsComplete:(void (^)(NSArray *results))completeBlock failed:(void (^)(NSError *failed))failedBlock {
    
    NSString *path = [NSString stringWithFormat:@"/projects.json"];
    
    [self getObjectsAtPath:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        completeBlock([mappingResult array]);
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        failedBlock(error);
        
    }];
}

- (NSFetchRequest *)fetchRequestForServices {
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"MiteService"];
    return fetchRequest;
}

- (void)getServicesComplete:(void (^)(NSArray *results))completeBlock failed:(void (^)(NSError *failed))failedBlock {
    
    NSString *path = [NSString stringWithFormat:@"/services.json"];
    
    [self getObjectsAtPath:path parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        
        completeBlock([mappingResult array]);
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        failedBlock(error);
        
    }];
}

#pragma mark - Timer

- (id <ATTimeEntryProtocol>)insertNewTimeEntry {
    RKManagedObjectStore *objectStore = [self managedObjectStore];
    MiteTimeEntry *newTimeEntry = [NSEntityDescription insertNewObjectForEntityForName:@"MiteTimeEntry" inManagedObjectContext:objectStore.mainQueueManagedObjectContext];
    return newTimeEntry;
}

- (void)postTimeEntry:(id <ATTimeEntryProtocol>)timeEntry success:(void (^)())success failure:(void (^)(NSError *error))failure {
    
    NSString *postPath = [NSString stringWithFormat:@"/time_entries"];
    
    
    [self postObject:timeEntry path:postPath parameters:Nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {

        if (success) success();
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        
        if (failure) failure(error);
        
    }];

}

- (void)savelyStartTimeEntry:(id <ATTimeEntryProtocol>)timeEntry success:(void (^)())success failure:(void (^)(NSError *error))failure {
    if ([timeEntry isInserted]) {
        [self createAndStartTimeEntry:timeEntry success:success failure:failure];
    } else {
        [self startTimeEntry:timeEntry success:success failure:failure];
    }
}

- (void)createAndStartTimeEntry:(id <ATTimeEntryProtocol>) timeEntry success:(void (^)())success failure:(void (^)(NSError *error))failure {

    [self postTimeEntry:timeEntry success:^{
        
       [self startTimeEntry:timeEntry success:^{
           
           if (success) success();
           
       } failure:failure];
        
    } failure:failure];
    
}

- (void)startTimeEntry:(id <ATTimeEntryProtocol>)timeEntry success:(void (^)())success failure:(void (^)(NSError *error))failure {
   __weak __typeof(& *self) weakSelf = self;
    
    NSString *putPath = [NSString stringWithFormat:@"/tracker/%@.json", [timeEntry timeEntryIdentifier]];
    
    [self.HTTPClient putPath:putPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        __strong __typeof(&*weakSelf) strongSelf = weakSelf;
        
        NSDictionary *tracking_time_entry =[strongSelf valueTransformedDictionaryFromTrackingResponseObject:responseObject];
        
        [timeEntry setTrackingDurationInMinutes:tracking_time_entry[@"minutes"]];
        [timeEntry setTrackingSince:tracking_time_entry[@"since"]];
        
        if (success) success();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failure) failure(error);
        
    }];
}

- (void)stopTimeEntry:(id <ATTimeEntryProtocol>)timeEntry success:(void (^)())success failure:(void (^)(NSError *error))failure {
    
    NSString *deletePath = [NSString stringWithFormat:@"/tracker/%@.json", [timeEntry timeEntryIdentifier]];
    __weak __typeof(& *self) weakSelf = self;

    [self.HTTPClient deletePath:deletePath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {

        __strong __typeof(&*weakSelf) strongSelf = weakSelf;
        
        NSDictionary *tracking_time_entry =[strongSelf valueTransformedDictionaryFromTrackingResponseObject:responseObject];

        [timeEntry setDurationInMinutes:tracking_time_entry[@"minutes"]];
        [timeEntry setTrackingSince:nil];
        
        [timeEntry stopTimer];
        
        if (success) success();
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failure) failure(error);
        
    }];
    
}

- (void)checkForTrackingTimeEntrySuccess:(void (^)(id <ATTimeEntryProtocol> timeEntry))success failure:(void (^)(NSError *error))failure {
    
    
    NSString *trackingPath = [NSString stringWithFormat:@"/tracker"];
    
    __weak __typeof(& *self) weakSelf = self;

    [self.HTTPClient getPath:trackingPath parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {

//        NSLog(@"%@", operation.response.allHeaderFields[@"date"]);
        
        __strong __typeof(&*weakSelf) strongSelf = weakSelf;
        
        NSNumber *remoteID = [[[responseObject objectForKey:@"tracker"] objectForKey:@"tracking_time_entry"] objectForKey:@"id"];
        
        if (remoteID) {
            NSString *runningTimeEntryPath = [NSString stringWithFormat:@"/time_entries/%@.json", remoteID];
            
            [strongSelf getObject:nil path:runningTimeEntryPath parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                
                if ([mappingResult count] > 0) {
                    
                    id <ATTimeEntryProtocol> trackingTimeEntry = (id <ATTimeEntryProtocol>)[mappingResult firstObject];
                    
                    if (success) success(trackingTimeEntry);
                    
                } else {
                    
                    if (success) success(nil);
                    
                }
                
                
            } failure:^(RKObjectRequestOperation *operation, NSError *error) {
                
                if (failure) failure(error);
                
            }];
        } else {
            if (success) success(nil);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failure) failure(error);
    
    }];
    
}

- (void)deleteTimeEntries:(NSSet *)timeEntries progess:(void (^)(float progress))progress completion:(void (^)())completion failed:(void (^)()) failed {
    
    NSMutableArray *operations = [[NSMutableArray alloc] init];
    
    [timeEntries enumerateObjectsUsingBlock:^(id <ATTimeEntryProtocol> timeEntry, BOOL *stop) {
       
        NSString *path = [NSString stringWithFormat:@"/time_entries/%@.json", [timeEntry timeEntryIdentifier]];
        
        NSURLRequest *request = [self.HTTPClient requestWithMethod:@"DELETE" path:path parameters:nil];
        
        AFHTTPRequestOperation *operation = [self.HTTPClient HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            [timeEntry delete];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            
            NSLog(@"Could not delete: %@", timeEntry);
        }];

        [operations addObject:operation];
    
    }];
    
    [self.HTTPClient enqueueBatchOfHTTPRequestOperations:operations progressBlock:^(NSUInteger numberOfFinishedOperations, NSUInteger totalNumberOfOperations) {
       
        if (progress) progress(numberOfFinishedOperations / totalNumberOfOperations);
        
    } completionBlock:^(NSArray *operations) {
    
        if (completion) completion();
        
    }];
    
}


- (NSDictionary *)valueTransformedDictionaryFromTrackingResponseObject:(NSDictionary *)responseObject {
    
    NSMutableDictionary *mutableDict = [NSMutableDictionary dictionary];

    NSDictionary *tracking_time_entry = [[responseObject valueForKey:@"tracker"] objectForKey:@"tracking_time_entry"];
    
    if (!tracking_time_entry) tracking_time_entry = [[responseObject valueForKey:@"tracker"] objectForKey:@"stopped_time_entry"];
    
    NSString *trackingSinceString = tracking_time_entry[@"since"];

    if (trackingSinceString) {
        NSDate *trackingSinceDate = RKDateFromString(trackingSinceString);
        
        if (trackingSinceDate)
            mutableDict[@"since"] = trackingSinceDate;
    }

    mutableDict[@"minutes"] = tracking_time_entry[@"minutes"];
    
    return mutableDict;
}

@end