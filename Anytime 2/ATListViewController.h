//
//  ATListViewController.h
//  anytime
//
//  Created by Josef Materi on 06.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ATListViewControllerDelegate;
@protocol ATListViewControllerDatasource;

@interface ATListViewController : UITableViewController

@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSArray *deletedObjects;

@property (nonatomic, weak) id <ATListViewControllerDelegate> delegate;
@property (nonatomic, weak) id <ATListViewControllerDatasource> datasource;

- (void)reloadData;
- (void)refreshData;

@end

@protocol ATListViewControllerDelegate <NSObject>

- (void)listViewController:(ATListViewController *)tevc didSelectObject:(NSManagedObject *)timeEntry;

@optional

- (void)listViewControllerDidScroll:(ATListViewController *)listViewController;
- (void)listViewControllerWillBeginDragging:(ATListViewController *)listViewController;
- (void)listViewControllerDidEndDecelerating:(ATListViewController *)listViewController;
- (void)listViewControllerWillEndDragging:(ATListViewController *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset;
@end

@protocol ATListViewControllerDatasource <NSObject>
@optional
- (NSSet *)selectedObjectsForListViewController:(ATListViewController *)listViewController;
@end