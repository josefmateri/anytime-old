//
//  ATCategorySelectionViewController.m
//  anytime
//
//  Created by Josef Materi on 06.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATCategorySelectionViewController.h"
#import "ATMainCategoryViewController.h"
#import "ATSecondaryCategoryViewController.h"
#import "UIImage+Tint.h"
#import "ATDarkTableViewCell.h"
#import <Masonry.h>
#import "ATButtonGestureRecognizer.h"

@interface ATCategorySelectionViewController () <ATListViewControllerDatasource, ATListViewControllerDelegate>
@property (nonatomic, strong) UITabBar *tabBar;
@property (nonatomic, strong) UISegmentedControl *segmentedControl;
@property (nonatomic, strong) ATMainCategoryViewController *mainCategoryListViewController;
@property (nonatomic, strong) ATSecondaryCategoryViewController *secondaryCategoryListViewController;
@property (nonatomic, strong) ATDarkTableViewCell *mainCategoryCell;
@property (nonatomic, strong) ATDarkTableViewCell *secondaryCategoryCell;
@property (nonatomic, strong) NSArray *categoryConfigurations;
@property (nonatomic, strong) UIView *borderLineBetweenCells;
@property (nonatomic) CategorySelectionMode categorySelectionMode;
@end

@implementation ATCategorySelectionViewController

- (id)initWithCategoryConfigurations:(NSArray *)categoryConfigurations {

    self = [super initWithNibName:nil bundle:nil];
    if (self) {
        // Custom initialization
        self.categoryConfigurations = categoryConfigurations;
    }
    return self;
    
}

- (void)setBottomBorderColor:(UIColor *)color {
    [self.mainCategoryCell setBottomBorderColor:color];
    [self.secondaryCategoryCell setBottomBorderColor:color];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UITabBar *tabBar = [[UITabBar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 49)];
    [self.view addSubview:tabBar];
    
    self.view.clipsToBounds = YES;
    
    [tabBar makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view.width);
        make.bottom.equalTo(self.view.bottom);
        make.centerX.equalTo(self.view.centerX);
    }];
    
    tabBar.barTintColor = [UIColor colorWithRed:0.106 green:0.165 blue:0.243 alpha:000];
    [tabBar setBackgroundImage:[UIImage imageNamed:@"navbar-bg"]];
    
    tabBar.tintColor = [UIColor whiteColor];
    
    UITabBarItem *item1 = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemFavorites tag:0];
    UITabBarItem *item2 = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemMostRecent tag:0];
    UITabBarItem *item3 = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemFeatured tag:0];
    UITabBarItem *item4 = [[UITabBarItem alloc] initWithTabBarSystemItem:UITabBarSystemItemBookmarks tag:0];

    [tabBar setItems:@[item1, item2, item3, item4] animated:NO];
    
    [tabBar setSelectedItem:item1];
    self.tabBar = tabBar;
    self.tabBar.alpha = 0;
//
//    UISegmentedControl *segControl = [[UISegmentedControl alloc] initWithItems:@[@"Project", @"Service"]];
//    [self.view addSubview:segControl];
//    
//    [segControl makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self.view.top).offset(10);
//        make.width.equalTo(categoryLabel.width).offset(-20);
//        make.centerX.equalTo(categoryLabel.centerX);
//    }];
//    
//    segControl.tintColor = [UIColor colorWithWhite:1 alpha:0.5];
//    [segControl setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor colorWithRed:0.094 green:0.149 blue:0.216 alpha:1.000]} forState:UIControlStateHighlighted];
//    [segControl setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor colorWithRed:0.094 green:0.149 blue:0.216 alpha:1.000]} forState:UIControlStateSelected];
//
//    segControl.alpha = 0;
//    self.segmentedControl = segControl;
//    
    self.mainCategoryListViewController = [[ATMainCategoryViewController alloc] initWithStyle:UITableViewStylePlain];
    self.mainCategoryListViewController.categoryDatasource = self.categoryDatasource;
    
    [self.view addSubview:self.mainCategoryListViewController.view];
    
    self.mainCategoryListViewController.delegate = self;
    
    [self.mainCategoryListViewController.view makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.top);
        make.width.equalTo(self.view.width);
        make.bottom.equalTo(tabBar.top);
        make.centerX.equalTo(self.view.centerX);
    }];
    
    
    self.secondaryCategoryListViewController = [[ATSecondaryCategoryViewController alloc] initWithStyle:UITableViewStylePlain];
    self.secondaryCategoryListViewController.categoryDatasource = self.categoryDatasource;
    
    [self.view addSubview:self.secondaryCategoryListViewController.view];
    
    self.secondaryCategoryListViewController.delegate = self;
    
    [self.secondaryCategoryListViewController.view makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view.top);
        make.width.equalTo(self.view.width);
        make.bottom.equalTo(tabBar.top);
        make.centerX.equalTo(self.view.centerX);
    }];
    
//    [self.mainCategoryListViewController refreshData];
    
    ATDarkTableViewCell *project = [[ATDarkTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    [self.view addSubview:project];
    project.textLabel.textColor = [UIColor whiteColor];
    project.frame = CGRectMake(0, 0, self.view.bounds.size.width, 60);
//    project.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [project setBorderHidden:NO];
    __weak __typeof(& *self) weakSelf = self;

    [project registerTouchesBeganBlock:^{
        
    } touchesEndedBlock:^{
        __strong __typeof(&*weakSelf) strongSelf = weakSelf;
        [strongSelf didTapOnMainCategoryCell:nil];
        [project setHighlighted:NO animated:YES];
    } touchesMoved:^{
        
    } touchesCancled:^{
        
    }];

    
    ATDarkTableViewCell *service =[[ATDarkTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    [self.view addSubview:service];
    service.textLabel.textColor = [UIColor whiteColor];
    service.frame = CGRectMake(0, 60, self.view.bounds.size.width, 60);
//    service.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [service setBorderHidden:NO];
    [service registerTouchesBeganBlock:^{
                
    } touchesEndedBlock:^{
        __strong __typeof(&*weakSelf) strongSelf = weakSelf;
        [strongSelf didTapOnSecondaryCategoryCell:nil];
        [service setHighlighted:NO animated:YES];
    } touchesMoved:^{
        
    } touchesCancled:^{
        
    }];

    
    self.secondaryCategoryCell = service;
    self.mainCategoryCell = project;

    [self reloadData];

    [self setSelectionElementsHidden:YES];
  
}

- (void)didTapOnMainCategoryCell:(UILongPressGestureRecognizer *)gesture {
            
        [self.mainCategoryListViewController.view setHidden:NO];
        [self.secondaryCategoryListViewController.view setHidden:YES];
        
        [self.mainCategoryListViewController refreshData];
        
        self.categorySelectionMode = CategorySelectionModeMainCategory;
        
        __strong id <ATCategorySelectionViewControllerDelegate> delegate = self.delegate;
        
        if ([delegate respondsToSelector:@selector(categorySelectionViewControllerWillShowMainCategorySelection:)]) {
            [delegate categorySelectionViewControllerWillShowMainCategorySelection:self];
        }
}

- (void)didTapOnSecondaryCategoryCell:(UILongPressGestureRecognizer *)gesture {
    
    
    
        [self.mainCategoryListViewController.view setHidden:YES];
        [self.secondaryCategoryListViewController.view setHidden:NO];
        
        [self.secondaryCategoryListViewController refreshData];
        
        self.categorySelectionMode = CategorySelectionModeSecondaryCategory;
        
        __strong id <ATCategorySelectionViewControllerDelegate> delegate = self.delegate;
        
        if ([delegate respondsToSelector:@selector(categorySelectionViewControllerWillShowSecondaryCategorySelection:)]) {
            [delegate categorySelectionViewControllerWillShowSecondaryCategorySelection:self];
        }
}

- (void)reloadData {
    
    self.secondaryCategoryCell.textLabel.text = [self titleForSecondaryCategory];
    self.secondaryCategoryCell.imageView.image = [self iconForSecondaryCategory];
    
    self.mainCategoryCell.textLabel.text = [self titleForMainCategory];
    self.mainCategoryCell.imageView.image = [self iconForMainCategory];

    [self.mainCategoryCell setNeedsLayout];
    [self.secondaryCategoryCell setNeedsLayout];
        
}

- (NSString *)titleForMainCategory {
    __strong id <ATCategorySelectionViewControllerDatasource> datasource = self.datasource;
    NSString *title = nil;
    if ([datasource respondsToSelector:@selector(mainCategoryTitleForCategorySelectionViewController:)]) {
        title = [datasource mainCategoryTitleForCategorySelectionViewController:self];
    }
    return title;
}

- (NSString *)titleForSecondaryCategory {
    
    __strong id <ATCategorySelectionViewControllerDatasource> datasource = self.datasource;
    
    NSString *title = nil;
    
    if ([datasource respondsToSelector:@selector(secondaryCategoryTitleForCategorySelectionViewController:)]) {
        title = [datasource secondaryCategoryTitleForCategorySelectionViewController:self];
    }
    
    return title;
}

- (UIImage *)iconForMainCategory {
      __strong id <ATCategorySelectionViewControllerDatasource> datasource = self.datasource;
    ATCategoryConfiguration *configuration = nil;
    if ([datasource respondsToSelector:@selector(mainCategoryConfigurationForCategorySelectionViewController:)]) {
        configuration = [datasource mainCategoryConfigurationForCategorySelectionViewController:self];
    }
    UIImage *image= [configuration icons][@"tableViewCell"];
    return image;
}

- (UIImage *)iconForSecondaryCategory {
    __strong id <ATCategorySelectionViewControllerDatasource> datasource = self.datasource;
    ATCategoryConfiguration *configuration = nil;
    if ([datasource respondsToSelector:@selector(secondaryCategoryConfigurationForCategorySelectionViewController:)]) {
        configuration = [datasource secondaryCategoryConfigurationForCategorySelectionViewController:self];
    }
    UIImage *image= [configuration icons][@"tableViewCell"];
    return image;
}

- (void)setSelectionElementsHidden:(BOOL)hidden {
    if (hidden) {
        self.detailCategoryLabel.alpha = 1;
        self.segmentedControl.alpha = 0;
        self.tabBar.alpha = 0;
        
        self.mainCategoryListViewController.view.alpha = 0;
        self.secondaryCategoryListViewController.view.alpha = 0;
        
        self.mainCategoryCell.alpha = 1;
        self.secondaryCategoryCell.alpha = 1;
        self.borderLineBetweenCells.alpha = 1;
        
    } else {
        
        if (self.categorySelectionMode == CategorySelectionModeMainCategory) {
            self.mainCategoryListViewController.view.alpha = 1;
        } else {
            self.secondaryCategoryListViewController.view.alpha = 1;
        }
        
        self.detailCategoryLabel.alpha = 0;
        self.segmentedControl.alpha = 1;
        self.tabBar.alpha = 1;
        self.mainCategoryCell.alpha = 0;
        self.secondaryCategoryCell.alpha = 0;
        self.borderLineBetweenCells.alpha = 0;
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)listViewController:(ATListViewController *)tevc didSelectObject:(NSManagedObject *)object {
    
    __strong id <ATCategorySelectionViewControllerDelegate> delegate = self.delegate;
    
    if (self.categorySelectionMode == CategorySelectionModeMainCategory) {
        if ([delegate respondsToSelector:@selector(categorySelectionViewController:didSelectMainCategory:)]) {
            [delegate categorySelectionViewController:self didSelectMainCategory:(id <ATCategoryProtocol>)object];
        }
    } else {
        if ([delegate respondsToSelector:@selector(categorySelectionViewController:didSelectSecondaryCategory:)]) {
            [delegate categorySelectionViewController:self didSelectSecondaryCategory:(id <ATCategoryProtocol>)object];
        }
    }

    
}


@end
