//
//  ATTouchableView.h
//  anytime
//
//  Created by Josef Materi on 26.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ATTouchableView : UIView

typedef void(^ATTouchesBlock)(void);

@property (nonatomic, copy) ATTouchesBlock touchesBeganBlock;
@property (nonatomic, copy) ATTouchesBlock touchesEndedBlock;
@property (nonatomic, copy) ATTouchesBlock touchesMovedBlock;
@property (nonatomic, copy) ATTouchesBlock touchesCancledBlock;

- (void)registerTouchesBeganBlock:(void (^)())touchesBeganBlock touchesEndedBlock:(void (^)())touchesEndedBlock touchesMoved:(void (^)())touchesMovedBlock touchesCancled:(void (^)())touchesCancledBlock;

@end
