//
//  ATTestViewController.m
//  Anytime 2
//
//  Created by Josef Materi on 07.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATCalendarWithTimeEntriesViewController.h"
#import "ATTimeEntriesListViewController.h"
#import "ATDateSelectionView.h"
#import "ATWeekCollectionViewController.h"
#import "UIView+FLKAutoLayout.h"
#import "UIView+Position.h"
#import "ATDateConfiguration.h"
#import "NSDate+Additions.h"
#import "ATStopWatchViewController.h"
#import "UIImage+ImageEffects.h"
#import "ATPercentDrivenTransition.h"
#import "UIView+Positioning.h"
#import <Masonry.h>
#import "NSObject+Delay.h"
#import "UIImage+Tint.h"
#import "DMLazyScrollView.h"
#import "ATTimeEntryProtocol.h"
#import "ATStopWatchViewControllerCollection.h"
#import "ATProgressHUD.h"

@interface ATCalendarWithTimeEntriesViewController () <ATListViewControllerDelegate, ATListViewControllerDatasource, ATDateSelectionViewDelegate, UIGestureRecognizerDelegate, ATDynamicBouncingDelegate, DMLazyScrollViewDelegate, ATStopWatchViewControllerDelegate, ATDateSelectionViewDatasource>

@property (nonatomic, strong) UIDynamicAnimator* animator;
@property (nonatomic, strong) ATTimeEntriesListViewController *timeEntriesViewController;
@property (nonatomic, strong) ATDateSelectionView *dateSelectionView;
@property (nonatomic, strong) NSLayoutConstraint *topTableConstraint;
@property (nonatomic, strong) NSLayoutConstraint *bottomTableConstraint;
@property (nonatomic, strong) NSLayoutConstraint *toolbarTopConstraint;

@property (nonatomic) CGPoint startPoint;
@property (nonatomic, strong) UIPageViewController *pageViewController;
@property (nonatomic, strong) NSMutableArray *timeEntriesControllers;
@property (nonatomic, strong) DMLazyScrollView *timeEntriesViewControllerScrollView;
@property (nonatomic, strong) UIToolbar *toolbar;
@property (nonatomic, strong) ATStopWatchViewController *stopWatchViewController;
@property (nonatomic, strong) UIDynamicAnimator *pushAnimator;
@property (nonatomic, weak) id <UIViewControllerContextTransitioning> transitionContext;
@property (nonatomic, strong) NSMutableSet *selectedTimeEntries;
@property (nonatomic) float initialToolbarBottomOffset;
@property (nonatomic, strong) UILabel *selectedTimeEntriesLabel;
@end

@implementation ATCalendarWithTimeEntriesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        self.selectedTimeEntries = [[NSMutableSet alloc] init];
        
    }
    return self;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    self.initialToolbarBottomOffset = 44;

    self.view.backgroundColor = [UIColor whiteColor];
    
    
    
    ATDateSelectionView *datePresentationView = [[ATDateSelectionView alloc] initWithFrame:CGRectMake(0,0, self.view.bounds.size.width, 130)];
   // datePresentationView.backgroundColor = [UIColor colorWithWhite:0.98 alpha:1];
    [self.view addSubview:datePresentationView];
    
    datePresentationView.delegate = self;
    datePresentationView.datasource = self;
    
    datePresentationView.alpha = 1;
    
    self.dateSelectionView = datePresentationView;
    
//    self.dateSelectionView.translatesAutoresizingMaskIntoConstraints = NO;
//
//    [self.dateSelectionView makeConstraints:^(MASConstraintMaker *make) {
//        
//        make.width.equalTo(self.view.width);
//        make.top.equalTo(self.view.top);
//        make.centerX.equalTo(self.view.centerX);
//        make.height.equalTo(@(130));
//    }];
    
    [self.dateSelectionView setCurrentDate:[NSDate date]];
    
    [self.dateSelectionView reloadNavigationItems];
    
//    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
//
//    self.pageViewController.delegate = self;
//    self.pageViewController.dataSource = self;
//    
//    self.pageViewController.view.frame = self.view.bounds;
    
//    [self addChildViewController:self.pageViewController];
//    
//    [self.view addSubview:self.pageViewController.view];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.timeEntriesViewControllerScrollView = [[DMLazyScrollView alloc] initWithFrameAndDirection:self.view.bounds direction:DMLazyScrollViewDirectionHorizontal circularScroll:YES];
    self.timeEntriesViewControllerScrollView.backgroundColor = [UIColor redColor];
    
    self.timeEntriesControllers = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 3; i++) {
        ATTimeEntriesListViewController *timeEntriesViewController = [[ATTimeEntriesListViewController alloc] initWithStyle:UITableViewStylePlain];
        timeEntriesViewController.delegate = self;
        timeEntriesViewController.datasource = self;
        timeEntriesViewController.tableView.contentInset = UIEdgeInsetsMake(self.dateSelectionView.initialHeight, 0, self.initialToolbarBottomOffset, 0);
        timeEntriesViewController.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        timeEntriesViewController.view.frame = self.view.bounds;
        [self.timeEntriesControllers addObject:timeEntriesViewController];
    }
    
    self.timeEntriesViewControllerScrollView.controlDelegate = self;
    self.timeEntriesViewControllerScrollView.translatesAutoresizingMaskIntoConstraints = NO;
    
    __weak __typeof(&*self)weakSelf = self;
    
    _timeEntriesViewControllerScrollView.dataSource = ^(NSUInteger index) {
        return weakSelf.timeEntriesControllers[index];
    };
    
    self.timeEntriesViewControllerScrollView.numberOfPages = 3;
    
    [self.view addSubview:self.timeEntriesViewControllerScrollView];
    

//    ATTimeEntriesViewController *first = [self timeEntriesViewControllerWithDateComponent:self.dateSelectionView.dateComponents];
//    first.view.translatesAutoresizingMaskIntoConstraints = NO;
//    
//    [self.view addSubview:first.view];
    
    self.timeEntriesViewControllerScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
    [self.timeEntriesViewControllerScrollView makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view.width);
        make.centerX.equalTo(self.view.centerX);
        make.height.equalTo(self.view.height);
    }];
    
    [self.view bringSubviewToFront:self.dateSelectionView];
    
    self.topTableConstraint = [NSLayoutConstraint constraintWithItem:self.timeEntriesViewControllerScrollView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.dateSelectionView attribute:NSLayoutAttributeBottom multiplier:1 constant:-self.dateSelectionView.bounds.size.height];

    [self.view addConstraint:self.topTableConstraint];

    
     [self configureEditingToolbar];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];

}

- (void)configureEditingToolbar {
    self.navigationController.toolbar.barTintColor = [UIColor colorWithRed:0.041 green:0.210 blue:0.421 alpha:1.000];
    self.navigationController.toolbar.tintColor = [UIColor whiteColor];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    

    ATTimeEntriesListViewController *current = (ATTimeEntriesListViewController *)self.timeEntriesViewControllerScrollView.currentViewController;
    current.dateComponents = self.dateSelectionView.dateComponents;
    [current reloadData];
    [current refreshData];
    
}

- (void)didSelectStopWatch:(UIBarButtonItem *)item {
    
//
//    [self.stopWatchViewController setModalPresentationStyle:UIModalPresentationCustom];
//    self.stopWatchViewController.transitioningDelegate = self;
//
//    ATPercentDrivenTransition *transition = [[ATPercentDrivenTransition alloc] initWithNavigationController:nil];
//    self.stopWatchViewController.transition = transition;
    
}

- (void)dynamicBouncer:(UIViewController *)viewController isBouncingToFrame:(CGRect)frame {
    self.topTableConstraint.constant =  - self.view.bounds.size.height + frame.origin.y -80 + 44;
    self.bottomTableConstraint.constant = -self.view.bounds.size.height + frame.origin.y + 44;
}

- (void)dynamicAnimatorDidPause:(UIDynamicAnimator*)animator {
    [self.timeEntriesViewController.tableView setContentOffset:CGPointMake(0, -80)];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}



#pragma mark - DateSelectionView

- (void)dateSelectionViewDidBeginBouncing:(ATDateSelectionView *)selectionView {
    
    //[self.timeEntriesViewController.tableView setContentOffset:CGPointMake(0, -80) animated:NO];
//    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didPanTimeEntries:)];
//    [self.timeEntriesViewController.tableView addGestureRecognizer:pan];
    
}

- (void)dateSelectionViewDidToggleMenu:(ATDateSelectionView *)selectionView {
    
    
}


- (UINavigationItem *)navigationItemsForDataSelectionView:(ATDateSelectionView *)dateSelectionview {
    
    UINavigationItem *navitem = [[UINavigationItem alloc] init];
   
    UIBarButtonItem *editButtonItem = self.editButtonItem;

    navitem.rightBarButtonItem = editButtonItem;
    
    return navitem;
}

- (void)dateSelectionView:(ATDateSelectionView *)selectionView didSelectDateWithComponents:(NSDateComponents *)components {
    
    [selectionView expand:NO animated:YES];
    
    ATTimeEntriesListViewController *current = (ATTimeEntriesListViewController *)self.timeEntriesViewControllerScrollView.currentViewController;
    current.dateComponents = components;
    
    [current reloadData];
    [current refreshData];
    
}

- (void)dateSelectionView:(ATDateSelectionView *)selectionView setEditing:(BOOL)editing {

}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
        
    [self.selectedTimeEntries removeAllObjects];
    
    [self.timeEntriesViewController setEditing:editing animated:YES];
    
    [self setEditingToolBarHidden:!editing completion:^(BOOL finished) {
        
    }];
}

- (void)setEditingToolBarHidden:(BOOL)hidden completion:(void (^)(BOOL finished))completion {
    
    
    UIBarButtonItem *deletionItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemTrash target:self action:@selector(deleteSelection:)];
    
    UILabel *centerLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
    centerLabel.textColor = [UIColor whiteColor];
    self.selectedTimeEntriesLabel = centerLabel;

    [self updateSelectedTimeEntriesLabel];
    
    UIBarButtonItem *centerLabelItem = [[UIBarButtonItem alloc] initWithCustomView:centerLabel];
    
    UIBarButtonItem *actionItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(selectedAction:)];
    
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    [self setToolbarItems:@[deletionItem,flexItem, centerLabelItem, flexItem, actionItem]];
    
    if (!hidden) {
        
        [self.navigationController setToolbarHidden:NO animated:YES];

        [[ATStopWatchViewControllerCollection sharedCollection] hideStopwatchesAnimated:YES completion:^(BOOL finished) {
    
            
        }];
        
    } else {
        
        [self.navigationController setToolbarHidden:YES animated:YES];

        [[ATStopWatchViewControllerCollection sharedCollection] showStopwatchesAnimated:YES completion:^(BOOL finished) {
            
        }];
    }

}

- (void)updateSelectedTimeEntriesLabel {
    
    NSInteger numberOfSelectedItems = [self.selectedTimeEntries count];
    
    if (numberOfSelectedItems) {
        if (numberOfSelectedItems == 1) {
            self.selectedTimeEntriesLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%i item selected", nil), [self.selectedTimeEntries count]];
        } else {
            self.selectedTimeEntriesLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%i items selected", nil), [self.selectedTimeEntries count]];
        }
    } else {
        self.selectedTimeEntriesLabel.text = [NSString stringWithFormat:NSLocalizedString(@"No items selected", nil), [self.selectedTimeEntries count]];
    }
    
    
    [self.selectedTimeEntriesLabel sizeToFit];
}

- (void)deleteSelection:(UIBarButtonItem *)deleteItem {
    

    ATProgressHUD *hud = [ATProgressHUD showProgressHUDWithType:ATProgressHUDTypeDelete inParent:self.view.window];
    
    
   [[ATDatasources currentDatasource] deleteTimeEntries:self.selectedTimeEntries progess:^(float progress) {
       
       [hud setProgress:progress];
       
   } completion:^{
   
        [hud stopSpinProgressBackgroundLayer];
       [hud setLabelText:NSLocalizedString(@"Done", nil)];
        [hud hide:YES afterDelay:1];
       
   } failed:^{
       
   }];
}

#pragma mark - TimeEntriesViewControllerDelegate

- (void)listViewController:(ATListViewController *)tevc didSelectObject:(id <ATTimeEntryProtocol>)timeEntry {
    
    
    if (![tevc isEditing]) {

        [[ATStopWatchViewControllerCollection sharedCollection] showStopWatchViewControllerWithTimeEntry:timeEntry configure:^(ATStopWatchViewController *controller) {
            
            [controller configureWithTimeEntry:timeEntry];
            controller.delegate = self;
            
        } completion:^{
            
        }];
        
    } else {
        
        if ([self.selectedTimeEntries containsObject:timeEntry]) {
            [self.selectedTimeEntries removeObject:timeEntry];
        } else {
            [self.selectedTimeEntries addObject:timeEntry];
        }
        
        [self updateSelectedTimeEntriesLabel];
        
    }
}

- (void)listViewControllerDidScroll:(ATTimeEntriesListViewController *)listViewController {

    return;
    
    CGPoint velo = [listViewController.tableView.panGestureRecognizer velocityInView:listViewController.tableView];
    CGPoint offset = listViewController.tableView.contentOffset;

    
    offset.y += 80;
    offset.y *= -1;
    
    if (offset.y < 0) {
        self.topTableConstraint.constant = -80;
    } else {
        self.topTableConstraint.constant = -80 - offset.y;
    }
    
    offset.y += (self.dateSelectionView.initialHeight) ;
    
    
//    if (!self.stopWatchViewController.presenting) {
       [self.dateSelectionView didMoveToPoint:offset withState:UIGestureRecognizerStateChanged andVelocity:velo];
//    }
    
    
   CGPoint newOffset = listViewController.tableView.contentOffset;
   
   if (newOffset.y > -80) {
       
       float scrollOffset = fabs(offset.y) + listViewController.view.frame.size.height - 44;
       
       float contentSize = listViewController.tableView.contentSize.height;
       
       if (contentSize < listViewController.tableView.frame.size.height) {
           contentSize = listViewController.tableView.frame.size.height;
           scrollOffset = fabs(offset.y - 80) + listViewController.view.frame.size.height;
       }
       
       
       if (!self.stopWatchViewController.presenting) {
           
           if (scrollOffset > contentSize) {
               
               float difference = scrollOffset - contentSize;
               
               self.toolbarTopConstraint.constant= self.view.bounds.size.height - self.initialToolbarBottomOffset - difference;
            
               [self.stopWatchViewController show];
               
           }
       }
   }

}

- (void)listViewControllerWillEndDragging:(ATListViewController *)listViewController withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    return;
    
    UIScrollView *scrollView = listViewController.tableView;
        
    if (scrollView.contentOffset.y < -200 || (scrollView.contentOffset.y < -100 && velocity.y < -1)) {
        
        if (targetContentOffset->y < 0) {
            targetContentOffset->y = scrollView.contentOffset.y;
        }
        
//        CGPoint velo = [listViewController.tableView.panGestureRecognizer velocityInView:listViewController.tableView];
//        
////        [self.dateSelectionView slideDownWithSpeed:MIN(30,(int)velo.y)];
    }
 
   CGPoint newOffset = listViewController.tableView.contentOffset;
   
   if (newOffset.y > -80) {
       
       float scrollOffset = fabs(scrollView.contentOffset.y) + listViewController.view.frame.size.height - 44;
       
       float contentSize = listViewController.tableView.contentSize.height;
       
       if (contentSize < listViewController.tableView.frame.size.height) {
           contentSize = listViewController.tableView.frame.size.height;
           scrollOffset = fabs(newOffset.y) + 80 + listViewController.view.frame.size.height;
       }
       
       
       if (!self.stopWatchViewController.presenting) {
           
           if (scrollOffset > listViewController.tableView.contentSize.height) {
               float difference = scrollOffset - contentSize;
               
               
               if (difference > 140 || velocity.y > 1.5) {
                   self.stopWatchViewController.presenting = YES;
                   
                   targetContentOffset->y = scrollView.contentOffset.y;
                   [self didSelectStopWatch:nil];
                   
                   self.timeEntriesViewController.tableView.contentOffset = CGPointMake(0, 0);

               }
           }
       }
   }

}

- (void)listViewControllerWillBeginDragging:(ATTimeEntriesListViewController *)listViewController {
    
    [self.dateSelectionView removeAllAnimators];

}

//- (ATlistViewController *)listViewControllerWithDateComponent:(NSDateComponents *)dateComponent {
//    
//    ATlistViewController *controller = [self.timeEntriesControllers objectForKey:dateComponent];
//    
//    if (!controller) {
//        controller = [[ATlistViewController alloc] initWithStyle:UITableViewStylePlain];
//        controller.dateComponents = dateComponent;
//        controller.date = [[NSCalendar currentCalendar] dateFromComponents:dateComponent];
//    
//        controller.delegate = self;
//    
//        controller.tableView.contentInset = UIEdgeInsetsMake(self.dateSelectionView.initialHeight, 0, 0, 0);
//        
//        controller.view.frame = self.view.bounds;
//        controller.tableView.frame = self.view.bounds;
//        
//        [self.timeEntriesControllers setObject:controller forKey:dateComponent];
//    }
//    
//    return controller;
//    
//}

- (ATTimeEntriesListViewController *)timeEntriesViewController {
    return (ATTimeEntriesListViewController *)self.timeEntriesViewControllerScrollView.currentViewController;
}

- (NSSet *)selectedObjectsForListViewController:(ATListViewController *)timeEntriesViewController {
    return self.selectedTimeEntries;
}

#pragma mark - UIPanGestureRecogniter

- (void)didPan:(UIPanGestureRecognizer *)recognizer {

    UIGestureRecognizerState state = recognizer.state;

    CGPoint velocity = [recognizer velocityInView:self.view];
    
    if (state == UIGestureRecognizerStateEnded) {
        
        if (velocity.x < 0) {
            // right
        
            [self.dateSelectionView goToNextDay];
            
            
        } else {
            // left
        
            [self.dateSelectionView goToPrevDay];

        }
        
    }
}




#pragma mark - DMLazy

- (NSDateComponents *)currentComponentsByAddingDays:(NSInteger)days {

    NSDate *currentDate = [[NSCalendar currentCalendar] dateFromComponents:self.dateSelectionView.dateComponents];
    
    NSDate *nextDate = [currentDate dateByAddingDays:days];
    
    NSDateComponents *nextComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekday | NSCalendarUnitWeekOfYear | NSCalendarUnitWeekOfMonth fromDate:nextDate];

    return nextComponents;
}

- (void)lazyScrollViewWillEndDragging:(DMLazyScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    CGPoint off = *targetContentOffset;
    
    CGPoint currentOff = scrollView.currentPageOffset;
    
    float diff = fabs(off.x - currentOff.x);
    
    BOOL willBouncedBack = NO;
    
    if (diff != scrollView.bounds.size.width && diff < scrollView.bounds.size.width) willBouncedBack = YES;
    
    if (!willBouncedBack) {
        if (off.x > scrollView.contentOffset.x) {
            [self.dateSelectionView goToNextDay];
        } else {
            [self.dateSelectionView goToPrevDay];
        }
    }
    
}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView willChangeToNextPageWithViewController:(ATTimeEntriesListViewController *)viewController beforeViewController:
(ATTimeEntriesListViewController *)beforeViewController afterViewController:(ATTimeEntriesListViewController *)afterViewController {
    
    viewController.dateComponents = [self currentComponentsByAddingDays:1];

    [viewController setEditing:[self isEditing] animated:NO];

    [viewController reloadData];
    
}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView willChangeToPrevPageWithViewController:(ATTimeEntriesListViewController *)viewController beforeViewController:
(ATTimeEntriesListViewController *)beforeViewController afterViewController:(ATTimeEntriesListViewController *)afterViewController {

    viewController.dateComponents = [self currentComponentsByAddingDays:-1];
    [viewController setEditing:[self isEditing] animated:NO];
    [viewController reloadData];
}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView didChangeToNextPageWithViewController:(ATTimeEntriesListViewController *)viewController beforeViewController:(ATTimeEntriesListViewController *)beforeViewController afterViewController:(ATTimeEntriesListViewController *)afterViewController {
    [viewController refreshData];

    [self.dateSelectionView updateMonthView];

}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView didChangeToPrevPageWithViewController:(ATTimeEntriesListViewController *)viewController beforeViewController:(ATTimeEntriesListViewController *)beforeViewController afterViewController:(ATTimeEntriesListViewController *)afterViewController {
    [viewController refreshData];

    [self.dateSelectionView updateMonthView];
}




//- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
//    
//    ATTimeEntriesViewController *timeEntriesViewController = (ATTimeEntriesViewController *)viewController;
//    
//    NSDateComponents *dateComponents = [timeEntriesViewController.date componentsByAddingDays:1];
//    
//    return [self timeEntriesViewControllerWithDateComponent:dateComponents];
//    
//}
//- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
//    
//    ATTimeEntriesViewController *timeEntriesViewController = (ATTimeEntriesViewController *)viewController;
//    
//    NSDateComponents *dateComponents = [timeEntriesViewController.date componentsByAddingDays:-1];
//    
//    return [self timeEntriesViewControllerWithDateComponent:dateComponents];
//    
//}
//
//- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray *)pendingViewControllers {
//    
//}
//
//- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
//    
//    ATTimeEntriesViewController *current = [pageViewController.viewControllers objectAtIndex:0];
//    
//    
//    
//    
////    [self.dateSelectionView updateWithDateComponents:current.dateComponents];
//    
//}



#pragma mark - ATStopWatchViewControllerDelegate

- (void)stopWatchViewControllerWillDisappear:(ATStopWatchViewController *)stopWatchViewController {
    [self.timeEntriesViewController viewWillAppear:YES];
}

@end
