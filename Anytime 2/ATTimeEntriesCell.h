//
//  ATTimeEntriesCell.h
//  Anytime 2
//
//  Created by Josef Materi on 22.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ATTimeEntriesCell : UITableViewCell

@property (nonatomic, strong) UILabel *headline1;
@property (nonatomic, strong) UILabel *headline2;
@property (nonatomic, strong) UILabel *subline1;

@property (nonatomic, strong) UILabel *subline2;

@property (nonatomic, strong) UILabel *minutes;

@property (nonatomic, strong) UIColor *minutesBackgroundColor;

@end
