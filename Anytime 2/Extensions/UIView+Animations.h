//
//  UIView+Animations.h
//  Anytime
//
//  Created by Josef Materi on 23.10.11.
//  Copyright (c) 2011 materi design. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Animations)

typedef void (^CompletionHandler)();

- (void)startBlinkingWithSpeed:(CGFloat)speed;
- (void)stopBlinking;
-(void)stopBlinkingAfterDelay:(float)delay;

-(void)startBlinkingWithSpeed:(CGFloat)speed stopAfterDelay:(float)delay completion:(CompletionHandler)handler;

- (void)shakeAnimationWithDuration:(float)duration repeatCount:(NSUInteger)repeatCount completed:(CompletionHandler)completeBlock;

- (void)stopAnimations;

- (void)fadeToAlpha:(float)alpha withDuration:(NSTimeInterval)duration completed:(CompletionHandler)handler;

- (void)animateToScale:(float)scale andMoveByOffset:(CGPoint)offset duration:(NSTimeInterval)duration complete:(void (^)())complete;

- (void)animateToScale:(float)scale andMoveToPosition:(CGPoint)offset duration:(NSTimeInterval)duration complete:(void (^)())complete;

- (void)animateToScale:(float)scale  duration:(NSTimeInterval)duration complete:(void (^)())complete;

@end
