//
//  UIView+Position.h
//  Soccer
//
//  Created by Josef Materi on 08.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Position)

- (void)alignRightWithMargin:(float)margin;
- (void)alignLeftWithMargin:(float)margin;
- (void)alignBelowView:(UIView *)view withMargin:(float)margin;


- (void)adjustY:(float)y;

- (CGRect)frameForLeftAlignmentWithMargin:(float)margin;
- (CGRect)frameForRightAlignmentWithMargin:(float)margin;

- (void)moveFromFrame:(CGRect)frame toFrame:(CGRect)toFrame progress:(float)progress;

@end
