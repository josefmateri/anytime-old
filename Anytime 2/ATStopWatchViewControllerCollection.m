//
//  ATStopWatchViewControllerCollection.m
//  anytime
//
//  Created by Josef Materi on 22.09.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATStopWatchViewControllerCollection.h"
#import "UIView+Positioning.h"
#import "ATTimeEntryProtocol.h"
#import "ATDatasources.h"

@interface ATStopWatchViewControllerCollection () <ATStopWatchViewControllerNavigationDelegate>
@property (nonatomic, strong) ATStopWatchViewController *mainStopWatchViewController;
@property (nonatomic, strong) NSMutableArray *stopWatchViewControllers;
@property (nonatomic, strong) NSMutableArray *stopWatchViewControllersQueue;
@property (nonatomic, strong) id <ATTimeEntryProtocol> currentTrackingTimeEntry;
@end

@implementation ATStopWatchViewControllerCollection


#pragma mark - Public

+ (instancetype)sharedCollection {
    
    static dispatch_once_t once;
    static ATStopWatchViewControllerCollection *singleton;
    dispatch_once(&once, ^ {
        singleton = [[ATStopWatchViewControllerCollection alloc] init];
    });
    return singleton;

}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.stopWatchViewControllers = [[NSMutableArray alloc] init];
        self.stopWatchViewControllersQueue = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)hideStopwatchesAnimated:(BOOL)animated completion:(void (^)(BOOL finished))completion {
    [UIView animateWithDuration:0.25 animations:^{
        [self.mainStopWatchViewController.view moveTopToBottomWithMargin:0];
    } completion:completion];
}

- (void)showStopwatchesAnimated:(BOOL)animated completion:(void (^)(BOOL finished))completion {
    [UIView animateWithDuration:0.25 animations:^{
        [self.mainStopWatchViewController.view moveTopToBottomWithMargin:44];
    } completion:completion];
}

- (void)showInitialStopWatchViewController:(void (^)(ATStopWatchViewController *controller))configurationBlock completion:(void (^)())completionBlock {

    ATStopWatchViewController *stopWatchViewController = nil;
    
    if ([self.stopWatchViewControllersQueue count]) {
        
        stopWatchViewController = [self.stopWatchViewControllersQueue firstObject];
        [self.stopWatchViewControllersQueue removeObject:stopWatchViewController];
        
    } else {
        stopWatchViewController = [[ATStopWatchViewController alloc] init];
        stopWatchViewController.remoteDatasource = self.remoteDatasource;
    }
    
    self.mainStopWatchViewController = stopWatchViewController;
    
    [self.stopWatchViewControllers addObject:stopWatchViewController];
    
    stopWatchViewController.navigationDelegate = self;
    
    [self.parentContainerView addSubview:stopWatchViewController.view];
    
    [stopWatchViewController.view adjustHeightTo:self.parentContainerView.bounds.size.height + 50];
    [stopWatchViewController.view moveTopToBottomWithMargin:44];
    
    if (configurationBlock) configurationBlock(stopWatchViewController);
    
    
    [self.remoteDatasource checkForTrackingTimeEntrySuccess:^(id<ATTimeEntryProtocol> timeEntry) {
       
        if (timeEntry) {
            self.currentTrackingTimeEntry = timeEntry;
            
            [stopWatchViewController configureWithTimeEntry:timeEntry];
            [stopWatchViewController showTimerDigits];
        }
        
        if (completionBlock) completionBlock();
        
    } failure:^(NSError *error) {
        
    }];
}

- (void)showStopWatchViewControllerWithTimeEntry:(NSObject <ATTimeEntryProtocol> *)timeEntry configure:(void (^)(ATStopWatchViewController *controller))configurationBlock completion:(void (^)())completionBlock {
    
    
    ATStopWatchViewController *stopWatchViewController = nil;
    
    if (![self.mainStopWatchViewController isTracking] || [timeEntry isEqual:self.mainStopWatchViewController.timeEntry]) {
        stopWatchViewController = self.mainStopWatchViewController;
    }
    
    if (!stopWatchViewController) {
     
        if ([self.stopWatchViewControllersQueue count]) {
            
            stopWatchViewController = [self.stopWatchViewControllersQueue firstObject];
            [self.stopWatchViewControllersQueue removeObject:stopWatchViewController];
            
        } else {
            stopWatchViewController = [[ATStopWatchViewController alloc] init];
            stopWatchViewController.remoteDatasource = self.remoteDatasource;
        }
        
        [UIView animateWithDuration:0.25 animations:^{
            [self.mainStopWatchViewController.view moveTopToBottomWithMargin:0];
        }];
        
        
    }

    [self.stopWatchViewControllers addObject:stopWatchViewController];

    stopWatchViewController.navigationDelegate = self;

    [self.parentContainerView addSubview:stopWatchViewController.view];
    
    [stopWatchViewController.view adjustHeightTo:self.parentContainerView.bounds.size.height + 50];
    [stopWatchViewController.view adjustY:self.parentContainerView.bounds.size.height];

    
    if (configurationBlock) configurationBlock(stopWatchViewController);
    
    [stopWatchViewController showAnimatedWithCompletion:completionBlock];

}

#pragma mark - ATStopWatchViewController Navigation Delegate


- (void)stopWatchViewControllerWillClose:(ATStopWatchViewController *)stopWatchViewController withSpeed:(float)speed {
    
    
    if (![stopWatchViewController isEqual:self.mainStopWatchViewController]) {
        [UIView animateWithDuration:0.25 delay:0.5 options:0 animations:^{
            
            [self.mainStopWatchViewController.view moveTopToBottomWithMargin:44];
            
        } completion:^(BOOL finished) {
            
        }];
    }

    [stopWatchViewController hideWithSpeed:speed completion:^{
        
        if (![stopWatchViewController isTracking] && ![stopWatchViewController isEqual:self.mainStopWatchViewController]) {
            
            [self.stopWatchViewControllersQueue addObject:stopWatchViewController];
            [self.stopWatchViewControllers removeObject:stopWatchViewController];
            
        }
        
    }];

}

- (BOOL)stopWatchViewControllerShouldHideCompletely:(ATStopWatchViewController *)stopWatchViewController {
   
    if ([stopWatchViewController isTracking] || [stopWatchViewController isEqual:self.mainStopWatchViewController]) {
        return NO;
    } else 
    return YES;
}

- (void)stopWatchViewControllerWillAddNewTimeEntry:(ATStopWatchViewController *)stopWatchViewController {
    
    id <ATTimeEntryProtocol> timeEntry  = [self.remoteDatasource insertNewTimeEntry];

    [self showStopWatchViewControllerWithTimeEntry:timeEntry configure:^(ATStopWatchViewController *controller) {
        [controller configureWithTimeEntry:timeEntry];
    } completion:^{
        
    }];
    
}

- (void)stopWatchViewController:(ATStopWatchViewController *)stopWatchViewController willStartTimeEntry:(id<ATTimeEntryProtocol>)timeEntry success:(void (^)())success failure:(void (^)(NSError *error))failure {
    
    
    [self.currentTrackingTimeEntry stopTimer];
    self.currentTrackingTimeEntry = timeEntry;
    
    
    if (![stopWatchViewController isEqual:self.mainStopWatchViewController]) {
        [self.stopWatchViewControllersQueue addObject:self.mainStopWatchViewController];
        [self.mainStopWatchViewController.view removeFromSuperview];
        [self.stopWatchViewControllers removeObject:self.mainStopWatchViewController];
        
        self.mainStopWatchViewController = stopWatchViewController;
        [self.stopWatchViewControllers addObject:self.mainStopWatchViewController];
    }
    
    
    [self.remoteDatasource savelyStartTimeEntry:timeEntry success:^{
        
        if (success) success();
        
    } failure:^(NSError *error) {
        
        if (failure) failure(error);
    
    }];
    
}

- (void)stopWatchViewController:(ATStopWatchViewController *)stopWatchViewController willStopTimeEntry:(id<ATTimeEntryProtocol>)timeEntry success:(void (^)())success failure:(void (^)(NSError *))failure {
    
    
    [self.remoteDatasource stopTimeEntry:timeEntry success:^{
        
        if (success) success();
        
    } failure:^(NSError *error) {
        
        if (failure) failure(error);
        
    }];
    
}

- (void)stopWatchViewControllerWillShowMenu:(ATStopWatchViewController *)stopWatchViewController {
    __strong id <ATStopWatchViewControllerCollectionDelegate> delegate = self.delegate;
    if ([delegate respondsToSelector:@selector(stopWatchViewControllerCollectionWillShowMenu:)]) {
        [delegate stopWatchViewControllerCollectionWillShowMenu:self];
    }
}


@end
