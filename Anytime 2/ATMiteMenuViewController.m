//
//  ATMenuViewController.m
//  anytime
//
//  Created by Josef Materi on 26.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATMiteMenuViewController.h"
#import "ATDarkTableViewCell.h"
#import "UIImage+Tint.h"
#import "ATProjectsViewController.h"

@interface ATMiteMenuViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation ATMiteMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.title = NSLocalizedString(@"Menu", nil);
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.rowHeight = 44;
    
    [self.view addSubview:self.tableView];
    
    [self.tableView registerClass:[ATDarkTableViewCell class] forCellReuseIdentifier:@"Cell"];
 
    [self.navigationController setToolbarHidden:YES animated:NO];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ATDarkTableViewCell *cell = (ATDarkTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
 
    cell.backgroundColor = [UIColor clearColor];
    cell.textLabel.textColor = [UIColor whiteColor];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            cell.textLabel.text = NSLocalizedString(@"Dashboard", nil);
            cell.imageView.image = [[UIImage imageNamed:@"dashboard"] imageTintedWithColor:[UIColor whiteColor]];
        }
        
        if (indexPath.row == 1) {
            cell.textLabel.text = NSLocalizedString(@"Time entries", nil);
            cell.imageView.image = [[UIImage imageNamed:@"calendar"] imageTintedWithColor:[UIColor whiteColor]];
        }
        
        if (indexPath.row == 2) {
            cell.textLabel.text = NSLocalizedString(@"Reports", nil);
            cell.imageView.image = [[UIImage imageNamed:@"reports"] imageTintedWithColor:[UIColor whiteColor]];
            
        }
    }
    
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            cell.textLabel.text = NSLocalizedString(@"Customers", nil);
            cell.imageView.image = [[UIImage imageNamed:@"dashboard"] imageTintedWithColor:[UIColor whiteColor]];
        }
        
        if (indexPath.row == 1) {
            cell.textLabel.text = NSLocalizedString(@"Projects", nil);
            cell.imageView.image = [[UIImage imageNamed:@"calendar"] imageTintedWithColor:[UIColor whiteColor]];
        }
        
        if (indexPath.row == 2) {
            cell.textLabel.text = NSLocalizedString(@"Services", nil);
            cell.imageView.image = [[UIImage imageNamed:@"reports"] imageTintedWithColor:[UIColor whiteColor]];
            
        }
    }

    
    return cell;
}
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UIViewController *viewController = nil;
    
    if (indexPath.row == 0) {
        
        viewController = self.calendarViewController;
        
    } else {
        
        if (!self.mainCategoryViewController) {
            ATProjectsViewController *con = [[ATProjectsViewController alloc] initWithStyle:UITableViewStyleGrouped];
            con.categoryDatasource = self.remoteDatasource;
            con.title = @"Projekte";
            self.mainCategoryViewController = con;
        }
        
        viewController = self.mainCategoryViewController;
    }


    __strong id <ATMenuViewControllerDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(menuViewController:didSelectViewController:)]) {
        [delegate menuViewController:self didSelectViewController:viewController];
    }
    
}

- (ATCalendarWithTimeEntriesViewController *)calendarViewController {
    if (!_calendarViewController) {
        self.calendarViewController =  [[ATCalendarWithTimeEntriesViewController alloc] initWithNibName:Nil bundle:nil];
    }
    return _calendarViewController;
}

@end
