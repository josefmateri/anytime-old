//
//  JMBackgroundView.h
//  Anytime
//
//  Created by Josef Materi on 19.09.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMBackgroundView : UIView

@property (nonatomic, strong) UIImage *backgroundImage;

@property (nonatomic, assign) UIRectCorner radiiCorners;

@property (nonatomic, strong) UIColor *fillColor;
@property (nonatomic, strong) UIColor *strokeColor;
@property (nonatomic, strong) UIColor *innerStrokeColor;
@property (nonatomic, assign) CGFloat innerStrokeWidth;

@property (nonatomic, assign) CGFloat cornerRadius;

@property (nonatomic, assign) CGSize shadowOffset;
@property (nonatomic, assign) CGFloat shadowBlur;
@property (nonatomic, strong) UIColor *shadowColor;


@property (nonatomic, strong) UIColor *gradientStartColor;
@property (nonatomic, strong) UIColor *gradientEndColor;

@property (nonatomic, assign, getter = isBackgroundImageHidden) BOOL backgroundImageHidden;
@end
