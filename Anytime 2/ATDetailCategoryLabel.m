//
//  ATDetailCategoryLabel.m
//  anytime
//
//  Created by Josef Materi on 06.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATDetailCategoryLabel.h"
#import <Masonry.h>

@interface ATDetailCategoryLabel()
@property (nonatomic, strong) NSLayoutConstraint *topConstraint;
@property (nonatomic, strong) NSLayoutConstraint *leftConstraint;
@property (nonatomic, strong) NSArray *verticalConstraints;
@property (nonatomic, strong) UIView *line;
@end

@implementation ATDetailCategoryLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.textColor = [UIColor colorWithWhite:0.1 alpha:1];
        
        self.firstCategoryLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.secondCategoryLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        
        self.firstCategoryLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:14];
        self.secondCategoryLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
        
        
        self.secondCategoryLabel.textColor = self.textColor;
        self.firstCategoryLabel.textColor = self.textColor;
        
        self.firstCategoryLabel.alpha = 0.5;
        
        self.firstCategoryLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.secondCategoryLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.firstCategoryLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self.firstCategoryLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        
        [self.secondCategoryLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        
        [self.secondCategoryLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self.secondCategoryLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        
        [self addSubview:_firstCategoryLabel];
        [self addSubview:_secondCategoryLabel];
        // Initialization code
        
        self.contentInsets = UIEdgeInsetsMake(0, 10, 0, 10);

//        self.line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 1)];
//
//        [self addSubview:self.line];
//        self.line.backgroundColor = [UIColor colorWithWhite:0 alpha:0.4];
//        
//        [self.line makeConstraints:^(MASConstraintMaker *make) {
//            make.width.equalTo(self.width);
//            make.height.equalTo(@(1));
//            make.centerX.equalTo(self.centerX);
//            make.centerY.equalTo(self.centerY);
//        }];
    }
    return self;
}


- (void)setContentInsets:(UIEdgeInsets)contentInsets {
    _contentInsets = contentInsets;
    [self updateLayout];
}

- (void)updateLayout {
    
    if (self.topConstraint) {
        [self removeConstraints:@[self.leftConstraint, self.topConstraint]];
        [self removeConstraints:self.verticalConstraints];
    }
   
    
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:self.firstCategoryLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:self.contentInsets.top];
    
    self.topConstraint = topConstraint;
    [self addConstraint:topConstraint];
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:self.firstCategoryLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:self.contentInsets.left];
    
    self.leftConstraint = left;
    
    [self addConstraint:left];
    
    NSArray *verticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_firstCategoryLabel]-(0)-[_secondCategoryLabel]" options:NSLayoutFormatAlignAllLeft metrics:nil views:NSDictionaryOfVariableBindings(_firstCategoryLabel,_secondCategoryLabel)];
    
    [self addConstraints:verticalConstraints];
    
    self.verticalConstraints = verticalConstraints;
    
    [self layoutIfNeeded];
    
}

- (void)setFirstCategory:(NSString *)firstCategory secondCategory:(NSString *)secondCategory {
    
    self.firstCategoryLabel.text = firstCategory;
    self.secondCategoryLabel.text = secondCategory;
    
    [self updateLayout];
    
}

- (void)setTextColor:(UIColor *)textColor {
    _textColor = textColor;
    
    [[self subviews] enumerateObjectsUsingBlock:^(UILabel *obj, NSUInteger idx, BOOL *stop) {
        if ([obj respondsToSelector:@selector(setTextColor:)]) {
            obj.textColor = textColor;
        }
    }];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
