//
//  NSObject+Delay.m
//  Anytime
//
//  Created by Josef Materi on 24.04.11.
//  Copyright 2011 materi design. All rights reserved.
//

#import "NSObject+Delay.h"


@implementation NSObject (PWObject)

- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay
{
    int64_t delta = (int64_t)(1.0e9 * delay);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), dispatch_get_main_queue(), block);
}

- (void)performBlock:(void (^)(void))block queue:(dispatch_queue_t)queue afterDelay:(NSTimeInterval)delay
{
    int64_t delta = (int64_t)(1.0e9 * delay);
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, delta), queue, block);
}

@end