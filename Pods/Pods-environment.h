
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 1
#define COCOAPODS_VERSION_MINOR_AFNetworking 3
#define COCOAPODS_VERSION_PATCH_AFNetworking 3

// Block-KVO
#define COCOAPODS_POD_AVAILABLE_Block_KVO
#define COCOAPODS_VERSION_MAJOR_Block_KVO 2
#define COCOAPODS_VERSION_MINOR_Block_KVO 2
#define COCOAPODS_VERSION_PATCH_Block_KVO 1

// Masonry
#define COCOAPODS_POD_AVAILABLE_Masonry
#define COCOAPODS_VERSION_MAJOR_Masonry 0
#define COCOAPODS_VERSION_MINOR_Masonry 2
#define COCOAPODS_VERSION_PATCH_Masonry 4

// REFrostedViewController
#define COCOAPODS_POD_AVAILABLE_REFrostedViewController
#define COCOAPODS_VERSION_MAJOR_REFrostedViewController 2
#define COCOAPODS_VERSION_MINOR_REFrostedViewController 2
#define COCOAPODS_VERSION_PATCH_REFrostedViewController 1

// RestKit
#define COCOAPODS_POD_AVAILABLE_RestKit
#define COCOAPODS_VERSION_MAJOR_RestKit 0
#define COCOAPODS_VERSION_MINOR_RestKit 20
#define COCOAPODS_VERSION_PATCH_RestKit 3

// RestKit/Core
#define COCOAPODS_POD_AVAILABLE_RestKit_Core
#define COCOAPODS_VERSION_MAJOR_RestKit_Core 0
#define COCOAPODS_VERSION_MINOR_RestKit_Core 20
#define COCOAPODS_VERSION_PATCH_RestKit_Core 3

// RestKit/CoreData
#define COCOAPODS_POD_AVAILABLE_RestKit_CoreData
#define COCOAPODS_VERSION_MAJOR_RestKit_CoreData 0
#define COCOAPODS_VERSION_MINOR_RestKit_CoreData 20
#define COCOAPODS_VERSION_PATCH_RestKit_CoreData 3

// RestKit/Network
#define COCOAPODS_POD_AVAILABLE_RestKit_Network
#define COCOAPODS_VERSION_MAJOR_RestKit_Network 0
#define COCOAPODS_VERSION_MINOR_RestKit_Network 20
#define COCOAPODS_VERSION_PATCH_RestKit_Network 3

// RestKit/ObjectMapping
#define COCOAPODS_POD_AVAILABLE_RestKit_ObjectMapping
#define COCOAPODS_VERSION_MAJOR_RestKit_ObjectMapping 0
#define COCOAPODS_VERSION_MINOR_RestKit_ObjectMapping 20
#define COCOAPODS_VERSION_PATCH_RestKit_ObjectMapping 3

// RestKit/Support
#define COCOAPODS_POD_AVAILABLE_RestKit_Support
#define COCOAPODS_VERSION_MAJOR_RestKit_Support 0
#define COCOAPODS_VERSION_MINOR_RestKit_Support 20
#define COCOAPODS_VERSION_PATCH_RestKit_Support 3

// SOCKit
#define COCOAPODS_POD_AVAILABLE_SOCKit
#define COCOAPODS_VERSION_MAJOR_SOCKit 1
#define COCOAPODS_VERSION_MINOR_SOCKit 1
#define COCOAPODS_VERSION_PATCH_SOCKit 0

// TransformerKit
#define COCOAPODS_POD_AVAILABLE_TransformerKit
#define COCOAPODS_VERSION_MAJOR_TransformerKit 0
#define COCOAPODS_VERSION_MINOR_TransformerKit 2
#define COCOAPODS_VERSION_PATCH_TransformerKit 4

// TransitionKit
#define COCOAPODS_POD_AVAILABLE_TransitionKit
#define COCOAPODS_VERSION_MAJOR_TransitionKit 1
#define COCOAPODS_VERSION_MINOR_TransitionKit 1
#define COCOAPODS_VERSION_PATCH_TransitionKit 1

