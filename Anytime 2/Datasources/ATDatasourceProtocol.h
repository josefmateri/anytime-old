//
//  ATDatasourceProtocol.h
//  anytime
//
//  Created by Josef Materi on 29.09.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@protocol ATTimeEntryProtocol, ATCategoryDatasourceProtocol;

@protocol ATDatasourceProtocol <NSObject, ATCategoryDatasourceProtocol>

- (NSManagedObjectContext *)mainQueueManagedObjectContext;

#pragma mark - Time Entries

- (NSFetchRequest *)fetchRequestForTimeEntriesWithDateComponents:(NSDateComponents *)dateComponents;

- (NSFetchRequest *)fetchRequestForTimeEntriesWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate;

- (void)getTimeEntriesWithDateComponents:(NSDateComponents *)dateComponents complete:(void (^)(NSArray *results))completeBlock failed:(void (^)(NSError *failed))failedBlock;

- (void)postTimeEntry:(id <ATTimeEntryProtocol>)timeEntry success:(void (^)())success failure:(void (^)(NSError *error))failure;

- (void)deleteTimeEntries:(NSSet *)timeEntries progess:(void (^)(float progress))progress completion:(void (^)())completion failed:(void (^)()) failed;

- (id <ATTimeEntryProtocol>)insertNewTimeEntry;

#pragma mark - Timer

- (void)createAndStartTimeEntry:(id <ATTimeEntryProtocol>) timeEntry success:(void (^)())success failure:(void (^)(NSError *error))failure;

- (void)savelyStartTimeEntry:(id <ATTimeEntryProtocol>)timeEntry success:(void (^)())success failure:(void (^)(NSError *error))failure;

- (void)startTimeEntry:(id <ATTimeEntryProtocol>)timeEntry success:(void (^)())success failure:(void (^)(NSError *error))failure;

- (void)stopTimeEntry:(id <ATTimeEntryProtocol>)timeEntry success:(void (^)())success failure:(void (^)(NSError *error))failure;

- (void)checkForTrackingTimeEntrySuccess:(void (^)(id <ATTimeEntryProtocol> timeEntry))success failure:(void (^)(NSError *error))error;

#pragma mark - Projects

- (NSFetchRequest *)fetchRequestForProjects;

- (void)getProjectsComplete:(void (^)(NSArray *results))completeBlock failed:(void (^)(NSError *failed))failedBlock;

@end
