//
//  ATMiteServiceConfiguration.h
//  anytime
//
//  Created by Josef Materi on 28.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATServiceConfiguration.h"

@interface ATMiteServiceConfiguration : NSObject <ATServiceConfigurationProtocol>

@end
