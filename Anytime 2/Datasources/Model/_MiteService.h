// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MiteService.h instead.

#import <CoreData/CoreData.h>


extern const struct MiteServiceAttributes {
	__unsafe_unretained NSString *created_at;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *remote_id;
	__unsafe_unretained NSString *updated_at;
} MiteServiceAttributes;

extern const struct MiteServiceRelationships {
} MiteServiceRelationships;

extern const struct MiteServiceFetchedProperties {
} MiteServiceFetchedProperties;







@interface MiteServiceID : NSManagedObjectID {}
@end

@interface _MiteService : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MiteServiceID*)objectID;





@property (nonatomic, strong) NSDate* created_at;



//- (BOOL)validateCreated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* remote_id;



@property int64_t remote_idValue;
- (int64_t)remote_idValue;
- (void)setRemote_idValue:(int64_t)value_;

//- (BOOL)validateRemote_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* updated_at;



//- (BOOL)validateUpdated_at:(id*)value_ error:(NSError**)error_;






@end

@interface _MiteService (CoreDataGeneratedAccessors)

@end

@interface _MiteService (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveCreated_at;
- (void)setPrimitiveCreated_at:(NSDate*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSNumber*)primitiveRemote_id;
- (void)setPrimitiveRemote_id:(NSNumber*)value;

- (int64_t)primitiveRemote_idValue;
- (void)setPrimitiveRemote_idValue:(int64_t)value_;




- (NSDate*)primitiveUpdated_at;
- (void)setPrimitiveUpdated_at:(NSDate*)value;




@end
