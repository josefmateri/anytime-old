//
//  ATButtonGestureRecognizer.m
//  anytime
//
//  Created by Josef Materi on 20.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATButtonGestureRecognizer.h"
@interface ATButtonGestureRecognizer ()

@property (nonatomic, copy) ATTouchDownInsideBlock touchDownInsideBlock;
@property (nonatomic, copy) ATTouchUpInsideBlock touchUpInsideBlock;
@property (nonatomic, copy) ATTouchCancledBlock touchCancledBlock;

@property (nonatomic) CGPoint touchDownInsidePoint;
@end

@implementation ATButtonGestureRecognizer

+(instancetype) recognizerForView:(UIView *)view touchDownInside:(void (^)())touchDownInsideBlock touchUpInside:(void (^)())touchUpInsideBlock touchCanceld:(void (^)())touchCancledBlock {
 
    ATButtonGestureRecognizer *longPress = [[ATButtonGestureRecognizer alloc] init];
    [longPress addTarget:longPress action:@selector(handleLongPress:)];
    
    longPress.touchDownInsideBlock = touchDownInsideBlock;
    longPress.touchUpInsideBlock = touchUpInsideBlock;
    longPress.touchCancledBlock = touchCancledBlock;
    
    longPress.minimumPressDuration = 0;
    
    [view addGestureRecognizer:longPress];
    
    return longPress;
}

- (void)addToView:(UIView *)view touchDownInside:(void (^)())touchDownInsideBlock touchUpInside:(void (^)())touchUpInsideBlock {
    [view addGestureRecognizer:self];
    self.minimumPressDuration = 0;
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)longPress {
    if (longPress.state == UIGestureRecognizerStateBegan) {
        
        self.touchDownInsidePoint = [longPress locationInView:longPress.view];
        
        if (self.touchDownInsideBlock) {
            self.touchDownInsideBlock();
        }
    }
    
    if (longPress.state == UIGestureRecognizerStateChanged) {
        CGPoint currentPoint = [longPress locationInView:longPress.view];
        if (currentPoint.y < -30 || currentPoint.y > longPress.view.bounds.size.height + 30) {
            [longPress setEnabled:NO];
            [longPress setEnabled:YES];
        }
    }
    
    if (longPress.state == UIGestureRecognizerStateCancelled) {
        if (self.touchCancledBlock) {
            self.touchCancledBlock();
        }        
    }
    
    if (longPress.state == UIGestureRecognizerStateEnded) {
        
        if (self.touchUpInsideBlock) {
            self.touchUpInsideBlock();
        }
    }
}

@end
