//
//  ATDatasources.m
//  anytime
//
//  Created by Josef Materi on 29.09.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATDatasources.h"
#import "ATMiteDatasource.h"

@interface ATDatasources()
@property (nonatomic, strong) id <ATDatasourceProtocol> currentDatasource;
@end

@implementation ATDatasources

+ (instancetype)sharedDatasources {
    
    static dispatch_once_t once;
    static ATDatasources *singleton;
    dispatch_once(&once, ^ {
        singleton = [[ATDatasources alloc] init];
        singleton.currentDatasource = [ATMiteDatasource sharedDatasource];
    });
    return singleton;
    
}

+ (id <ATDatasourceProtocol>)currentDatasource {
    return [[ATDatasources sharedDatasources] currentDatasource];
}

@end
