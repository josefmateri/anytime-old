//
//  ATTimeEntriesListViewController.h
//  Anytime 2
//
//  Created by Josef Materi on 23.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ATListViewController.h"

@class NSManagedObject;



@interface ATTimeEntriesListViewController : ATListViewController

@property (nonatomic, strong) NSNumber *index;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSDateComponents *dateComponents;


- (void)reloadData;
- (void)refreshData;

@end


