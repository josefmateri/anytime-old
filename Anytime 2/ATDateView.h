//
//  ATDateView.h
//  Anytime 2
//
//  Created by Josef Materi on 10.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ATDateView : UIView

@property (nonatomic, strong) UILabel *weekDayLabel;
@property (nonatomic, strong) UILabel *dayLabel;
@property (nonatomic, strong) UILabel *monthLabel;

@property (nonatomic, strong) UILabel *yearLabel;

@property (nonatomic, strong) UILabel *secondMonthLabel;
@property (nonatomic, strong) UILabel *secondYearLabel;

@property (nonatomic, strong) UIColor *textColor;

@property (nonatomic) UIEdgeInsets contentInsets;

- (void) setSecondMonthLabelAlpha:(CGFloat)alpha;

- (void) updateWithDateComponents:(NSDateComponents *)component;

@end
