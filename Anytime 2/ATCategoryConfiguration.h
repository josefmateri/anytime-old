//
//  ATCategoryConfiguration.h
//  anytime
//
//  Created by Josef Materi on 19.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATListViewController.h"
#import "ATCategoryProtocol.h"

@interface ATCategoryConfiguration : NSObject

@property (nonatomic, strong) NSDictionary *icons;

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *shortDescription;

@property (nonatomic, strong) ATListViewController *listViewController;

@property (nonatomic, strong) id <ATCategoryProtocol> modelObject;


+(instancetype)categoryWithName:(NSString *)name shortDescription:(NSString *)shortDescription icons:(NSDictionary *)icons listViewController:(ATListViewController *)listViewController modelObject:(id <ATCategoryProtocol>)modelObject;

@end
