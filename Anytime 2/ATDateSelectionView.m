//
//  ATDatePresentationView.m
//  Anytime 2
//
//  Created by Josef Materi on 07.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATDateSelectionView.h"
#import "ATCalendarViewController.h"
#import "UIView+Position.h"
#import "ATCalenderMonthViewController.h"
#import "ATWeekCollectionViewController.h"
#import "ATCalendarWeekHeaderView.h"
#import "UIView+FLKAutoLayout.h"
#import "DMLazyScrollView.h"
#import "UIImage+Tint.h"
#import "UIView+Positioning.h"
#import "ATDescriptiveDateComponents.h"
#import <Masonry.h>

@interface ATDateSelectionView () <UICollisionBehaviorDelegate, ATCalendarViewDelegate>

@property (nonatomic, strong) UIButton *handle;
@property (nonatomic, strong) UIDynamicAnimator *animator;

@property (nonatomic, strong) UIToolbar *toolbar;
@property (nonatomic, strong) UIToolbar *topToolBar;
@property (nonatomic, strong) UISegmentedControl *dateScopeSegmentationControl;
@property (nonatomic) float topPadding;

@property (nonatomic, strong) UILabel *calendarTitleLabel;

@property (nonatomic, strong) DMLazyScrollView *monthScollView;

@property (nonatomic, strong) NSLayoutConstraint *currentWeekTopConstraint;
@property (nonatomic, strong) NSLayoutConstraint *dateLabelTopConstraint;
@property (nonatomic, strong) NSLayoutConstraint *currentMonthTopConstraint;
@property (nonatomic, strong) NSMutableDictionary *monthViewControllers;

@property (nonatomic, strong) ATCalenderMonthViewController *calenderMonthViewController;
@property (nonatomic, strong) ATCalendarViewController *calenderWeekViewController;
@property (nonatomic, strong) ATCalendarWeekHeaderView *calenderWeekHeaderView;

@property (nonatomic, strong) UINavigationBar *bottomToolbar;

@property (nonatomic) NSInteger numberOfCurrentWeek;
@property (nonatomic) DateSelectionViewState presentationState;

@property (nonatomic, strong) UIButton *editButton;

@property (nonatomic, strong) UIButton *monthButton;
@end

@implementation ATDateSelectionView

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if (self) {
		self.topPadding = frame.size.height;
        
		self.initialHeight = frame.size.height;
        
        self.backgroundColor = [UIColor clearColor];

		self.monthViewControllers = [NSMutableDictionary dictionary];
        
		self.monthScollView = [[DMLazyScrollView alloc] initWithFrameAndDirection:CGRectMake(0, 0, self.bounds.size.width, 300) direction:DMLazyScrollViewDirectionHorizontal circularScroll:YES];
        
		self.monthScollView.controlDelegate = self;
        
		__weak __typeof(& *self) weakSelf = self;
        
		self.monthScollView.dataSource = ^(NSUInteger index) {
			return [weakSelf monthCollectionViewAtIndex:index];
		};
        
		self.monthScollView.numberOfPages = 3;
        
		[self addSubview:self.monthScollView];
        
		self.monthScollView.translatesAutoresizingMaskIntoConstraints = NO;
        
		UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 400)];
		toolBar.frame = self.bounds;
		toolBar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		toolBar.barTintColor = [UIColor colorWithRed:0.041 green:0.210 blue:0.421 alpha:1.000];
        toolBar.barTintColor = [toolBar.barTintColor  colorWithAlphaComponent:0.67];
		toolBar.translatesAutoresizingMaskIntoConstraints = NO;
		self.toolbar = toolBar;
		[self addSubview:toolBar];
        
        
        self.bottomToolbar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 20, self.bounds.size.width, 44)];
        
        //[self.bottomToolbar setItems:@[listButton, flex, editItem]];
        self.bottomToolbar.backgroundColor = [UIColor clearColor];
        self.bottomToolbar.barTintColor = [UIColor clearColor];
        
        [self.bottomToolbar setBackgroundImage:[UIImage imageNamed:@"navbar-bg"]  forBarMetrics:UIBarMetricsDefault];
        
        [self.bottomToolbar setShadowImage:[UIImage imageNamed:@"navbar-bg"]];
        
        self.bottomToolbar.tintColor = [UIColor whiteColor];
        for (UIView *subview in [self.bottomToolbar subviews]) {
            
            if ([subview isKindOfClass:[UIImageView class]] && subview.frame.size.height < 20) {
                
                [subview removeFromSuperview];
                
            }
            
        }
        
        [self addSubview:self.bottomToolbar];
        
		[self addConstraints:[toolBar constrainWidth:[@(self.bounds.size.width)description]]];
		[self addConstraints:[toolBar alignAttribute:NSLayoutAttributeTop toAttribute:NSLayoutAttributeTop ofView:self predicate:@"0"]];
        
		[self addConstraints:[toolBar alignAttribute:NSLayoutAttributeBottom toAttribute:NSLayoutAttributeBottom ofView:self predicate:@""]];
        
        
		UIPanGestureRecognizer *panning = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didPan:)];
		[self addGestureRecognizer:panning];
		self.clipsToBounds = YES;
    
        
        
//		self.dateLabel = [[ATDateLabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 30)];
//		self.dateLabel.translatesAutoresizingMaskIntoConstraints = NO;
//        
//		[self addSubview:self.dateLabel];
//        
//		[self.dateLabel makeConstraints: ^(MASConstraintMaker *make) {
//		    make.centerX.equalTo(self.centerX);
//		    make.height.equalTo(@(44));
//		}];
//
//        self.dateLabelTopConstraint = [NSLayoutConstraint constraintWithItem:self.dateLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:20];
//        [self addConstraint:self.dateLabelTopConstraint];
//        
//        
//        UITapGestureRecognizer *dateLabelTapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateLabelTapped:)];
//        [self.dateLabel addGestureRecognizer:dateLabelTapped];
        
        self.calenderWeekHeaderView = [[ATCalendarWeekHeaderView alloc] initWithFrame:CGRectMake(0, 0, 100, 10)];
        self.calenderWeekHeaderView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:self.calenderWeekHeaderView];
        [self.calenderWeekHeaderView makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@(64));
            make.width.equalTo(self.width);
            make.height.equalTo(@(10));
            make.centerX.equalTo(self.centerX);
        }];
        
		self.calenderWeekViewController = [[ATCalendarViewController alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 60) date:[NSDate date]];
		[self addSubview:self.calenderWeekViewController];
		self.calenderWeekViewController.translatesAutoresizingMaskIntoConstraints = NO;
        self.calenderWeekViewController.delegate = self;
        
		[self.calenderWeekViewController makeConstraints: ^(MASConstraintMaker *make) {
		    make.width.equalTo(self.width);
		    make.height.equalTo(@(60));
		    make.centerX.equalTo(self.centerX);
		}];

        self.currentWeekTopConstraint = [NSLayoutConstraint constraintWithItem:self.calenderWeekViewController attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.calenderWeekHeaderView attribute:NSLayoutAttributeBottom multiplier:1 constant:5];
        [self addConstraint:self.currentWeekTopConstraint];
        
		[self addSubview:self.monthScollView];
		[self addConstraints:[self.monthScollView constrainWidth:[@(self.bounds.size.width)description]]];
		[self addConstraints:[self.monthScollView constrainHeight:[@(300)description]]];
        
        self.currentMonthTopConstraint = [NSLayoutConstraint constraintWithItem:self.monthScollView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.calenderWeekViewController attribute:NSLayoutAttributeTop multiplier:1 constant:0];

        [self addConstraint:self.currentMonthTopConstraint];
        
		UIView *border = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1)];
		border.backgroundColor = [UIColor colorWithRed:0.109 green:0.149 blue:0.195 alpha:.2000];
		[self addSubview:border];
        
		border.translatesAutoresizingMaskIntoConstraints = NO;
        //
		NSLayoutConstraint *borderConstraint = [NSLayoutConstraint constraintWithItem:border attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
		[self addConstraint:borderConstraint];
        
		borderConstraint = [NSLayoutConstraint constraintWithItem:border attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1 constant:0];
		[self addConstraint:borderConstraint];
        
		[self addConstraints:[border constrainHeight:@"1"]];
        
        
        
        
        UIButton *arrowDown = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        [arrowDown setImage:[UIImage imageNamed:@"arrow_down"] forState:UIControlStateNormal];
        [arrowDown sizeToFit];
        arrowDown.translatesAutoresizingMaskIntoConstraints = NO;
        arrowDown.showsTouchWhenHighlighted = NO;
        arrowDown.backgroundColor = [UIColor clearColor];
//        [arrowDown addTarget:self action:@selector(toggleHandle:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:arrowDown];
        
        //  arrowDown.transform = CGAffineTransformMakeScale(0.5, 0.5);
        self.handle = arrowDown;
        
        NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:arrowDown attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
        [self addConstraint:constraint];
        
        constraint = [NSLayoutConstraint constraintWithItem:arrowDown attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:-5];
        [self addConstraint:constraint];
     
            //
        
        self.handle.userInteractionEnabled = NO;

        
        [self updateLayout];

       
	}
	return self;
}

- (void)setTransparentBackground:(BOOL)transparent {
	[self.toolbar setHidden:transparent];
	if (transparent) {
		self.backgroundColor = [UIColor clearColor];
	}
}

- (UIBarButtonItem *)editButtonItem {
    UIBarButtonItem *editItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit", Nil) style:UIBarButtonItemStyleBordered target:self action:@selector(edit:)];
    
    return editItem;
}

- (UIBarButtonItem *)doneButtonItem {
    UIBarButtonItem *editItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit", Nil) style:UIBarButtonItemStyleDone target:self action:@selector(done:)];
    
    return editItem;
}

#pragma mark - Gesture Recognizers

- (void)dateLabelTapped:(UITapGestureRecognizer *)dateLabelTapped {

    if (self.presentationState == DateSelectionViewStateOpen) {
        [self expand:NO animated:YES];
    } else {
        [self expand:YES animated:YES];
    }
 }


#pragma mark - UINavigationBar

- (void)reloadNavigationItems {
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 33)];
    [button setImage:[UIImage imageNamed:@"navbar-icon-back"] forState:UIControlStateNormal];
    
    [button setBackgroundColor:[UIColor clearColor]];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];

    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
    [button addTarget:self action:@selector(show) forControlEvents:UIControlEventTouchUpInside];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.monthButton = button;
    
    [self configureMonthButtonWithDateComponents:self.dateComponents];
    
    UIBarButtonItem *monthItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    

    
    __strong id <ATDateSelectionViewDatasource> datasource = self.datasource;
    if ([datasource respondsToSelector:@selector(navigationItemsForDataSelectionView:)]) {
        
        UINavigationItem *item = [datasource navigationItemsForDataSelectionView:self];
        item.leftBarButtonItem = monthItem;
        
        [self.bottomToolbar pushNavigationItem:item
                                      animated:NO];
        
    }
    
}

- (void)updateButton {
    if (self.presentationState == DateSelectionViewStateOpen) {
        self.monthButton.imageView.alpha = 0;
    } else {
        self.monthButton.imageView.alpha = 1;
    }
}

- (void)configureMonthButtonWithDateComponents:(NSDateComponents *)dateComponents {
    ATDescriptiveDateComponents *descriptiveDateComponents = [ATDescriptiveDateComponents descriptiveDateComponentsFromDateComponents:dateComponents];
     [self.monthButton setTitle:[NSString stringWithFormat:@"%@ %@", descriptiveDateComponents.month, descriptiveDateComponents.year] forState:UIControlStateNormal];
    [self updateButton];
}

#pragma mark - Manual date navigation

- (void)goToNextDay {
   self.dateComponents = [self.calenderWeekViewController goToNextDay];
    
    
    [self.dateLabel updateWithDateComponents:self.dateComponents];
}

- (void)goToPrevDay {
    self.dateComponents = [self.calenderWeekViewController goToPrevDay];

    [self.dateLabel updateWithDateComponents:self.dateComponents];
}

- (void)updateMonthView {
    [self configureCurrentCalenderMonthWithDateComponents:self.dateComponents];
}

#pragma mark - user interaction

- (void)edit:(UIBarButtonItem *)edit {

    __strong id <ATDateSelectionViewDelegate> delegate = self.delegate;
    
	if ([delegate respondsToSelector:@selector(dateSelectionView:setEditing:)]) {
		[delegate dateSelectionView:self setEditing:YES];
	}
    
}

- (void)didToggleMenu {
    __strong id <ATDateSelectionViewDelegate> delegate = self.delegate;
    
	if ([delegate respondsToSelector:@selector(dateSelectionViewDidToggleMenu:)]) {
		[delegate dateSelectionViewDidToggleMenu:self];
	}
}

- (void)updateLayout {
	float y = self.frame.size.height;
    
	float backgroundAlpha = y / 576.0f;
	if (backgroundAlpha > 0.6) backgroundAlpha = 0.6;
    
	self.backgroundColor = [UIColor colorWithWhite:0 alpha:backgroundAlpha];

    float offset = 5;
	
//    self.bottomToolbar.alpha = (y - self.initialHeight) / (576 - self.initialHeight);

    float moved = y - self.initialHeight;
    
    float goal = self.numberOfCurrentWeek * 45 + offset;
    float current = moved;

    float fraction = current / goal;
    
    if (current + offset < goal) {
        self.currentWeekTopConstraint.constant = current + offset;
    } else {
        self.currentWeekTopConstraint.constant = goal;
    }
    
   ATCalenderMonthViewController *currentMonthController = (ATCalenderMonthViewController *)[self.monthScollView currentViewController];
   currentMonthController.view.alpha = fraction;

    
}

- (void)didMoveToPoint:(CGPoint)p withState:(UIGestureRecognizerState)state andVelocity:(CGPoint)velo {
    
    if (p.y < self.initialHeight) return;
    
    CGRect rect = self.frame;
    rect.size.height = p.y;
    self.frame = rect;
    
    [self updateLayout];
    
    if (state == UIGestureRecognizerStateEnded) {
        
        
        if (velo.y > 0) {
            
            [self slideDownWithSpeed:MIN(5,(int)velo.y / 100)];
            
        } else {
            
            [self slideDownWithSpeed: MAX((int)velo.y / 100, -5)];
            
            
        }
        
    }
    
    if (state == UIGestureRecognizerStateBegan) {
        [self setWeekViewHidden:NO];
    }
    
}

- (void)didPan:(UIPanGestureRecognizer *)pan {
    
    [self.animator removeAllBehaviors];
    
    CGPoint p = [pan locationInView:[[pan view] superview]];
    
    CGPoint velo = [pan velocityInView:[pan view]];
    
    UIGestureRecognizerState state =pan.state;
    
    [self didMoveToPoint:p withState:state andVelocity:velo];
}

- (void)removeAllAnimators {
    [self.animator removeAllBehaviors];
    
}

- (void)toggleHandle:(UIButton *)handleButton {
    [self slideDownWithSpeed:5.0f];
}

- (void)show {
    [self slideDownWithSpeed:5.0f];
}


- (void)slideDownWithSpeed:(float)speed  {

    if (speed == 0) speed = -1;
    
    if (speed > 0) {
        self.presentationState = DateSelectionViewStateWillOpen;
    } else {
        self.presentationState = DateSelectionViewStateWillClose;
    }
    
    UIGravityBehavior *dynamicBehavior = [[UIGravityBehavior alloc] initWithItems:@[self.handle]];
    dynamicBehavior.gravityDirection = CGVectorMake(0, speed);
    
    UICollisionBehavior *coll = [[UICollisionBehavior alloc] initWithItems:@[self.handle]];
    coll.collisionMode = UICollisionBehaviorModeBoundaries;
    coll.collisionDelegate = self;
    [coll setTranslatesReferenceBoundsIntoBoundaryWithInsets:UIEdgeInsetsMake(self.initialHeight - self.handle.bounds.size.height - 5, 0, 0, 0)];

    UIDynamicAnimator *animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.superview];
    [animator addBehavior:dynamicBehavior];
    [animator addBehavior:coll];
    
    UIDynamicItemBehavior *behav = [[UIDynamicItemBehavior alloc] initWithItems:@[self.handle]];
    behav.elasticity =0;
    
    [animator addBehavior:behav];
    
    if (speed > 0) self.handle.alpha = 0;
    else self.handle.alpha = 1;
    
    dynamicBehavior.action = ^{
        
        
        CGRect rect = self.frame;
        rect.size.height = self.handle.frame.origin.y + self.handle.frame.size.height + 5;
        
        self.frame = rect;
        
        [self updateLayout];
        
    };
    
    
    self.animator = animator;
}


#pragma mark - Month Calendar

- (ATCalenderMonthViewController *)monthCollectionViewAtIndex:(NSInteger)index {
	ATCalenderMonthViewController *calendarMonthViewController = self.monthViewControllers[@(index % 3)];
	if (!calendarMonthViewController) {
		UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
		layout.itemSize = CGSizeMake(40, 40);
		layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5);
		layout.minimumInteritemSpacing = 5.0f;
		layout.minimumLineSpacing = 5.0f;
		layout.scrollDirection = UICollectionViewScrollDirectionVertical;
        
        
		NSDateComponents *calenderMonth = [[NSDateComponents alloc] init];
		calenderMonth.month = 7 + index;
		calenderMonth.day = 1;
		calenderMonth.year = 2013;
        
        
		calendarMonthViewController = [[ATCalenderMonthViewController alloc] initWithCollectionViewLayout:layout];
		[calendarMonthViewController setDateWithComponents:calenderMonth];
        
		[calendarMonthViewController.collectionView reloadData];
        
		calendarMonthViewController.delegate = self;
        
		self.monthViewControllers[@(index)] = calendarMonthViewController;
	}
    
	return calendarMonthViewController;
}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView willChangeToNextPageWithViewController:(UIViewController *)viewController beforeViewController:(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController {
    ATCalenderMonthViewController *current = (ATCalenderMonthViewController *)viewController;
	NSDateComponents *component = current.calenderMonth;
    
	if (component.month == self.dateComponents.month && component.year == self.dateComponents.year) {
		[current highlightCellForDateComponent:self.dateComponents];
	}
}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView willChangeToPrevPageWithViewController:(UIViewController *)viewController beforeViewController:(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController {
    ATCalenderMonthViewController *current = (ATCalenderMonthViewController *)viewController;
	NSDateComponents *component = current.calenderMonth;
    
	if (component.month == self.dateComponents.month && component.year == self.dateComponents.year) {
		[current highlightCellForDateComponent:self.dateComponents];
	}
}


- (void)lazyScrollView:(DMLazyScrollView *)pagingView didChangeToViewController:(UIViewController *)viewController beforeViewController:(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController {
   
    [self didChangeToPage:(ATCalenderMonthViewController *)viewController after:(ATCalenderMonthViewController *)afterViewController before:(ATCalenderMonthViewController *)beforeViewController];

}

- (void)didChangeToPage:(ATCalenderMonthViewController *)currentViewController after:(ATCalenderMonthViewController *)afterViewController before:(ATCalenderMonthViewController *)beforeViewController {
    
	ATCalenderMonthViewController *current = (ATCalenderMonthViewController *)currentViewController;
	NSDateComponents *component = current.calenderMonth;
    
    if (component.month == self.dateComponents.month) {
        [current highlightCellForDateComponent:self.dateComponents];
    }
    
	NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:component];
    
	NSDateComponents *addMonth = [[NSDateComponents alloc] init];
	addMonth.month = 1;
	NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:addMonth toDate:date options:0];
	NSDateComponents *nextMonth = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay fromDate:newDate];
    nextMonth.day = 1;
	
    addMonth.month = -1;
	newDate = [[NSCalendar currentCalendar] dateByAddingComponents:addMonth toDate:date options:0];
	NSDateComponents *prevMonth = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitDay fromDate:newDate];
	prevMonth.day = 1;
    ATCalenderMonthViewController *after = (ATCalenderMonthViewController *)afterViewController;
    after.calenderMonth = nil;
    [after setDateWithComponents:nextMonth];
    [after deselectAllHighlightedDays];
    
    ATCalenderMonthViewController *before = (ATCalenderMonthViewController *)beforeViewController;
    before.calenderMonth = nil;
    [before setDateWithComponents:prevMonth];
    [before deselectAllHighlightedDays];
    
    [self configureMonthButtonWithDateComponents:component];
    
//    if (component.month != self.dateComponents.month) {
//        [self controller:current didSeletDayWithDateComponent:component];
//    }
}

- (void)controller:(ATCalenderMonthViewController *)controller didSeletDayWithDateComponent:(NSDateComponents *)component {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *date = [calendar dateFromComponents:component];
    
	self.dateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekday | NSCalendarUnitDay | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear fromDate:date];
	
    [self.dateLabel updateWithDateComponents:self.dateComponents];
    //
    
    [self.calenderWeekViewController setCurrentDate:date];
    
    [self.calenderWeekViewController updateWithDateComponents:self.dateComponents];
    
    [self.calenderWeekViewController setCurrentDayHighlighted:YES];
    
    self.numberOfCurrentWeek = [controller lineOfCurentSelectedItem];
    
    if (self.presentationState == DateSelectionViewStateOpen) {
        self.currentWeekTopConstraint.constant = self.numberOfCurrentWeek * 45 + 5;
        self.currentMonthTopConstraint.constant = -45 * self.numberOfCurrentWeek;       
    }
   
	__strong id <ATDateSelectionViewDelegate> delegate = self.delegate;
    
	if ([delegate respondsToSelector:@selector(dateSelectionView:didSelectDateWithComponents:)]) {
		[delegate dateSelectionView:self didSelectDateWithComponents:component];
	}

}

- (void)calendarMonthViewController:(ATCalenderMonthViewController *)controller didSelectDate:(NSDateComponents *)component {
	
    [self controller:controller didSeletDayWithDateComponent:component];
    
}

#pragma mark - Date configuration

- (void)setCurrentDate:(NSDate *)date {
	
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
	self.dateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekday | NSCalendarUnitDay | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear fromDate:date];
	
    [self.dateLabel updateWithDateComponents:self.dateComponents];

    [self configureCurrentCalenderWeekWithDateComponents:self.dateComponents];
	[self configureCurrentCalenderMonthWithDateComponents:self.dateComponents];
}

- (void)updateWithDateComponents:(NSDateComponents *)components {
	NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDate *date = [calendar dateFromComponents:components];
	[self setCurrentDate:date];
}

- (void)configureCurrentCalenderWeekWithDateComponents:(NSDateComponents *)dateComponents {

    NSCalendar *calendar = [NSCalendar currentCalendar];
	NSDate *date = [calendar dateFromComponents:dateComponents];
    
    self.calenderWeekViewController.currentDate = date;

    [self.calenderWeekViewController reloadData];

    [self.calenderWeekViewController setCurrentDayHighlighted:YES];
}

- (void)configureCurrentCalenderMonthWithDateComponents:(NSDateComponents *)dateComponents {
	
    ATCalenderMonthViewController *current = (ATCalenderMonthViewController *)[self.monthScollView currentViewController];
    
    [current setDateWithComponents:dateComponents];
    
    [self.monthScollView reloadData];
    
    self.monthScollView.userInteractionEnabled = NO;
    self.numberOfCurrentWeek = [current lineOfCurentSelectedItem];
    
    self.currentMonthTopConstraint.constant = -45 * self.numberOfCurrentWeek;
}

#pragma mark - Week Calendar View Delegate

- (void)calenderViewController:(ATCalendarViewController *)calendarViewController didChangeToWeek:(NSDate *)date {
    
}


- (void)calenderViewController:(ATCalendarViewController *)calendarViewController didSelectDateWithDateComponents:(NSDateComponents *)dateComponents {
    
    self.dateComponents = dateComponents;
    
    [self configureCurrentCalenderMonthWithDateComponents:dateComponents];

    [self.dateLabel updateWithDateComponents:dateComponents];
    
    __strong id <ATDateSelectionViewDelegate> delegate = self.delegate;
    
	if ([delegate respondsToSelector:@selector(dateSelectionView:didSelectDateWithComponents:)]) {
		[delegate dateSelectionView:self didSelectDateWithComponents:dateComponents];
	}
    
}

- (void)calenderViewController:(ATCalendarViewController *)calendarViewController willChangeToNextWeek:(NSDate *)date {

}

- (void)calenderViewController:(ATCalendarViewController *)calendarViewController willChangeToPrevWeek:(NSDate *)date {
    
}


- (void)expand:(BOOL)expand animated:(BOOL)animated {
    
    if (expand) {
    
        if (self.presentationState != DateSelectionViewStateOpen) {
            [self slideDownWithSpeed:10];
            
            self.dateLabel.imageView.image = [[UIImage imageNamed:@"arrow_sans_up"] imageTintedWithColor:[UIColor whiteColor]];
        }
    
        
    } else {
        
        if (self.presentationState != DateSelectionViewStateClosed) {
            [self slideDownWithSpeed:-10];
            
            self.dateLabel.imageView.image = [[UIImage imageNamed:@"arrow_sans_down"] imageTintedWithColor:[UIColor whiteColor]];
            
        }
    
    }
}

#pragma mark - UICollisionBehavior


- (void)collisionBehavior:(UICollisionBehavior*)behavior endedContactForItem:(id <UIDynamicItem>)item withBoundaryIdentifier:(id <NSCopying>)identifier {
    
    if (self.presentationState == DateSelectionViewStateWillOpen) {
        self.presentationState = DateSelectionViewStateOpen;
    }
    
    if (self.presentationState == DateSelectionViewStateWillClose) {
        self.presentationState = DateSelectionViewStateClosed;
    }
    
    __strong id <ATDateSelectionViewDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(dateSelectionViewDidBeginBouncing:)]) {
        [delegate dateSelectionViewDidBeginBouncing:self];
    }

}

- (void)setPresentationState:(DateSelectionViewState)presentationState {
    _presentationState = presentationState;
    
    [self updateButton];
    
    switch (presentationState) {
        case DateSelectionViewStateOpen:
            [self setWeekViewHidden:YES];
            break;
        case DateSelectionViewStateWillClose:
            [self setWeekViewHidden:NO];
        default:
            break;
    }
}

- (void)collisionBehavior:(UICollisionBehavior *)behavior endedContactForItem:(id<UIDynamicItem>)item1 withItem:(id<UIDynamicItem>)item2 {
    
    
}



- (void)setWeekViewHidden:(BOOL)hidden {

    self.monthScollView.userInteractionEnabled = hidden;
    [self.calenderWeekViewController setHidden:hidden];

    
    ATCalenderMonthViewController *current = (ATCalenderMonthViewController *)[self.monthScollView currentViewController];
   [current setLineForSelectedDayHidden:!hidden];
    
}

@end

