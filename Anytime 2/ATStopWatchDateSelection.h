//
//  ATStopWatchDateSelection.h
//  Anytime 2
//
//  Created by Josef Materi on 23.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATDateView.h"
#import "ATTouchableView.h"

@protocol ATStopWatchDateSelection;


@interface ATStopWatchDateSelection : ATTouchableView

@property (nonatomic, weak) id <ATStopWatchDateSelection> delegate;
@property (nonatomic, strong) ATDateView *dateView;

- (void)setMonthLabelAlpha:(float)alpha;
- (void)configureWithDateComponents:(NSDateComponents *)comp;

- (void)highlightCurrentDayInMonthView;

- (void)setBottomBorderColor:(UIColor *)bottomBorderColor;

- (void)setMonthCalendarHidden:(BOOL)hidden;

@end

@protocol ATStopWatchDateSelection <NSObject>

- (void)stopWatchDateSelection:(ATStopWatchDateSelection *)selectionView didSelectDateWithComponents:(NSDateComponents *)components;

@end