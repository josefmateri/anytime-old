//
//  DMLazyScrollView.h
//  Lazy Loading UIScrollView for iOS
//
//  Created by Daniele Margutti (me@danielemargutti.com) on 24/11/12.
//  Copyright (c) 2012 http://www.danielemargutti.com. All rights reserved.
//  Distribuited under MIT License
//

#import <UIKit/UIKit.h>

@class DMLazyScrollView;

enum {
    DMLazyScrollViewDirectionHorizontal =   0,
    DMLazyScrollViewDirectionVertical   =   1,
};typedef NSUInteger DMLazyScrollViewDirection;

enum {
    DMLazyScrollViewTransitionAuto      =   0,
    DMLazyScrollViewTransitionForward   =   1,
    DMLazyScrollViewTransitionBackward  =   2
}; typedef NSUInteger DMLazyScrollViewTransition;

@protocol DMLazyScrollViewDelegate <NSObject>
@optional

- (void)lazyScrollViewWillBeginDragging:(DMLazyScrollView *)pagingView;
- (void)lazyScrollViewDidScroll:(DMLazyScrollView *)pagingView at:(CGPoint) visibleOffset;
- (void)lazyScrollViewDidEndDragging:(DMLazyScrollView *)pagingView;
- (void)lazyScrollViewWillBeginDecelerating:(DMLazyScrollView *)pagingView;
- (void)lazyScrollViewDidEndDecelerating:(DMLazyScrollView *)pagingView atPageIndex:(NSInteger)pageIndex;
- (void)lazyScrollView:(DMLazyScrollView *)pagingView currentPageChanged:(NSInteger)currentPageIndex;
- (void)lazyScrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset;

- (void)lazyScrollView:(DMLazyScrollView *)pagingView didChangeToViewController:(UIViewController *)viewController beforeViewController:(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController;

- (void)lazyScrollView:(DMLazyScrollView *)pagingView willChangeToNextPageWithViewController:(UIViewController *)viewController beforeViewController:(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController;

- (void)lazyScrollView:(DMLazyScrollView *)pagingView willChangeToPrevPageWithViewController:(UIViewController *)viewController beforeViewController:(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController;


- (void)lazyScrollView:(DMLazyScrollView *)pagingView didChangeToNextPageWithViewController:(UIViewController *)viewController beforeViewController:(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController;

- (void)lazyScrollView:(DMLazyScrollView *)pagingView didChangeToPrevPageWithViewController:(UIViewController *)viewController beforeViewController:(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController;


@end

typedef UIViewController*(^DMLazyScrollViewDataSource)(NSUInteger index);

@interface DMLazyScrollView : UIScrollView

@property (copy)                DMLazyScrollViewDataSource      dataSource;
@property (nonatomic, assign)   id<DMLazyScrollViewDelegate>    controlDelegate;

@property (nonatomic,assign)    NSUInteger                      numberOfPages;
@property (readonly)            NSUInteger                      currentPage;
@property (readonly)            DMLazyScrollViewDirection       direction;
@property (nonatomic, assign)   CGPoint    currentPageOffset;


- (id)initWithFrameAndDirection:(CGRect)frame
                      direction:(DMLazyScrollViewDirection)direction
                 circularScroll:(BOOL) circularScrolling;


- (void) reloadData;


- (void) setPage:(NSInteger) index animated:(BOOL) animated;
- (void) setPage:(NSInteger) newIndex transition:(DMLazyScrollViewTransition) transition animated:(BOOL) animated;
- (void) moveByPages:(NSInteger) offset animated:(BOOL) animated;

- (CGPoint)moveVector;
- (NSInteger) pageIndexByAdding:(NSInteger) offset from:(NSInteger) index;

- (UIViewController *) visibleViewController;
- (UIViewController *)currentViewController ;

- (UIViewController *)previousViewController;
- (UIViewController *)nextViewController;

- (void)goToNextPage;
- (void)goToPreviousPage;

@end
