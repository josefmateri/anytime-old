//
//  MiteDateTransformers.m
//  anytime
//
//  Created by Josef Materi on 12.09.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "MiteDateTransformers.h"
#import "NSValueTransformer+TransformerKit.h"

#include <time.h>
#include <xlocale.h>

NSString * const MiteDateTransformerDateOnly = @"MiteDateTransformerDateOnly";

static NSString * MiteDateTransformerDateOnlyFromDate(NSDate *date) {
    static NSDateFormatter *_miteDateFormatterFromDate = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _miteDateFormatterFromDate = [[NSDateFormatter alloc] init];
        [_miteDateFormatterFromDate setDateFormat:@"YYYY-MM-dd"];
//        [_iso8601DateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    });
    
    return [_miteDateFormatterFromDate stringFromDate:date];
}

static NSDate * MiteDateTransformerDateOnlyFromTimeStamp(NSString *timestamp) {
    static NSDateFormatter *_miteDateFormatterFromTimeStamp = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _miteDateFormatterFromTimeStamp = [[NSDateFormatter alloc] init];
        [_miteDateFormatterFromTimeStamp setDateFormat:@"YYYY-MM-dd"];
//        [_iso8601DateFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    });
    
    return [_miteDateFormatterFromTimeStamp dateFromString:timestamp];
}

@implementation MiteDateTransformers

+ (void)load {
    @autoreleasepool {
        [NSValueTransformer registerValueTransformerWithName:MiteDateTransformerDateOnly transformedValueClass:[NSString class] returningTransformedValueWithBlock:^id(id value) {
            return MiteDateTransformerDateOnlyFromTimeStamp(value);
        } allowingReverseTransformationWithBlock:^id(id value) {
            return MiteDateTransformerDateOnlyFromDate(value);
        }];
    }
}

@end