//
//  TimeEntryViewContainer.h
//  Anytime
//
//  Created by Josef Materi on 21.10.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeEntryView.h"
#import "JMKeyboardViewController.h"

typedef enum {
    TimeEntryKeyboardModeDigits,
    TimeEntryKeyboardModeAssist
} TimeEntryKeyboardMode;

@protocol TimeEntryViewContainerDelegate;

@interface TimeEntryViewContainer : UIView <JMKeyboardViewDatasource, JMKeyboardViewDelegate, UIGestureRecognizerDelegate, TimeEntryViewDelegate>
@property (nonatomic, weak) id <TimeEntryViewContainerDelegate> delegate;
@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, assign, readonly) TimeEntryKeyboardMode keyboardMode;

- (void)stopEditingWithCompletionHandler:(void (^)())completionHandler;
- (void)startEditingWithCompletionHandler:(void (^)())completionHandler;


- (void)setBottomBorderColor:(UIColor *)bottomBorderColor;

@end

@protocol TimeEntryViewContainerDelegate <NSObject>

- (void)timeEntryViewContainerDidTapOnFrom:(TimeEntryViewContainer *)timeEntryView;
- (void)timeEntryViewContainerDidTapOnTo:(TimeEntryViewContainer *)timeEntryView;
- (void)timeEntryViewContainerDidFinishEditing:(TimeEntryViewContainer *)timeEntryView;

@end