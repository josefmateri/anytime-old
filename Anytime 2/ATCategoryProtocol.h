//
//  ATCategoryProtocol.h
//  anytime
//
//  Created by Josef Materi on 19.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ATCategoryProtocol <NSObject>

- (NSString *)title;

- (NSNumber *)categoryIdentifier;

@end
