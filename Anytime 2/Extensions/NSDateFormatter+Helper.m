//
//  NSDateFormatter+Helper.m
//  Anytime 2
//
//  Created by Josef Materi on 13.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "NSDateFormatter+Helper.h"

@implementation NSDateFormatter (Helper)

- (NSArray *)reginalWeekdaySymbols {
    
    NSInteger shiftValue = self.calendar.firstWeekday - 1;
    
    NSMutableArray *normalWeekdaySymbols = [[self weekdaySymbols] mutableCopy];
    
    for (int i = 0; i < shiftValue; i++) {
        NSString *first = [normalWeekdaySymbols firstObject];
        [normalWeekdaySymbols removeObject:first];
        [normalWeekdaySymbols addObject:first];
    }
    
    return [NSArray arrayWithArray:normalWeekdaySymbols];
}

- (NSArray *)reginalShortWeekdaySymbols {
    
    NSInteger shiftValue = self.calendar.firstWeekday - 1;
    
    NSMutableArray *normalWeekdaySymbols = [[self shortWeekdaySymbols] mutableCopy];
    
    for (int i = 0; i < shiftValue; i++) {
        NSString *first = [normalWeekdaySymbols firstObject];
        [normalWeekdaySymbols removeObject:first];
        [normalWeekdaySymbols addObject:first];
    }
    
    return [NSArray arrayWithArray:normalWeekdaySymbols];
}

@end
