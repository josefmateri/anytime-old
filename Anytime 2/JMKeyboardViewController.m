//
//  JMKeyboardViewController.m
//  Anytime
//
//  Created by Josef Materi on 01.10.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import "JMKeyboardViewController.h"
#import "JMKeyboardViewActionCell.h"

#define kKeySize CGSizeMake(318/4, 216 / 4)

@interface JMKeyboardViewController ()

@end

@implementation JMKeyboardViewController

- (id)initWithCollectionViewLayout:(UICollectionViewFlowLayout *)layout {
    self = [super initWithCollectionViewLayout:layout];
    if (self) {
        // Custom initialization
        
        self.view.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.view.clipsToBounds = YES;
        
        [self.collectionView registerClass:[JMKeyboardViewCell class] forCellWithReuseIdentifier:@"Cell"];

        [self.collectionView registerClass:[JMKeyboardViewActionCell class] forCellWithReuseIdentifier:@"ActionCell"];
        
        
        self.collectionView.backgroundColor = [UIColor clearColor];
        

    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.collectionView.translatesAutoresizingMaskIntoConstraints = NO;
    self.collectionView.delaysContentTouches = NO;
    
    NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:320];
    
    [self.view addConstraint:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:self.view attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:216];
    ;
    [self.view addConstraint:constraint];

    
    constraint = [NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeHeight multiplier:1 constant:0];
    ;
    [self.view addConstraint:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeWidth multiplier:1 constant:0];
    ;
    [self.view addConstraint:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:0];
    
    [self.view addConstraint:constraint];
    
    constraint = [NSLayoutConstraint constraintWithItem:self.collectionView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
    
    [self.view addConstraint:constraint];

    [self.collectionView reloadData];
    
    self.collectionView.userInteractionEnabled = YES;
      
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UICollectionViewController

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 12;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *identifier = @"Cell";
    if (indexPath.row == 12) identifier = @"ActionCell";
    
    JMKeyboardViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];

    cell.imageView.image = nil;
    cell.textLabel.text = nil;
    
    if ([_datasource respondsToSelector:@selector(keyboardViewController:configureItem:atIndexPath:)]) {
        
        [_datasource keyboardViewController:self configureItem:cell atIndexPath:indexPath];
        
    } else {
        cell.textLabel.text = [@(indexPath.row) description];
    }
    
    return cell;
}


//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.row == 12) {
//        return CGSizeMake(320, 216 / 4);
//    } else {
//        return kKeySize;
//    }
//}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath {

}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([_delegate respondsToSelector:@selector(keyboardViewController:didSelectItemAtIndexPath:)]) {
        [_delegate keyboardViewController:self didSelectItemAtIndexPath:indexPath];
    }
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
}

@end
