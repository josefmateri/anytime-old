//
//  ATDetailCategoryLabel.h
//  anytime
//
//  Created by Josef Materi on 06.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ATDetailCategoryLabel : UIView


@property (nonatomic, strong) UILabel *firstCategoryLabel;
@property (nonatomic, strong) UILabel *secondCategoryLabel;

@property (nonatomic, strong) UIColor *textColor;

@property (nonatomic) UIEdgeInsets contentInsets;

- (void)setFirstCategory:(NSString *)firstCategory secondCategory:(NSString *)secondCategory;

@end
