//
//  NSObject+Delay.h
//  Anytime
//
//  Created by Josef Materi on 24.04.11.
//  Copyright 2011 materi design. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSObject (PWObject)

- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay;

- (void)performBlock:(void (^)(void))block queue:(dispatch_queue_t)queue afterDelay:(NSTimeInterval)delay;



@end


/* NSObject+PWObject.m */

