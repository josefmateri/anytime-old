//
//  JMKeyboardViewCell.h
//  Anytime
//
//  Created by Josef Materi on 02.10.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JMKeyboardViewCell : UICollectionViewCell

@property (nonatomic, strong, readonly) UILabel *textLabel;
@property (nonatomic, strong, readonly) UIImageView *imageView;

- (UIImageView *)customBackgroundImageView;

@end
