//
//  NSNumber+Time.m
//  Anytime
//
//  Created by Josef Materi on 07.10.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import "NSNumber+Time.h"

@implementation NSNumber (Time)

- (NSString *) durationValue {
	NSInteger hours = [self intValue] / 60;
	NSInteger minutes = abs([self intValue]) % 60;
	NSString *duration = [NSString stringWithFormat:@"%@:%@", [self formatHours: hours], [self formatTime: minutes]];
	return duration;
}

- (NSString *) formatHours:(int)time {
	return [[NSString alloc] initWithFormat: @"%02d", time];
}

- (NSString *) formatTime:(int) time {
    return [[NSString alloc] initWithFormat: @"%02d", time];
}

@end
