//
//  ATCategoryConfiguration.m
//  anytime
//
//  Created by Josef Materi on 19.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATCategoryConfiguration.h"

@implementation ATCategoryConfiguration

+(instancetype)categoryWithName:(NSString *)name shortDescription:(NSString *)shortDescription icons:(NSDictionary *)icons listViewController:(ATListViewController *)listViewController modelObject:(id <ATCategoryProtocol>)modelObject {
    
    ATCategoryConfiguration *conf = [[ATCategoryConfiguration alloc] init];
    conf.name = name;
    conf.shortDescription = shortDescription;
    conf.icons = icons;
    conf.listViewController = listViewController;
    conf.modelObject = modelObject;
    
    return conf;
    
}

@end
