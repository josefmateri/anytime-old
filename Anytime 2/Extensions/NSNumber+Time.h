//
//  NSNumber+Time.h
//  Anytime
//
//  Created by Josef Materi on 07.10.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Time)

- (NSString *) durationValue;
- (NSString *) formatHours:(int)time;
- (NSString *) formatTime:(int) time;

@end
