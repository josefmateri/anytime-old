//
//  ATDatasources.h
//  anytime
//
//  Created by Josef Materi on 29.09.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATDatasourceProtocol.h"

@interface ATDatasources : NSObject

+ (instancetype)sharedDatasources;
+ (id <ATDatasourceProtocol>)currentDatasource;

@end
