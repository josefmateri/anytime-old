//
//  ATDarkTableViewCell.m
//  anytime
//
//  Created by Josef Materi on 20.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATDarkTableViewCell.h"
@interface DebugView : UIView

@end
@implementation DebugView
- (void)setAlpha:(CGFloat)alpha {
    [super setAlpha:alpha];
}
- (void)setHidden:(BOOL)hidden {
    [super setHidden:hidden];
}
- (void)removeFromSuperview {
    [super removeFromSuperview];
}
- (void)setFrame:(CGRect)frame {
    [super setFrame:frame];
}
@end

@interface ATDarkTableViewCell()
@property (nonatomic, strong) UIView *bottomBorder;
@end

@implementation ATDarkTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
        self.selectedBackgroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];

        DebugView *line = [[DebugView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1)];
        line.backgroundColor = [UIColor colorWithRed:0.376 green:0.412 blue:0.447 alpha:1.000];
        [self.contentView addSubview:line];
        self.bottomBorder = line;
        [self.bottomBorder setHidden:YES];
        
        line.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        
    }
    return self;
}

- (void)setBorderHidden:(BOOL)hidden {
    [self.bottomBorder setHidden:hidden];
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    

}

- (void)setBottomBorderColor:(UIColor *)bottomBorderColor {
    self.bottomBorder.backgroundColor = bottomBorderColor;
}

- (void)registerTouchesBeganBlock:(void (^)())touchesBeganBlock touchesEndedBlock:(void (^)())touchesEndedBlock touchesMoved:(void (^)())touchesMovedBlock touchesCancled:(void (^)())touchesCancledBlock {
    self.touchesBeganBlock = touchesBeganBlock;
    self.touchesCancledBlock = touchesCancledBlock;
    self.touchesMovedBlock = touchesMovedBlock;
    self.touchesEndedBlock = touchesEndedBlock;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self setHighlighted:YES animated:YES];
    if (self.touchesBeganBlock) self.touchesBeganBlock();
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    if (self.touchesEndedBlock) self.touchesEndedBlock();
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    [self setHighlighted:NO animated:YES];
    [self setSelected:NO animated:YES];
    if (self.touchesCancledBlock) self.touchesCancledBlock();
}

@end
