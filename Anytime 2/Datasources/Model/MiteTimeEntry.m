#import "MiteTimeEntry.h"
#import "NSNumber+Time.h"

@interface MiteTimeEntry () <ATStopWatchDelegate>

// Private interface goes here.

@end


@implementation MiteTimeEntry

@synthesize stopWatch;


- (void)delete {
    [self.managedObjectContext deleteObject:self];
}

- (NSDate *)date {
    return self.date_at;
}

- (NSNumber *)durationInMinutes {
    return self.minutes;
}

- (void)setDurationInMinutes:(NSNumber *)minutes {
    self.minutes = minutes;
}

- (NSNumber *)timeEntryIdentifier {
    return self.remote_id;
}

- (NSNumber *)trackingDurationInMinutes {
    return self.trackingMinutes;
}

- (void)setTrackingDurationInMinutes:(NSNumber *)tracking {
    self.trackingMinutes = tracking;
}


- (void)stopTimer {
    
    [self.stopWatch stop];
    self.minutes = self.trackingMinutes;
    
    self.trackingMinutes = nil;
    self.trackingSince = nil;
    
}

- (void)startTimer {
    
    self.stopWatch = [[ATStopWatch alloc] init];
    
    self.stopWatch.delegate = self;
    
    [self.stopWatch startWithDate:self.trackingSince andRunningTime:[self.durationInMinutes integerValue] * 60];
    
}


#pragma mark - Categories

- (BOOL)hasSecondaryCategory {
    return YES;
}

- (id <ATCategoryProtocol>)mainCategory {
    return (id <ATCategoryProtocol>)self.project;
}

- (id <ATCategoryProtocol>)secondaryCategory {
    return (id <ATCategoryProtocol>)self.service;
}

- (void)setMainCategory:(id <ATCategoryProtocol>)mainCategory {
    
    self.project_id = [mainCategory categoryIdentifier];
    self.project_name = [mainCategory title];
    
    [self setProject:(MiteProject *)mainCategory];

}

- (void)setSecondaryCategory:(id <ATCategoryProtocol>)secondaryCategory {
    self.service_id = [secondaryCategory categoryIdentifier];
    self.service_name = [secondaryCategory title];
    
    [self setService:(MiteService *)secondaryCategory];
}

- (NSString *)mainCategoryTitle {
    if (self.project_name)
    return self.project_name;
    else return NSLocalizedString(@"No project", nil);
}

- (NSString *)secondaryCategoryTitle {
    if (self.service_name)
    return self.service_name;
    else return NSLocalizedString(@"No service", nil);
}




#pragma mark - Stop Watch

- (void)stopWatch:(ATStopWatch *)stopWatch didCountToRunningSeconds:(NSInteger)runningSeconds totalSeconds:(NSInteger)totalSeconds {
    
    //    NSInteger secondHour = runningSeconds / 60 / 60 % 10;
    //    NSInteger firstHour = runningSeconds / 60 / 60 / 10;
    //
    //
    //    NSInteger firstMinute = runningSeconds / 60 % 60 / 10;
    //	NSInteger  secondMinute= runningSeconds / 60 % 60 % 10;
    
    NSNumber *minutes = @(runningSeconds / 60);
    
    NSLog(@"%i %@", runningSeconds, [minutes durationValue]);
    
    if (![[self trackingDurationInMinutes] isEqual:minutes]) {
        [self setTrackingDurationInMinutes:minutes];
    }
    
    //    NSInteger firstSecond = runningSeconds % 60 / 10;
    //    NSInteger secondSecond = runningSeconds % 60 % 60 % 10;
    //
    //    NSLog(@"%i %i %i %i: %i%i", firstHour, secondHour, firstMinute, secondMinute, firstSecond, secondSecond);
}

- (void)awakeFromFetch {
    
    if (self.trackingSince) {
        [self startTimer];
    }
}

@end
