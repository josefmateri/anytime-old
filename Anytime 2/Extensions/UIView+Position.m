//
//  UIView+Position.m
//  Soccer
//
//  Created by Josef Materi on 08.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "UIView+Position.h"

@implementation UIView (Position)

- (void)alignRightWithMargin:(float)margin {
    
    CGRect frame = self.frame;
    frame.origin.x = self.superview.bounds.size.width - frame.size.width - margin;
    self.frame = frame;
    
}

- (CGRect)frameForRightAlignmentWithMargin:(float)margin {
    
    CGRect frame = self.frame;
    frame.origin.x = self.superview.bounds.size.width - frame.size.width - margin;
    return frame;
}

- (void)alignLeftWithMargin:(float)margin {
    
    CGRect frame = self.frame;
    frame.origin.x = margin;
    self.frame = frame;
    
}

- (CGRect)frameForLeftAlignmentWithMargin:(float)margin {
    
    CGRect frame = self.frame;
    frame.origin.x = margin;
    return frame;
}

- (void)alignBelowView:(UIView *)view withMargin:(float)margin {
    
    CGRect frame = self.frame;
    frame.origin.y = view.frame.origin.y + view.frame.size.height + margin;
    self.frame = frame;
}

- (void)moveFromFrame:(CGRect)frame toFrame:(CGRect)toFrame progress:(float)progress {
   
    float progressX = (toFrame.origin.x - frame.origin.x) * fabs(progress);
    
    CGRect newFrame = toFrame;
    newFrame.origin.x = frame.origin.x + progressX;
    
    self.frame = newFrame;
}

- (void)adjustY:(float)y {
    
    CGRect newFrame = self.frame;
    newFrame.origin.y = y;
    self.frame = newFrame;
    
}

@end
