//
//  ATMiteServiceConfiguration.m
//  anytime
//
//  Created by Josef Materi on 28.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATMiteServiceConfiguration.h"


#import "ATMiteDatasource.h"
#import "ATMiteMenuViewController.h"

@implementation ATMiteServiceConfiguration

- (id <ATDatasourceProtocol>)remoteDatasource {
    return [ATMiteDatasource sharedDatasource];
}

- (UIViewController *)menuViewController {
    return [[ATMiteMenuViewController alloc] initWithNibName:Nil bundle:nil];
}

@end
