//
//  ATDateLabel.m
//  anytime
//
//  Created by Josef Materi on 02.09.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATDateLabel.h"
#import <Masonry.h>
#import "ATDescriptiveDateComponents.h"
#import "UIImage+Tint.h"
@interface ATDateLabel ()

@property (nonatomic, strong) UILabel *dateLabel;
@end

@implementation ATDateLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        // Initialization code
        self.dateLabel = [[UILabel alloc] initWithFrame:frame];
        self.dateLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.dateLabel.textAlignment = NSTextAlignmentLeft;
        self.dateLabel.backgroundColor = [UIColor clearColor];
        self.dateLabel.textColor = [UIColor whiteColor];
        
       [self.dateLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
       [self.dateLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
       [self.dateLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
       [self.dateLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];

        self.dateLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:16];
        
        [self addSubview:self.dateLabel];
        
        
       self.imageView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"arrow_sans_down"] imageTintedWithColor:[UIColor whiteColor]]];

       [self addSubview:self.imageView];
        self.imageView.contentMode = UIViewContentModeCenter;
        self.imageView.translatesAutoresizingMaskIntoConstraints = NO;
        
        
       [self.imageView makeConstraints:^(MASConstraintMaker *make) {
           make.top.equalTo(self.dateLabel.bottom).offset(-3);
          make.centerX.equalTo(self.centerX);
       }];
        

        
        [self.dateLabel makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.centerX);
            make.centerY.equalTo(self.centerY);
        }];
        
        
        [self makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(self.dateLabel);
        }];


    }
    return self;
}


- (void) updateWithDateComponents:(NSDateComponents *)component {
    
    ATDescriptiveDateComponents *descriptiveDateComponents = [ATDescriptiveDateComponents descriptiveDateComponentsFromDateComponents:component];
    
    self.dateLabel.text = [NSString stringWithFormat:@"%@ %@",descriptiveDateComponents.month, descriptiveDateComponents.year];
    
    [self.dateLabel sizeToFit];
    
    [self layoutIfNeeded];
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
