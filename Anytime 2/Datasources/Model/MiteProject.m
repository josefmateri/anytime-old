#import "MiteProject.h"


@interface MiteProject ()

// Private interface goes here.

@end


@implementation MiteProject

// Custom logic goes here.

- (NSString *)title {
    return self.name;
}

- (NSNumber *)categoryIdentifier {
    return self.remote_id;
}

@end
