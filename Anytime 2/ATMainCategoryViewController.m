//
//  ATMainCategoryViewController.m
//  anytime
//
//  Created by Josef Materi on 06.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATMainCategoryViewController.h"
#import <Masonry.h>
#import "ATCategoryProtocol.h"
#import "ATDarkTableViewCell.h"
@interface ATMainCategoryViewController () <NSFetchedResultsControllerDelegate>

@end

@implementation ATMainCategoryViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self.tableView registerClass:[ATDarkTableViewCell class] forCellReuseIdentifier:@"CategoryCell"];
    [self.tableView registerClass:[UITableViewHeaderFooterView class] forHeaderFooterViewReuseIdentifier:@"Header"];
    
    self.tableView.rowHeight = 44;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorColor = [UIColor colorWithWhite:1 alpha:0.2];

    [self.navigationController setNavigationBarHidden:NO];
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)reloadData {
    
        NSFetchRequest *fetchRequest = [self.categoryDatasource fetchRequestForMainCategory];
    //
        fetchRequest.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES selector:@selector(compare:)]];
    //
        self.fetchedResultsController = nil;
    //
        NSManagedObjectContext *context = [[ATDatasources currentDatasource] mainQueueManagedObjectContext];
    //
        self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    //
        self.fetchedResultsController.delegate = self;
    //
        [self.fetchedResultsController performFetch:nil];
    //
        [self.tableView reloadData];
    //
    

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)refreshData {
    //
      // Add our descriptors to the manager
    id <ATCategoryDatasourceProtocol> datasource = self.categoryDatasource;
  
      [datasource getMainCategoryObjectsComplete:^(NSArray *results) {
          
          [self.refreshControl endRefreshing];
          
      } failed:^(NSError *failed) {
          [self.refreshControl endRefreshing];

      }];
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
    
    NSManagedObject <ATCategoryProtocol> *managedObject = [self.fetchedResultsController objectAtIndexPath:indexPath];

    cell.textLabel.text = [managedObject title];

    cell.textLabel.textColor = [UIColor whiteColor];
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    
//    UITableViewHeaderFooterView *view = [self.tableView dequeueReusableHeaderFooterViewWithIdentifier:@"Header"];
//   
//    view.backgroundColor = [UIColor clearColor];
//    
//    view.textLabel.text = [self tableView:tableView titleForHeaderInSection:section];
//    view.textLabel.textColor = [UIColor whiteColor];
//    view.backgroundView = [[UIView alloc] init];
//    
//    view.backgroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
//    
//    view.contentView.backgroundColor = [UIColor clearColor];
//    
//    view.textLabel.backgroundColor = [UIColor clearColor];
//    
//    return view;
//}
//
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    return @"Favorite Projects";
//}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}


@end
