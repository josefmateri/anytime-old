//
//  ATStopWatchViewController.m
//  Anytime 2
//
//  Created by Josef Materi on 13.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATStopWatchViewController.h"
#import "DigitsInputView.h"
#import "UIView+Positioning.h"
#import "ATPercentDrivenTransition.h"
#import "NSObject+Delay.h"
#import "UIView+FLKAutoLayout.h"
#import "TimeEntryViewContainer.h"
#import "ATDateSelectionView.h"
#import "ATDateView.h"
#import "UIImage+Tint.h"
#import "UIView+Animations.h"
#import "ATStopWatchDateSelection.h"
#import <Masonry.h>
#import "easing.h"
#import "UIView+Positioning.h"
#import "ATTimeEntryProtocol.h"
#import "NSNumber+Time.h"
#import "ATDateConfiguration.h"
#import "ATDetailCategoryLabel.h"
#import "ATCategorySelectionViewController.h"
#import "ATButtonGestureRecognizer.h"

@interface ATStopWatchViewController () <DigitsInputViewDatasource, DigitsInputViewDelegate, UIGestureRecognizerDelegate, UIDynamicAnimatorDelegate, TimeEntryViewContainerDelegate, ATStopWatchDateSelection, UIScrollViewDelegate, ATCategorySelectionViewControllerDelegate, ATCategorySelectionViewControllerDatasource>


@property (nonatomic, strong) DigitsInputView *digitsInputView;

@property (nonatomic, strong) UILabel *time;

@property (nonatomic) float start;

@property (nonatomic, strong) NSTimer *animationTimer;

@property (nonatomic, strong) UIView *digitsBackgroundView;

@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;

@property (nonatomic, strong) UIButton *timerActionButton;
@property (nonatomic, strong) UIButton *doneButton;

@property (nonatomic, strong) UIView *categorySelectionView;
@property (nonatomic, strong) TimeEntryViewContainer *timeEntryView;
@property (nonatomic, strong) ATStopWatchDateSelection *dateSelectionView;
@property (nonatomic, strong) NSLayoutConstraint *topContentLayoutConstraint;
@property (nonatomic) TimerButtonState previousTimerButtonState;
@property (nonatomic, strong) NSLayoutConstraint *timeEntryTopConstraint;
@property (nonatomic, strong) NSLayoutConstraint *timeEntryBottomConstraint;
@property (nonatomic, strong) NSLayoutConstraint *digitsInputViewTopConstraint;
@property (nonatomic, strong) NSLayoutConstraint *timerButtonBottomConstraint;
@property (nonatomic, strong) NSLayoutConstraint *doneButtonBottomConstraint;

@property (nonatomic, strong) NSLayoutConstraint *dateSelectionViewTopConstraint;
@property (nonatomic, strong) NSLayoutConstraint *dateSelectionViewBottomConstraint;

@property (nonatomic, strong) NSLayoutConstraint *categorySelectionViewTopConstraint;
@property (nonatomic, strong) NSLayoutConstraint *categorySelectionViewHeightConstraint;

@property (nonatomic, strong) NSLayoutConstraint *digitsInputViewBottomConstraint;

@property (nonatomic, strong) UIPanGestureRecognizer *panGesture;
@property (nonatomic, strong) ATButtonGestureRecognizer *dateSelectionTapGesture;
@property (nonatomic, strong) UIToolbar *bottomToolbar;
@property (nonatomic) float smallDigitScale;
@property (nonatomic) CGPoint smallDigitOffset;
@property (nonatomic) float timeEntryViewInitialYPosition;
@property (nonatomic, strong) UIButton *addTimeEntryButton;
@property (nonatomic, strong) UINavigationBar *topToolBar;
@property (nonatomic, strong) UIView *topBackgroundView;
@property (nonatomic, strong) UIPageControl *pageControl;
@property (nonatomic, strong) UIScrollView *contentScroller;
@property (nonatomic, strong) UIColor *startButtonTitleColor;
@property (nonatomic) float digitsTopPosition;
@property (nonatomic) float digitsNonEditingHeight;
@property (nonatomic) float digitsEditingHeight;
@property (nonatomic) float buttonHeight;
@property (nonatomic) float timeEntryViewHeight;
@property (nonatomic) float startButtonBottomConstraint;
@property (nonatomic) float digitsDefaultPosition;

@property (nonatomic, strong) UIToolbar *backgroundView;
@property (nonatomic, strong) UIColor *seperatorColor;
@property (nonatomic, strong) ATCategorySelectionViewController *categorySelectionViewController;
@end

@implementation ATStopWatchViewController


+ (instancetype)sharedStopwatchViewController {
    
    static dispatch_once_t once;
    static ATStopWatchViewController *singleton;
    dispatch_once(&once, ^ {
        singleton = [[ATStopWatchViewController alloc] initWithNibName:nil bundle:nil];
    });
    return singleton;
    
}

#pragma mark - Public

- (void)presentInViewController:(UIViewController *)viewController animated:(BOOL)animated completion:(void (^)())complete {
    
    UIView *parentView = viewController.view;
    [parentView addSubview:self.view];
    [self.view adjustY:parentView.bounds.size.height];
    
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:0 animations:^{
        [self.view adjustY:0];
    } completion:^(BOOL finished) {
        if (complete) complete();
    }];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
        
		self.backgroundImage = [[UIImageView alloc] initWithFrame:CGRectZero];
        
		self.initialBottomOffset = 44;
        
		self.smallDigitScale = 0.2;
		self.smallDigitOffset = CGPointMake(0, -25);
        
        self.digitsTopPosition = 120;
        self.buttonHeight = 72;
        
        self.digitsNonEditingHeight = 120;
        
        UIScreen *mainScreen = [UIScreen mainScreen];
        
        
        self.digitsDefaultPosition = self.digitsTopPosition - 60;
        
        self.digitsEditingHeight = mainScreen.bounds.size.height - self.digitsDefaultPosition;

        self.timeEntryViewHeight = 80;
        
        
	}
	return self;
}

#pragma mark - View lifcycle

- (UIStatusBarStyle)preferredStatusBarStyle {
	return UIStatusBarStyleLightContent;
}

- (void)viewDidLoad {
	[super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithWhite:0 alpha:0.1]; //[UIColor colorWithRed:0.041 green:0.210 blue:0.421 alpha:.0000];
	self.view.clipsToBounds = YES;
    
	[self.navigationController setNavigationBarHidden:YES];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
	self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0.216 green:0.318 blue:0.443 alpha:1.000];
	self.navigationController.navigationBar.translucent = NO;

	UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:self.view.bounds];
//	toolbar.barTintColor = [UIColor colorWithRed:0.345 green:0.447 blue:0.573 alpha:1.000];
    toolbar.barTintColor = [UIColor colorWithRed:0.041 green:0.210 blue:0.421 alpha:1.000];
    toolbar.barTintColor = [toolbar.barTintColor  colorWithAlphaComponent:0.67];

    [self.view addSubview:toolbar];
    
	toolbar.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.backgroundView = toolbar;
  
	UIView *background = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
	background.backgroundColor = [UIColor colorWithWhite:0 alpha:1];
    background.alpha = 0;
	background.autoresizingMask = UIViewAutoresizingFlexibleHeight;
	[self.view addSubview:background];
	self.topBackgroundView = background;
    
    
	self.digitsInputView = [[DigitsInputView alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.digitsNonEditingHeight)];
	self.digitsInputView.delegate = self;
	self.digitsInputView.datasource = self;
    self.digitsInputView.alpha = 0;
	[self.view addSubview:self.digitsInputView];
    
    self.digitsBackgroundView.backgroundColor = [UIColor greenColor];
    
    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
	[self.view addGestureRecognizer:panGesture];
	self.panGesture = panGesture;
	panGesture.delegate = self;
 
    self.contentScroller = [[UIScrollView alloc] initWithFrame:self.view.bounds];
	self.contentScroller.autoresizingMask = UIViewAutoresizingFlexibleHeight;
	[self.view addSubview:self.contentScroller];
    
	[self.contentScroller setContentSize:CGSizeMake(2000, self.view.bounds.size.height)];
	self.contentScroller.pagingEnabled = YES;
	self.contentScroller.delegate = self;
    
    [self configureStartButton];
    
    [self configureDoneButton];
    
	[self configureTimeEntryView];
    
    [self configureDateSelectionView];
    
	[self configurecategorySelectionView];
    
    [self configureTopToolbar];
    
    [self configurePageControl];
    
    self.seperatorColor = [UIColor colorWithWhite:1 alpha:0.1];

    [self.dateSelectionView setBottomBorderColor:self.seperatorColor];
    [self.timeEntryView setBottomBorderColor:self.seperatorColor];

  //  [self configureAddTimeEntry];
    
    self.digitsInputView.userInteractionEnabled = NO;
    
    
    [self.view bringSubviewToFront:self.digitsInputView];
    
//    [[ATMiteDatasource sharedDatasource] checkForTrackingTimeEntrySuccess:^(id <ATTimeEntryProtocol> response) {
//        
//        if (response) {
//            [self configureWithTimeEntry:response];
//        }
//        
//    } failure:^(NSError *error) {
//        
//    }];
    
    self.bottomToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
    
    
//    UIBarButtonItem *listItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"list-icon"] landscapeImagePhone:[UIImage imageNamed:@"list-icon"] style:UIBarButtonItemStyleBordered target:self action:@selector(list:)];
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 200, 100)];
    UIBarButtonItem *show = [[UIBarButtonItem alloc] initWithCustomView:button];
    [button addTarget:self action:@selector(show) forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *plusItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-plus"] landscapeImagePhone:[UIImage imageNamed:@"icon-plus"]  style:UIBarButtonItemStyleBordered target:self action:@selector(add:)];
    
    UIBarButtonItem *flexItem =  [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    
    
        UIBarButtonItem *moreItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"icon-list"] landscapeImagePhone:[UIImage imageNamed:@"icon-list"]  style:UIBarButtonItemStyleBordered target:self action:@selector(menu:)];
    
    [self.bottomToolbar setItems:@[moreItem, flexItem,show, flexItem, plusItem] animated:NO];
    self.bottomToolbar.barTintColor = [UIColor clearColor];
    [self.bottomToolbar setBackgroundImage:[UIImage imageNamed:@"navbar-bg"] forToolbarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    self.bottomToolbar.tintColor = [UIColor whiteColor];
    
    [self.bottomToolbar setShadowImage:[UIImage imageNamed:@"toolbar-shadow"] forToolbarPosition:UIBarPositionTop];
    
    [self.view addSubview:self.bottomToolbar];
}

- (void)resetPosition {
    [self.view moveTopToBottomWithMargin:44];
    [self.digitsInputView moveToInitialPosition];
}

- (void)showTimerDigits {
    self.digitsInputView.alpha = 1;
    self.digitsInputView.userInteractionEnabled = YES;
    [self.view bringSubviewToFront:self.digitsInputView];
}

- (void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
 
    self.digitsInputView.userInteractionEnabled = YES;

    __strong id <ATStopWatchViewControllerDelegate> delegate = self.delegate;

    if ([delegate respondsToSelector:@selector(stopWatchViewControllerDidAppear:)]) {
        [delegate stopWatchViewControllerDidAppear:self];
    }

}

- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
    
    [self.tapGesture setEnabled:NO];
    
	self.digitsInputView.userInteractionEnabled = NO;
    
    __strong id <ATStopWatchViewControllerDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(stopWatchViewControllerDidDisappear:)]) {
        [delegate stopWatchViewControllerDidDisappear:self];
    }
    
    [self.view bringSubviewToFront:self.digitsInputView];
}
#pragma mark - Visuals

- (void)setSeperatorColor:(UIColor *)seperatorColor {
    
    _seperatorColor = seperatorColor;
    
    [self.dateSelectionView setBottomBorderColor:self.seperatorColor];
    [self.timeEntryView setBottomBorderColor:self.seperatorColor];
    [self.categorySelectionViewController setBottomBorderColor:self.seperatorColor];
    
}


#pragma mark - TimeEntry Configuration

- (void)configureWithTimeEntry:(id <ATTimeEntryProtocol>)timeEntry {

    
    [self removeAllObservationsOfObject:self.timeEntry];
    
    self.timeEntry = timeEntry;
    
    [self setNavigationMode:NavigationModeDefault];
    [self configureDefaultNavigationMode];

    if ([self.timeEntry trackingSince]) {
        // We have a tracking time entry
        [self configureWithTrackingTimeEntry:self.timeEntry];
    } else
    if ([self.timeEntry isInserted]) {
        [self configureWithNewTimeEntry:self.timeEntry];
    } else {
        [self configureWithStoppedTimeEntry:self.timeEntry];
    }
    
    [self reload];
}

- (void)configureWithStoppedTimeEntry:(id<ATTimeEntryProtocol>)timeEntry {
    [self setNavigationMode:NavigationModeDefault];
    
    self.stopWatchMode = StopWatchModeTimerStopped;
    self.timerButtonState = TimerButtonStateStart;
    [self.digitsInputView stopColonAnimation];
    [self updateTimerButton];
}

- (void)configureWithTrackingTimeEntry:(id <ATTimeEntryProtocol>)trackingTimeEntry {
    
    [self setNavigationMode:NavigationModeDefault];
    
    self.stopWatchMode = StopWatchModeTimerRunning;
    self.timerButtonState = TimerButtonStateStop;
    
    [self.digitsInputView startColonAnimation];
    
    [self updateTimerButton];
    
    [self observeObject:self.timeEntry property:@"trackingMinutes" withBlock:^(__weak ATStopWatchViewController *self, __weak id <ATTimeEntryProtocol> timeEntry, id old, id new) {
        [self.digitsInputView setFormattedDuration:[[self.timeEntry trackingDurationInMinutes] durationValue]];
    }];
    
}

- (void)configureWithNewTimeEntry:(id <ATTimeEntryProtocol>)trackingTimeEntry {
    
    [self setNavigationMode:NavigationModeNew];
    
    [self updateTimerButton];
}


#pragma mark - State

- (void)reload {
    
    [self.digitsInputView setFormattedDuration:[self formattedDuration]];
    [self.dateSelectionView configureWithDateComponents:[self dateComponents]];
    [self.categorySelectionViewController reloadData];
    
}

- (BOOL)isNewTimeEntry {
    if ([self.timeEntry isInserted]) {
        return YES;
    } else return NO;
}

- (BOOL)hasAssignedTimeEntry {
    if (self.timeEntry) return YES;
    else return NO;
}

- (NSTimeInterval)durationInTimeInterval {
    return [[self durationInMinutes] integerValue] * 60;
}

- (NSNumber *)durationInMinutes {
    return [self.timeEntry durationInMinutes];
}

- (NSString *)formattedDuration {
    if ([self.timeEntry trackingSince]) {
        return [[self.timeEntry trackingDurationInMinutes] durationValue];
    } else
    return [[self.timeEntry durationInMinutes] durationValue];
}

- (NSDateComponents *)dateComponents {
    NSDate *date = [self.timeEntry date];
    if (!date) date = [NSDate date];
    return [[ATDateConfiguration currentConfiguration] dateComponentsFromDate:date];
}

- (void)updateTimeEntryWithMinutes:(NSNumber *)minutes {
    [self.timeEntry setDurationInMinutes:minutes];
}

- (void)removeInsertedTimeEntry {
    [self.timeEntry delete];
    self.timeEntry = nil;
}

- (BOOL)isTracking {
    
    if ([self.timeEntry trackingSince]) {
        return YES;
    }
    return NO;

}

#pragma mark - Timer

- (void)startTimer {
    
    __strong id <ATStopWatchViewControllerNavigationDelegate> delegate = self.navigationDelegate;
    
    if ([delegate respondsToSelector:@selector(stopWatchViewController:willStartTimeEntry:success:failure:)]) {
        [delegate stopWatchViewController:self willStartTimeEntry:self.timeEntry success:^{
            
            self.stopWatchMode = StopWatchModeTimerRunning;
            self.timerButtonState = TimerButtonStateStop;
            [self.digitsInputView startColonAnimation];
            [self updateTimerButton];

            
            [self.timeEntry startTimer];
            
            [self configureWithTrackingTimeEntry:self.timeEntry];
            
        } failure:^(NSError *error) {
            
            
        }];
    }
    
}

- (void)stopTimer {
    
    __strong id <ATStopWatchViewControllerNavigationDelegate> delegate = self.navigationDelegate;
    
    if ([delegate respondsToSelector:@selector(stopWatchViewController:willStopTimeEntry:success:failure:)]) {
        [delegate stopWatchViewController:self willStopTimeEntry:self.timeEntry success:^{

            self.stopWatchMode = StopWatchModeTimerStopped;
            [self.digitsInputView stopColonAnimation];
            self.timerButtonState = TimerButtonStateStart;
            [self.digitsInputView setFormattedDuration:[[self.timeEntry durationInMinutes] durationValue]];
            [self updateTimerButton];
        } failure:^(NSError *error) {
            
        }];
    
    }

    
}

#pragma mark - Creation

- (void)configureAddTimeEntry {
    
    self.addTimeEntryButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 44, 44)];
    [self.addTimeEntryButton setImage:[[UIImage imageNamed:@"icon-plus"] imageTintedWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.view addSubview:self.addTimeEntryButton];
    
    [self.addTimeEntryButton addTarget:self action:@selector(show) forControlEvents:UIControlEventTouchUpInside];
    
    [self.addTimeEntryButton makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.centerX);
        make.top.equalTo(self.view.top).offset(0);
        make.width.equalTo(self.view.width);
        make.height.equalTo(@(44));
    }];
    
    if ([self hasAssignedTimeEntry]) {
        self.addTimeEntryButton.alpha = 0;
        self.digitsInputView.alpha = 1;
    } else {
        self.addTimeEntryButton.alpha = 1;
        self.digitsInputView.alpha = 0;
    }
 
    self.addTimeEntryButton.showsTouchWhenHighlighted = YES;

}

#pragma mark - Navigation

- (void)configureTopToolbar {
    
    self.topToolBar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 64)];
	
    [self.view addSubview:self.topToolBar];
	self.topToolBar.backgroundColor = [UIColor colorWithWhite:0 alpha:0];
	[self.topToolBar setBackgroundImage:[UIImage imageNamed:@"navbar-bg"] forBarMetrics:UIBarMetricsDefault];
	self.topToolBar.barStyle = UIBarStyleBlackTranslucent;
    
	self.topToolBar.tintColor = [UIColor colorWithWhite:1 alpha:1];
    
	UIBarButtonItem *closeButton = self.closeButtonItem;
   
    UIBarButtonItem *editButton = self.editButtonItem;

    
    UINavigationBar *navigationBar = self.topToolBar;
    UINavigationItem *item = [[UINavigationItem alloc] init];
    item.leftBarButtonItem = closeButton;
    item.rightBarButtonItem = editButton;
    
    [navigationBar pushNavigationItem:item animated:NO];
    
	//[self.topToolBar setItems:@[closeButton, editButton] animated:YES];
	[self.topToolBar setHidden:YES];
    
    for (UIView *view in self.topToolBar.subviews) {
        for (UIView *view2 in view.subviews) {
            if ([view2 isKindOfClass:[UIImageView class]]) {
                [view2 removeFromSuperview];
            }
        }
    }
}

// Show hide

- (void)hideNavigatinItems {
    
    [self.topToolBar setHidden:YES];
//    
//    UINavigationItem *item = [[UINavigationItem alloc] init];
//    item.hidesBackButton = YES;
//    
//	[self.topToolBar pushNavigationItem:item animated:YES];
//    
//	
//	self.topToolBar.tintColor = [UIColor whiteColor];
//
}

- (void)showNavigationItems {

	[self.topToolBar setHidden:NO];
    
    //[self.topToolBar adjustHeightTo:66];

//    UINavigationItem *item = [[UINavigationItem alloc] init];
//    
//    item.leftBarButtonItem = [self closeButtonItem];
//    item.rightBarButtonItem = [self editButtonItem];
//    
//    [self.topToolBar pushNavigationItem:item animated:YES];
    
    
	//[self.topToolBar setHidden:NO];

}

// Actions
#pragma mark - Actions

- (void)menu:(UIBarButtonItem *)menu {
    __strong id <ATStopWatchViewControllerNavigationDelegate> delegate = self.navigationDelegate;
    if ([delegate respondsToSelector:@selector(stopWatchViewControllerWillShowMenu:)]) {
        [delegate stopWatchViewControllerWillShowMenu:self];
    }
}

- (void)add:(UIBarButtonItem *)add {

    __strong id <ATStopWatchViewControllerNavigationDelegate> delegate = self.navigationDelegate;
    if ([delegate respondsToSelector:@selector(stopWatchViewControllerWillAddNewTimeEntry:)]) {
        [delegate stopWatchViewControllerWillAddNewTimeEntry:self];
    }
    
}

- (void)close:(UIBarButtonItem *)close {
    [self closeAnimated:YES];
}

- (void)cancelNew:(UIBarButtonItem *)cancel {
    [self removeInsertedTimeEntry];
    [self closeAnimated:YES];
}

- (void)cancelEdit:(UIBarButtonItem *)cancel {

}

- (void)create:(UIBarButtonItem *)create {
    
    [self createTimeEntry:^{
        
        [self closeAnimated:YES];
        
    } failure:^(NSError *error) {
        
    }];
    
}

- (void)closeAnimated:(BOOL)animated {
    [self animateWithSpeed:1];
}

- (void)animateWithSpeed:(float)speed {

    __strong id <ATStopWatchViewControllerNavigationDelegate> delegate = self.navigationDelegate;
    
    if (speed > 0) {
        if ([delegate respondsToSelector:@selector(stopWatchViewControllerWillClose:withSpeed:)]) {
            [delegate stopWatchViewControllerWillClose:self withSpeed:speed];
        }
    } else {
        [self presentInDirection:speed completion:nil];
    }

}

- (void)hideWithSpeed:(float)speed completion:(void (^)())completionBlock {
    [self presentInDirection:fabs(speed) completion:completionBlock];
}

- (void)hideAnimatedWithCompletion:(void (^)())completionBlock {
    [self presentInDirection:1 completion:completionBlock];
}

- (BOOL)hideCompletetly {
    
    __strong id <ATStopWatchViewControllerNavigationDelegate> delegate = self.navigationDelegate;
    if ([delegate respondsToSelector:@selector(stopWatchViewControllerShouldHideCompletely:)]) {
        return [delegate stopWatchViewControllerShouldHideCompletely:self];
    }
    
    return _hideCompletetly;
}


- (void)doneButton:(UIBarButtonItem *)item {
    
    if (self.timeRangeEditingMode == TimeRangeModeEditingCategory) {
        [self doneEditingCategory];
    }
    
    if (self.timeRangeEditingMode == TimeRangeModeEditingFrom || self.timeRangeEditingMode == TimeRangeModeEditingTo) {
        [self stopTimeRangeEditingMode];
    }
    
    if (self.timeRangeEditingMode == TimeRangeModeEditingDate) {
        [self doneEditingDate];
    }
    
    if (self.timeRangeEditingMode == TimeRangeModeEditingDuration) {
        [self doneEditingDuration];
    }
    
    [self setNavigationMode:NavigationModeDefault];
}
#pragma mark - Create save


- (void)createTimeEntry:(void (^)())success failure:(void (^)(NSError *error))failure {
    
    [[ATDatasources currentDatasource] postTimeEntry:self.timeEntry success:^{
        
        if (success) success();
        
    } failure:^(NSError *error) {
        
        if (failure) failure(error);
        
    }];
}

#pragma mark - Navigation button items


- (UIBarButtonItem *)editButtonItem {
    return [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit", Nil) style:UIBarButtonItemStylePlain target:self action:@selector(edit:)];
}

- (UIBarButtonItem *)cancelDateEditing {
        return [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", Nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelDateEditing:)];
}

- (UIBarButtonItem *)closeButtonItem {
    return [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Close", Nil) style:UIBarButtonItemStylePlain target:self action:@selector(close:)];
}

- (UIBarButtonItem *)cancelEditButtonItem {
    return [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", Nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelEdit:)];
}

- (UIBarButtonItem *)cancelNewButtonItem {
    return [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", Nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelNew:)];
}


- (UIBarButtonItem *)doneButtonItem {
    return [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", Nil) style:UIBarButtonItemStyleDone target:self action:@selector(doneButton:)];
}

- (UIBarButtonItem *)saveButtonItem {
    return [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", Nil) style:UIBarButtonItemStyleDone target:self action:@selector(doneButton:)];
}

- (UIBarButtonItem *)createButtonItem {
    return [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Create", Nil) style:UIBarButtonItemStylePlain target:self action:@selector(create:)];
}


- (void)configureDefaultNavigationMode {
    

    UINavigationItem *item = [[UINavigationItem alloc] init];
    
    item.leftBarButtonItem = [self closeButtonItem];
    item.rightBarButtonItem = [self editButtonItem];
    
    [self.topToolBar pushNavigationItem:item animated:YES];


}

- (void)configureEditNavigationMode {
    
    UINavigationItem *item = [[UINavigationItem alloc] init];
    
    item.leftBarButtonItem = [self cancelEditButtonItem];
    item.rightBarButtonItem = [self saveButtonItem];
    
    [self.topToolBar pushNavigationItem:item animated:YES];
    
}

- (void)configureNewNavigationMode {
    

    UINavigationItem *item = [[UINavigationItem alloc] init];
    
    item.leftBarButtonItem = [self cancelNewButtonItem];
    item.rightBarButtonItem = [self createButtonItem];
    
    [self.topToolBar pushNavigationItem:item animated:YES];

}

- (void)configureDateSelectionNavigationMode {
    
    
    UINavigationItem *item = [[UINavigationItem alloc] init];
    
    item.leftBarButtonItem = [self cancelDateEditing];
    item.rightBarButtonItem = [self saveButtonItem];
    [self.topToolBar pushNavigationItem:item animated:YES];
    
}



- (void)setGlobalNavigationHidden:(BOOL)hidden {

    
    
}

- (void)setTimeRangeEditingMode:(TimeRangeEditingMode)timeRangeEditingMode {
    _timeRangeEditingMode = timeRangeEditingMode;
    
    NSLog(@"Setting time range editing mode to: %i", timeRangeEditingMode);
}

- (void)setNavigationMode:(NavigationMode)navigationMode {
    
    if (navigationMode == self.navigationMode) {
        return;
    }
    
    [self.topToolBar popNavigationItemAnimated:NO];
    [self.topToolBar bringToFront];
    
    [self.panGesture setEnabled:NO];

    switch (navigationMode) {
        case NavigationModeDefault:
            [self.panGesture setEnabled:YES];
            if (![self isNewTimeEntry]) {
                [self configureDefaultNavigationMode];
            } else {
                [self configureNewNavigationMode];
            }
            break;
        case NavigationModeEdit:
            [self configureEditNavigationMode];
            break;
        case NavigationModeNew:
            [self configureNewNavigationMode];
            break;
        case NavigationModeGlobal:
            break;
        case NavigationModeDateSelection:
            [self configureDateSelectionNavigationMode];
            break;
        default:
            break;
    }
    

    _navigationMode = navigationMode;
}

#pragma mark - Options

- (void)configurePageControl {
    
    
	self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
	[self.view addSubview:self.pageControl];
	self.pageControl.numberOfPages = 3;
    
	[self.pageControl makeConstraints: ^(MASConstraintMaker *make) {
	    make.width.equalTo(self.view);
	    make.centerX.equalTo(self.view.centerX);
	    make.top.equalTo(self.categorySelectionView.bottom).with.offset(0);
	}];
    

}

#pragma mark - Timer Button

- (void)configureStartButton {
    
    self.startButtonBottomConstraint = -65;
    
    self.startButtonTitleColor =[UIColor colorWithRed:0.041 green:0.210 blue:0.421 alpha:1.000];
    
    self.timerActionButton = [[UIButton alloc] initWithFrame:CGRectZero];
    
	[self.timerActionButton setBackgroundColor:[UIColor colorWithWhite:1 alpha:1]];
    
	[self.timerActionButton setTitle:@"Start" forState:UIControlStateNormal];
	[self.timerActionButton setTitleColor:self.startButtonTitleColor forState:UIControlStateNormal];
    self.timerActionButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:24];
    
	self.timerActionButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.timerActionButton.layer.cornerRadius = 5;
    
	[self.view addSubview:self.timerActionButton];
    
	[self.timerActionButton makeConstraints: ^(MASConstraintMaker *make) {
	    make.width.equalTo(self.view.width).with.offset(-20);
	    make.height.equalTo(@(44));
	    make.centerX.equalTo(self.view.centerX);
	}];
    

	NSLayoutConstraint *timeButtonBottomConstraint = [NSLayoutConstraint constraintWithItem:self.timerActionButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:self.startButtonBottomConstraint];
    
	[self.view addConstraint:timeButtonBottomConstraint];
    
	self.timerButtonBottomConstraint = timeButtonBottomConstraint;
    
	[self.timerActionButton addTarget:self action:@selector(didTapOnTimerButton:) forControlEvents:UIControlEventTouchUpInside];

}

- (void)configureDoneButton {
    
    self.startButtonTitleColor =[UIColor colorWithRed:0.041 green:0.210 blue:0.421 alpha:1.000];
    
    self.doneButton = [[UIButton alloc] initWithFrame:CGRectZero];
    
	[self.doneButton setBackgroundColor:[UIColor colorWithWhite:1 alpha:1]];
    
	[self.doneButton setTitle:NSLocalizedString(@"Done", nil) forState:UIControlStateNormal];
	[self.doneButton setTitleColor:self.startButtonTitleColor forState:UIControlStateNormal];
    self.doneButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:24];
    
	self.doneButton.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.doneButton.layer.cornerRadius = 5;
    
	[self.view addSubview:self.doneButton];
    
    [self.doneButton setHidden:YES];
    
	[self.doneButton makeConstraints: ^(MASConstraintMaker *make) {
	    make.width.equalTo(self.view.width).with.offset(-20);
	    make.height.equalTo(@(44));
	    make.centerX.equalTo(self.view.centerX);
	}];
    
    
	NSLayoutConstraint *timeButtonBottomConstraint = [NSLayoutConstraint constraintWithItem:self.doneButton attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeBottom multiplier:1 constant:-65];
    
	[self.view addConstraint:timeButtonBottomConstraint];
    
    self.doneButtonBottomConstraint = timeButtonBottomConstraint;
    
	[self.doneButton addTarget:self action:@selector(didTapOnDoneButton:) forControlEvents:UIControlEventTouchUpInside];
}


- (void)didTapOnTimerButton:(UIButton *)button {
    
	if (self.stopWatchMode == StopWatchModeTimerReady || self.stopWatchMode == StopWatchModeTimerStopped) {
			[self startTimer];
	}
	else if (self.stopWatchMode == StopWatchModeTimerRunning)    {
			[self stopTimer];
	}

}

- (void)didTapOnDoneButton:(UIButton *)button {
    if (self.timeRangeEditingMode == TimeRangeModeEditingDuration) {
        [self stopEditingDuration];
    }
    else if (self.timeRangeEditingMode == TimeRangeModeEditingDate) {
        [self doneEditingDate];
    }
    else {
        [self stopTimeRangeEditingMode];
    }

    [self setTimerButtonHidden:NO];
}

- (void)setTimerButtonHidden:(BOOL)hidden {
    //[self.timerActionButton setHidden:hidden];
}

- (void)setDoneButtonHidden:(BOOL)hidden {
  //  [self.timerActionButton setHidden:!hidden];
}

- (void)updateTimerButton {
	if (self.stopWatchMode == StopWatchModeTimerReady || self.stopWatchMode == StopWatchModeTimerStopped) {
		
        [self.timerActionButton setTitleColor:[UIColor colorWithRed:0.041 green:0.210 blue:0.421 alpha:1.000] forState:UIControlStateNormal];
        
        [self.timerActionButton setBackgroundColor:[UIColor whiteColor]];
        [self.timerActionButton setTitle:NSLocalizedString(@"Start", nil) forState:UIControlStateNormal];
        
	}
	else if (self.stopWatchMode == StopWatchModeTimerRunning)    {
        
        
        [self.timerActionButton setBackgroundColor:[UIColor colorWithRed:0.715 green:0.334 blue:0.358 alpha:1.000]];
        
        [self.timerActionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.timerActionButton setTitle:NSLocalizedString(@"Stop", nil) forState:UIControlStateNormal];
		
	}
}


#pragma mark - Date Selection

- (void)configureDateSelectionView {
    
	self.dateSelectionView = [[ATStopWatchDateSelection alloc] initWithFrame:CGRectMake(0, 280, self.view.bounds.size.width,self.buttonHeight)];

	self.dateSelectionView.delegate = self;
	
    [self.dateSelectionView configureWithDateComponents:self.dateComponents];
    
	[self.contentScroller addSubview:self.dateSelectionView];
    
	[self.view addConstraints:[self.dateSelectionView constrainWidth:[@(self.view.bounds.size.width)description]]];
    
	self.dateSelectionView.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.dateSelectionView.dateView.contentInsets = UIEdgeInsetsMake(13, 45, 15, 10);

	NSLayoutConstraint *dateSelectionView = [NSLayoutConstraint constraintWithItem:self.dateSelectionView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.timeEntryView attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    
	[self.view addConstraint:dateSelectionView];
    
	self.dateSelectionViewTopConstraint = dateSelectionView;
    
	self.dateSelectionViewBottomConstraint = [NSLayoutConstraint constraintWithItem:self.dateSelectionView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:self.buttonHeight];
	[self.view addConstraint:self.dateSelectionViewBottomConstraint];
    
    
    __weak __typeof(& *self) weakSelf = self;
    
    [self.dateSelectionView registerTouchesBeganBlock:^{
        __strong __typeof(&*weakSelf) strongSelf = weakSelf;
        [UIView animateWithDuration:0.5 animations:^{
            strongSelf.dateSelectionView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.3];
        }];
    } touchesEndedBlock:^{
        __strong __typeof(&*weakSelf) strongSelf = weakSelf;
        [UIView animateWithDuration:0.5 animations:^{
            strongSelf.dateSelectionView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.0];
        }];
        [strongSelf handleDateSelectionTap:nil];
    } touchesMoved:^{
        
    } touchesCancled:^{
        __strong __typeof(&*weakSelf) strongSelf = weakSelf;
        [UIView animateWithDuration:0.5 animations:^{
            strongSelf.dateSelectionView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.0];
        }];

    }];


}

- (void)handleDateSelectionTap:(UITapGestureRecognizer *)tapGesture {

	self.timeRangeEditingMode = TimeRangeModeEditingDate;
    
    [self setDoneButtonHidden:NO];
    
	self.panGesture.enabled = NO;
	self.dateSelectionTapGesture.enabled = NO;
    
	self.topContentLayoutConstraint.constant = -self.buttonHeight + 22;
	self.dateSelectionViewBottomConstraint.constant = self.view.bounds.size.height - self.buttonHeight / 2.0f;
    
    self.timerButtonBottomConstraint.constant = 0;
    
	[self.dateSelectionView configureWithDateComponents:self.dateComponents];
    
//	[UIView animateWithDuration:0.2 animations: ^{
//        
//        [self.digitsInputView adjustY:-self.digitsInputView.bounds.size.height];
//
//	    self.categorySelectionView.alpha = 0;
//	    self.timeEntryView.alpha = 0;
//	    self.digitsInputView.alpha = 0;
//	}];

    [self setNavigationMode:NavigationModeDateSelection];

    [self.dateSelectionView setMonthCalendarHidden:NO];

	[UIView animateWithDuration:0.75 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations: ^{
        
        [self.digitsInputView adjustY:-self.digitsInputView.bounds.size.height];
        
	    self.categorySelectionView.alpha = 0;
	    self.timeEntryView.alpha = 0;
	    self.digitsInputView.alpha = 0;
        
        [self.dateSelectionView setMonthLabelAlpha:1];

	    [self.view layoutIfNeeded];
        
	} completion: ^(BOOL finished) {
	}];
}

- (void)cancelDateEditing:(UIBarButtonItem *)item {
    [self doneEditingDate];
}

- (void)stopWatchDateSelection:(ATStopWatchDateSelection *)selectionView didSelectDateWithComponents:(NSDateComponents *)components {
	[self doneEditingDate];
    [self.timeEntry setDate_at:[[ATDateConfiguration currentConfiguration] dateFromDateComponents:components]];
}

- (void)doneEditingDate {
    self.timeRangeEditingMode = TimeRangeModeNotEditing;
	[self updateTimerButton];
    
	self.panGesture.enabled = YES;
	self.dateSelectionTapGesture.enabled = YES;
    
	self.digitsInputViewTopConstraint.constant = 30;
	self.topContentLayoutConstraint.constant = self.timeEntryViewInitialYPosition;
	self.dateSelectionViewBottomConstraint.constant = self.buttonHeight;
    
    self.timerButtonBottomConstraint.constant = self.startButtonBottomConstraint;

	[UIView animateWithDuration:0.5 delay:0.1 options:0 animations: ^{
	    self.categorySelectionView.alpha = 1;
	} completion: ^(BOOL finished) {
        
    }];
    
    [self setTimerButtonHidden:NO];
    
	[UIView animateWithDuration:0.75 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations: ^{
	    self.timeEntryView.alpha = 1;
	    self.digitsInputView.alpha = 1;
        
        [self.digitsInputView adjustY:self.digitsTopPosition / 2];
        
	    [self.dateSelectionView setMonthLabelAlpha:0];
        
	    [self.view layoutIfNeeded];
        
	} completion: ^(BOOL finished) {
        
	}];
    [self setNavigationMode:NavigationModeDefault];
}


#pragma mark - Category

- (void)configurecategorySelectionView {
    
    
    self.categorySelectionViewController = [[ATCategorySelectionViewController alloc] initWithNibName:Nil bundle:nil];
    self.categorySelectionViewController.delegate = self;
    self.categorySelectionViewController.categoryDatasource = self.remoteDatasource;
    self.categorySelectionViewController.datasource = self;
    
    self.categorySelectionView = self.categorySelectionViewController.view;
    
    self.categorySelectionView.translatesAutoresizingMaskIntoConstraints = NO;
   
    [self.contentScroller addSubview:self.categorySelectionView];
	[self.categorySelectionView setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.1]];

	NSLayoutConstraint *timeButtonBottomConstraint= [NSLayoutConstraint constraintWithItem:self.categorySelectionView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.dateSelectionView attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    
	[self.categorySelectionView makeConstraints: ^(MASConstraintMaker *make) {
	    make.width.equalTo(self.view.width);
	}];
    
	[self.view addConstraint:timeButtonBottomConstraint];
    
//	self.categorySelectionView.titleLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:30];
//	[self.categorySelectionView setImage:[[UIImage imageNamed:@"workout"] imageTintedWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
//	[self.categorySelectionView setImageEdgeInsets:UIEdgeInsetsMake(0, -20, 0, 0)];
//
	self.categorySelectionViewTopConstraint = timeButtonBottomConstraint;
    
    self.categorySelectionViewHeightConstraint = [NSLayoutConstraint constraintWithItem:self.categorySelectionView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.categorySelectionView attribute:NSLayoutAttributeTop multiplier:1 constant:120];
    
    [self.view addConstraint:self.categorySelectionViewHeightConstraint];

//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectcategorySelectionView:)];
//    [self.categorySelectionView addGestureRecognizer:tap];
    
    self.categorySelectionView.clipsToBounds = YES;
    
    //categoryLabel.alpha = 0;
    

//    
//    tabBar.alpha = 1;
//

    [self.contentScroller bringSubviewToFront:self.dateSelectionView];
}

- (void)showCategorySelectionViewController {
    
    
    self.timeRangeEditingMode = TimeRangeModeEditingCategory;
	[self updateTimerButton];
    
	self.panGesture.enabled = NO;
	self.dateSelectionTapGesture.enabled = NO;
    
    UIScreen *mainScreen = [UIScreen mainScreen];
    
	self.topContentLayoutConstraint.constant = -(self.timeEntryView.bounds.size.height + self.dateSelectionView.bounds.size.height - 64);
    self.categorySelectionViewHeightConstraint.constant = mainScreen.bounds.size.height - 64;
    
    self.timerButtonBottomConstraint.constant = 0;
    self.doneButtonBottomConstraint.constant = 0;
    
    
	[UIView animateWithDuration:0.75 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations: ^{
        
        [self.digitsInputView adjustY:-self.digitsInputView.bounds.size.height];
        
//        self.categorySelectionViewController.detailCategoryLabel.contentInsets = UIEdgeInsetsMake(5, 30, 5, 5);

        self.dateSelectionView.alpha = 0;
	    self.timeEntryView.alpha = 0;
	    self.digitsInputView.alpha = 0;

	    [self.view layoutIfNeeded];
	
        [self.categorySelectionViewController setSelectionElementsHidden:NO];
        
    } completion: ^(BOOL finished) {
        
	}];
    



    
}

- (void)doneEditingCategory {
    
    [[self.categorySelectionView.gestureRecognizers firstObject] setEnabled:YES];
    
    self.timeRangeEditingMode = TimeRangeModeEditingDate;
	
    [self updateTimerButton];
    
	self.panGesture.enabled = YES;
	self.dateSelectionTapGesture.enabled = YES;
    
	self.topContentLayoutConstraint.constant = self.timeEntryViewInitialYPosition;
    
    self.categorySelectionViewHeightConstraint.constant = 120;
    
    self.timerButtonBottomConstraint.constant = -65;
    self.doneButtonBottomConstraint.constant = -65;
//	[UIView animateWithDuration:0.4 animations: ^{
//        
//        [self.digitsInputView adjustY:self.digitsDefaultPosition];
//        self.dateSelectionView.alpha =1;
//	    self.timeEntryView.alpha = 1;
//	    self.digitsInputView.alpha = 1;
//        
//	}];
    
    
	[UIView animateWithDuration:0.75 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations: ^{
        
        [self.digitsInputView adjustY:self.digitsDefaultPosition];
        self.dateSelectionView.alpha =1;
	    self.timeEntryView.alpha = 1;
	    self.digitsInputView.alpha = 1;
        
        [self.categorySelectionViewController setSelectionElementsHidden:YES];

	    [self.view layoutIfNeeded];
        
	} completion: ^(BOOL finished) {
        
	}];
    
    [self setNavigationMode:NavigationModeDefault];

}

#pragma mark - CategorySelectionViewControllerDatasource

- (ATCategoryConfiguration *)mainCategoryConfigurationForCategorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController
{
    return [self.remoteDatasource configurationForMainCategory];
}

- (ATCategoryConfiguration *)secondaryCategoryConfigurationForCategorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController {
    return [self.remoteDatasource configurationForSecondaryCategory];
}

- (BOOL)categorySelectionViewControllerHasSecondaryCategory:(ATCategorySelectionViewController *)categorySelectionViewController {
    return [self.timeEntry hasSecondaryCategory];
}

- (id <ATCategoryProtocol>)mainCategoryForCategorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController {
    return [self.timeEntry mainCategory];
}

- (id <ATCategoryProtocol>)secondaryCategoryForCategorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController {
    return [self.timeEntry secondaryCategory];
}

- (NSString *)mainCategoryTitleForCategorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController {
    return [self.timeEntry mainCategoryTitle];
}

- (NSString *)secondaryCategoryTitleForCategorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController {
    return [self.timeEntry secondaryCategoryTitle];
}

#pragma mark - CategorySelectionViewController Delegate

- (void)categorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController didSelectCategory:(id<ATCategoryProtocol>)category {

    [self doneEditingCategory];

}

- (void)categorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController didSelectMainCategory:(id <ATCategoryProtocol>)category {

    [self.timeEntry setMainCategory:category];
    [categorySelectionViewController reloadData];
    
    [self doneEditingCategory];

}

- (void)categorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController didSelectSecondaryCategory:(id <ATCategoryProtocol>)category {
    [self.timeEntry setSecondaryCategory:category];
    [categorySelectionViewController reloadData];
    [self doneEditingCategory];
}

- (void)categorySelectionViewControllerWillShowMainCategorySelection:(ATCategorySelectionViewController *)categorySelectionViewController {
    
    [self setNavigationMode:NavigationModeEdit];

    [[[self.topToolBar items] lastObject] setTitle:[[self.remoteDatasource configurationForMainCategory] name]];

    [self showCategorySelectionViewController];
}

- (void)categorySelectionViewControllerWillShowSecondaryCategorySelection:(ATCategorySelectionViewController *)categorySelectionViewController {

    [self setNavigationMode:NavigationModeEdit];

    [[[self.topToolBar items] lastObject] setTitle:[[self.remoteDatasource configurationForSecondaryCategory] name]];

    [self showCategorySelectionViewController];
}

#pragma mark - Gestures

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    
	if ([gestureRecognizer isKindOfClass:[ATButtonGestureRecognizer class]] || [otherGestureRecognizer isKindOfClass:[ATButtonGestureRecognizer class]])
        return YES;
    return NO;
}

//- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
//    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
//        UIPanGestureRecognizer *panGesture = (UIPanGestureRecognizer *)gestureRecognizer;
//        CGPoint p = [panGesture translationInView:panGesture.view];
//        if (p.x > 0) return NO;
//    }
//    return YES;
//}

- (void)showAnimatedWithCompletion:(void (^)())completionBlock {
    [self.tapGesture setEnabled:NO];
    
	self.presenting = YES;
    
    
    if (![self hasAssignedTimeEntry]) {
        
        id <ATTimeEntryProtocol> timeEntry = [[ATDatasources currentDatasource] insertNewTimeEntry];
        
        [self configureWithTimeEntry:timeEntry];
        
    }
    
    [self presentInDirection:-1.25 completion:completionBlock];

}

- (void)show {
    [self showAnimatedWithCompletion:nil];
}

- (void)handleTap:(UIGestureRecognizer *)tap {
//    [self show];
}

- (void)handlePan:(UIPanGestureRecognizer *)panGesture {
    
	static CGPoint startPoint;
	static float startYOffset;
    
	CGPoint location = [panGesture locationInView:[[panGesture view] superview]];
    
    
	float startPointY =  startPoint.y;
    
	float diff = location.y - startPointY;
    
	CGPoint velocity = [panGesture velocityInView:[panGesture view]];
    
	if (panGesture.state == UIGestureRecognizerStateBegan) {
		startYOffset = self.view.frame.origin.y;
		startPoint = location;
        
	}
    
	if (panGesture.state == UIGestureRecognizerStateChanged) {
		self.positionContraint.constant = startYOffset + diff;
        
		[self.view adjustY: startYOffset + diff];
        
		[self updateOffset:CGPointMake(0, fabs(startYOffset + diff - self.view.superview.bounds.size.height + self.initialBottomOffset))];
	}
    
	if (panGesture.state == UIGestureRecognizerStateEnded) {
        
        [self animateWithSpeed:1 / velocity.y * 800.0f];
        
    }
    
    
}


#pragma mark - Animations

- (void)updateOffset:(CGPoint)position {
    
	[self layoutDigisForOffset:position.y];
}

- (void)layoutDigisForOffset:(float)offset {
    
	self.digitsBackgroundView.backgroundColor = [UIColor colorWithWhite:0 alpha:MIN(offset / 500.0f, 0.05)];
	float scale = MIN(self.digitsInputView.minimumScale + 0.25 * offset / 50, 1);
    
    self.topBackgroundView.alpha = MIN(scale - 0.3, 0.4);
    
    self.bottomToolbar.alpha = (1 - (scale));
    
	float adjustY = offset + self.digitsInputView.minimumOffset.y;
    
    adjustY *= 0.8;
    
    float y = MIN(self.digitsTopPosition , adjustY);
    
    [self.digitsInputView setScale:scale andPosition:CGPointMake(self.view.center.x, y)];
    
}

- (void)presentInDirection:(float)direction completion:(void(^)())completion {
    
    self.start = self.positionContraint.constant;
    
    float speedFactor = fabs(direction);
    //
    if (speedFactor < 0.5) speedFactor = 0.5;
    if (speedFactor > 1.5) speedFactor = 1.5;
    
    
    float damping = 0.8 ;
    
	if (direction < 0)
		self.positionContraint.constant = self.view.superview.bounds.size.height - (self.view.superview.bounds.size.height - 0);
	else self.positionContraint.constant = self.view.superview.bounds.size.height - self.initialBottomOffset;
    
    
	__weak __typeof(& *self) weakSelf = self;
    
    
    __strong id <ATStopWatchViewControllerDelegate> delegate = self.delegate;
    
	if (direction > 0) {
        
        if ([delegate respondsToSelector:@selector(stopWatchViewControllerWillDisappear:)]) {
            [delegate stopWatchViewControllerWillDisappear:self];
        }
        
		self.tapGesture.enabled = YES;
        
		[self hideNavigatinItems];
        
        self.presenting = NO;
        
        self.digitsInputView.userInteractionEnabled = YES;
        
	}
	else {
        
        __strong id <ATStopWatchViewControllerDelegate> delegate = self.delegate;
        if ([delegate respondsToSelector:@selector(stopWatchViewControllerWillAppear:)]) {
            [delegate stopWatchViewControllerWillAppear:self];
        }
        
		[self showNavigationItems];
        
        self.presenting = YES;
        
		self.tapGesture.enabled = NO;
		self.digitsInputView.userInteractionEnabled = YES;
	}
    
    
	[UIView animateWithDuration:speedFactor delay:0 usingSpringWithDamping:damping initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionCurveEaseOut animations: ^{
	    
        __strong __typeof(&*weakSelf) strongSelf = weakSelf;
        
        
        // Hide
	    if (direction > 0) {
            
            if (!self.hideCompletetly)
                [strongSelf.view moveTopToBottomWithMargin:44];
            else {
                [strongSelf.view moveTopToBottomWithMargin:0];
            }
            
            [strongSelf.digitsInputView moveToInitialPosition];
            strongSelf.topBackgroundView.alpha = 0;
            
            if ([strongSelf hasAssignedTimeEntry]) {
                strongSelf.addTimeEntryButton.alpha = 0;
                strongSelf.digitsInputView.alpha = 1;
            } else {
                
                strongSelf.digitsInputView.alpha = 0;
                
                strongSelf.addTimeEntryButton.alpha = 0;
            }
            
            strongSelf.bottomToolbar.alpha = 1;
            
            
        }
        // Show
	    else {
            
            [strongSelf.view adjustY:0];
            
            strongSelf.addTimeEntryButton.alpha = 0;
            [strongSelf.digitsInputView setScale:1 andPosition:CGPointMake(strongSelf.view.center.x, strongSelf.digitsTopPosition)];
            strongSelf.topBackgroundView.alpha = 0.4;
            
            strongSelf.digitsInputView.alpha = 1;
            
            strongSelf.bottomToolbar.alpha = 0;
            
		}
        
	} completion: ^(BOOL finished) {
	    __strong __typeof(&*weakSelf) strongSelf = weakSelf;
        
	    if (direction > 0) {
	        [strongSelf viewDidDisappear:YES];
		}
	    else {
	        [strongSelf viewDidAppear:YES];
		}

        if (completion) completion();
    }];

}


- (void)setPresenting:(BOOL)presenting {
    _presenting = presenting;
}

#pragma mark - DigitsInputView

- (BOOL)digitsInputviewShouldStartEditing:(DigitsInputView *)digitsInputView {
    
    if (!self.presenting) {
        [self show];
        return NO;
    }
    
    return self.presenting;
}

- (NSInteger)digitsInputView:(DigitsInputView *)digitsInputView digitAtIndex:(NSInteger)index {
	return 0;
}

- (void)digitsInputView:(DigitsInputView *)digitsInputView didSelectDigit:(NSInteger)digit atIndex:(NSInteger)index {
    
}

- (void)digitsInputViewWillEndEditing:(DigitsInputView *)digitsInputView {
    
}

- (void)digitsInputViewWillStartEditing:(DigitsInputView *)digitsInputView {
    if (self.presenting)
    [self.digitsInputView adjustHeightTo:self.digitsEditingHeight];

}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {
    [super setEditing:editing animated:animated];
    
    [self setDoneButtonHidden:NO];
    [self setTimerButtonHidden:YES];
    
}

- (void)digitsInputViewDidStartEditing:(DigitsInputView *)digitsInputView {
    
  
	self.timeRangeEditingMode = TimeRangeModeEditingDuration;
    
    [self setDoneButtonHidden:NO];
    
    self.timerButtonBottomConstraint.constant = 0;
    
	[UIView animateWithDuration:0.25 animations: ^{
	    self.dateSelectionView.alpha = 0;
	    self.categorySelectionView.alpha = 0;
	    self.timeEntryView.alpha = 0;
	}];
    
	[UIView animateWithDuration:0.75 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations: ^{
	    self.topContentLayoutConstraint.constant = self.view.bounds.size.height;
  
	    [self.view layoutIfNeeded];
	} completion: ^(BOOL finished) {
	}];

    [self setNavigationMode:NavigationModeEdit];
    
}


- (void)digitsInputViewDidEndEditing:(DigitsInputView *)digitsInputView {
    

    [self updateTimeEntryWithMinutes:[digitsInputView durationInMinutes]];
    
	self.timerButtonState = self.previousTimerButtonState;
    
	[self updateTimerButton];
    
    
	self.topContentLayoutConstraint.constant = self.timeEntryViewInitialYPosition;
	self.timeEntryBottomConstraint.constant = self.timeEntryViewHeight;
	self.dateSelectionViewTopConstraint.constant = 0;
	self.digitsInputViewTopConstraint.constant = 30;

    self.timerButtonBottomConstraint.constant = self.startButtonBottomConstraint;
    
    __weak __typeof(&*self)weakSelf = self;
    [self performBlock:^{
        __strong __typeof(&*weakSelf)strongSelf = weakSelf;
        [strongSelf.digitsInputView adjustHeightTo:strongSelf.digitsNonEditingHeight];
    } afterDelay:0.3];
    
    
	[UIView animateWithDuration:0.75 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations: ^{
	    self.dateSelectionView.alpha = 1;
	    self.categorySelectionView.alpha = 1;
	    self.timeEntryView.alpha = 1;
        
	    [self.view layoutIfNeeded];
	} completion: ^(BOOL finished) {
     

	}];
}

- (void)setKeyboardHidden:(BOOL)hidden complete:(void (^)())complete {

}

- (void)doneEditingDuration {
    [self stopEditingDuration];
}

- (void)stopEditingDuration {
	self.timeRangeEditingMode = TimeRangeModeNotEditing;
	[self.digitsInputView setEditingMode:DigitsInputViewEditingModeNotEditing];
}


#pragma mark - Helpfer

- (UIButton *)buttonWithTitle:(NSString *)title {
	UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.buttonHeight)];
    
	[button setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.1]];
	//    [self.startButton setBackgroundColor:[UIColor colorWithRed:0.715 green:0.334 blue:0.358 alpha:1.000]];
    
	[button setTitle:title forState:UIControlStateNormal];
	[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
	self.timerActionButton.layer.borderColor = [[UIColor colorWithWhite:1 alpha:0.1] CGColor];
	self.timerActionButton.layer.borderWidth = 1;
    
	button.translatesAutoresizingMaskIntoConstraints = NO;
    
	[self.contentScroller addSubview:button];
    
    //    [self.view addConstraints:[button alignAttribute:NSLayoutAttributeLeft toAttribute:NSLayoutAttributeLeft ofView:self.view predicate:@"-1"]];
    //    [self.view addConstraints:[button alignAttribute:NSLayoutAttributeRight toAttribute:NSLayoutAttributeRight ofView:self.view predicate:@"1"]];
    //
//	[self.view addConstraints:[button constrainHeight:[@(self.buttonHeight) description]]];
    
	return button;
}

#pragma mark -
#pragma mark - TimeEntryView

- (void)configureTimeEntryView {

    
    self.timeEntryView = [[TimeEntryViewContainer alloc] initWithFrame:CGRectMake(0, 200, self.view.bounds.size.width, self.timeEntryViewHeight)];
    
	[self.contentScroller addSubview:self.timeEntryView];
    
	self.timeEntryView.translatesAutoresizingMaskIntoConstraints = NO;
	self.timeEntryView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.1];
	self.timeEntryView.delegate = self;
    
	[self.view addConstraints:[self.timeEntryView alignAttribute:NSLayoutAttributeWidth toAttribute:NSLayoutAttributeWidth ofView:self.view predicate:@""]];
	//[self.view addConstraints:[self.timeEntryView alignAttribute:NSLayoutAttributeCenterX toAttribute:NSLayoutAttributeCenterX ofView:self.view predicate:@""]];
    
	self.timeEntryViewInitialYPosition = 200;
    
	self.topContentLayoutConstraint = [NSLayoutConstraint constraintWithItem:self.timeEntryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:self.timeEntryViewInitialYPosition];
    
	[self.view addConstraint:self.topContentLayoutConstraint];
    
	self.timeEntryBottomConstraint = [NSLayoutConstraint constraintWithItem:self.timeEntryView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.timeEntryView attribute:NSLayoutAttributeTop multiplier:1 constant:self.timeEntryViewHeight];
    
	[self.view addConstraint:self.timeEntryBottomConstraint];

    [self.timeEntryView setBottomBorderColor:self.seperatorColor];
}

- (void)timeEntryViewContainerDidTapOnFrom:(TimeEntryViewContainer *)timeEntryView {
	self.title = @"From";
    
    
    self.timeRangeEditingMode = TimeRangeModeEditingFrom;
    [self startTimeEditing];
}

- (void)timeEntryViewContainerDidTapOnTo:(TimeEntryViewContainer *)timeEntryView {
  self.timeRangeEditingMode = TimeRangeModeEditingTo;
  [self startTimeEditing];
}

- (void)timeEntryViewContainerDidFinishEditing:(TimeEntryViewContainer *)timeEntryView {
	[self startTimeEditing];
}


- (void)startTimeEditing {
    
    [self setNavigationMode:NavigationModeEdit];

    self.digitsInputView.userInteractionEnabled = NO;
    
    [self setDoneButtonHidden:NO];
    
	self.topContentLayoutConstraint.constant = 66;
	
    UIScreen *mainScreen = [UIScreen mainScreen];
    
    self.timeEntryBottomConstraint.constant = mainScreen.bounds.size.height - self.topContentLayoutConstraint.constant;
    
	self.dateSelectionViewTopConstraint.constant = 0;
	self.digitsInputViewTopConstraint.constant = -15;
    self.timerButtonBottomConstraint.constant = 0;
    
	[UIView animateWithDuration:0.7 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations: ^{
	    
        [self.view layoutIfNeeded];
        
        [_digitsInputView adjustY:-20];
        
	    self.dateSelectionView.alpha = 0;
        
	    self.categorySelectionView.alpha = 0;
        
	    [_timeEntryView startEditingWithCompletionHandler: ^{
            
		}];
        
	    //        [self.digitsInputView setScale:0.2 andPosition:CGPointMake(0, 350)];
	} completion: ^(BOOL finished) {
	}];
    
    
	[_digitsInputView animateToScale:.3 duration:.25 complete:^{
        
    }];
    
}

- (void)stopTimeRangeEditingMode {

    [self setDoneButtonHidden:YES];
    
    self.digitsInputView.userInteractionEnabled = YES;

	self.timeRangeEditingMode = TimeRangeModeNotEditing;
	[self updateTimerButton];
    
	self.topContentLayoutConstraint.constant = self.timeEntryViewInitialYPosition;
	self.timeEntryBottomConstraint.constant = self.timeEntryViewHeight;
	self.dateSelectionViewTopConstraint.constant = 0;
	self.digitsInputViewTopConstraint.constant = self.buttonHeight;
    
    self.timerButtonBottomConstraint.constant = self.startButtonBottomConstraint;

	[self.timeEntryView stopEditingWithCompletionHandler: ^{
	}];
    

    
	[UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations: ^{
	    [self.view layoutIfNeeded];
        
	    self.dateSelectionView.alpha = 1;
	    self.categorySelectionView.alpha = 1;
        [_digitsInputView adjustY:self.digitsDefaultPosition];

	} completion: ^(BOOL finished) {
	}];
    
    
	[_digitsInputView animateToScale:1  duration:.25 complete:^{
        
    }];
}

#pragma mark - UIScrollView

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
	[UIView animateWithDuration:1 - (velocity.y / 500.0)  delay:0 usingSpringWithDamping:0.7 initialSpringVelocity:0 options:UIViewAnimationOptionCurveEaseOut | UIViewKeyframeAnimationOptionAllowUserInteraction animations: ^{
	    [scrollView setContentOffset:*targetContentOffset animated:NO];
	} completion: ^(BOOL finished) {
	}];
}

#pragma mark - Memory management

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

@end
