//
//  ATButtonGestureRecognizer.h
//  anytime
//
//  Created by Josef Materi on 20.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ATTouchDownInsideBlock)(void);
typedef void(^ATTouchUpInsideBlock)(void);
typedef void(^ATTouchCancledBlock)(void);

@interface ATButtonGestureRecognizer : UILongPressGestureRecognizer


+(instancetype) recognizerForView:(UIView *)view touchDownInside:(void (^)())touchDownInsideBlock touchUpInside:(void (^)())touchUpInsideBlock touchCanceld:(void (^)())touchCancledBlock;


@end
