//
//  BackgroundView.h
//  pocket
//
//  Created by Josef Materi on 04.01.11.
//  Copyright 2011 materi design. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	BackgroundViewPositionTop,
	BackgroundViewPositionBottom,
	BackgroundViewPositionMiddle,
	BackgroundViewPositionSingle
} BackgroundViewPosition;

@interface BackgroundView : UIView {
	
}
@property (nonatomic, retain) UIColor *gradientColor;
@property (nonatomic, assign) float cornerRadius;
@property (nonatomic, assign) BackgroundViewPosition position;
@property (nonatomic, retain) UIColor *tintColor;
@property (nonatomic, retain) UIColor *gridColor;
@property (nonatomic, assign) BOOL hideBackground;


- (void)drawGradiantWithColor: (UIColor *) fromColor toColor: (UIColor *) secondColor forRect:(CGRect)rect inContext:(CGContextRef)c;

@end
