//
//  TimeEntryView.m
//  Anytime
//
//  Created by Josef Materi on 21.10.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import "TimeEntryView.h"
#import "UIView+Animations.h"

@interface TimeEntryView ()

@property (nonatomic, strong) UILabel *fromFirstHourLabel;
@property (nonatomic, strong) UILabel *fromSecondHourLabel;
@property (nonatomic, strong) UILabel *fromFirstMinuteLabel;
@property (nonatomic, strong) UILabel *fromSecondMinuteLabel;
@property (nonatomic, strong) UILabel *fromColon;
@property (nonatomic, strong) NSLayoutConstraint *alignmentConstraint;
@property (nonatomic, assign) BOOL performingShakeAnimation;
@end

@implementation TimeEntryView


- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if(self) {
		// Initialization code


		self.backgroundColor = [UIColor clearColor];

		_titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 44)];
		_fromFirstHourLabel  = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 44)];
		_fromSecondHourLabel  = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 44)];
		_fromFirstMinuteLabel  = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 44)];
		_fromSecondMinuteLabel  = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 44)];
		_fromColon  = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 20, 44)];

		_fromFirstHourLabel.translatesAutoresizingMaskIntoConstraints = NO;
		_fromSecondHourLabel.translatesAutoresizingMaskIntoConstraints = NO;
		_fromFirstMinuteLabel.translatesAutoresizingMaskIntoConstraints = NO;
		_fromSecondMinuteLabel.translatesAutoresizingMaskIntoConstraints = NO;
		_fromColon.translatesAutoresizingMaskIntoConstraints = NO;
		_titleLabel.translatesAutoresizingMaskIntoConstraints = NO;

		[self addSubview:_fromFirstHourLabel];
		[self addSubview:_fromSecondHourLabel];
		[self addSubview:_fromFirstMinuteLabel];
		[self addSubview:_fromSecondMinuteLabel];
		[self addSubview:_fromColon];
		[self addSubview:_titleLabel];

		[self configureLabels];

		_tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
		[self addGestureRecognizer:_tapGestureRecognizer];
	}
	return self;
}

#pragma mark -
#pragma mark Gesture Recognizers

- (void)handleTap:(UITapGestureRecognizer *)tapGesture {
	if([_delegate respondsToSelector:@selector(timeEntryViewDidStartEditing:)]) {
		[_delegate timeEntryViewDidStartEditing:self];
	}
}

#pragma mark -
#pragma mark Configure Labels

- (NSArray *)fromLabels {
	return @[_fromFirstHourLabel, _fromSecondHourLabel, _fromFirstMinuteLabel, _fromSecondMinuteLabel, _fromColon];
}

- (void)configureLabels {
	_fromFirstHourLabel.text = @"0";
	_fromSecondHourLabel.text = @"0";
	_fromFirstMinuteLabel.text = @"0";
	_fromSecondMinuteLabel.text = @"0";
	_titleLabel.text = NSLocalizedString(@"FROM", nil);
	_fromColon.text = @":";

	_fromColon.textAlignment = NSTextAlignmentCenter;

	[[self fromLabels] enumerateObjectsUsingBlock:^(UILabel * label, NSUInteger idx, BOOL * stop) {
		 label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:32];
		 label.textColor = [UIColor colorWithWhite:1 alpha:0.75];
		 label.backgroundColor = [UIColor clearColor];
	 }];

	_titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:14];
    _titleLabel.textColor = [UIColor colorWithWhite:1 alpha:0.75];
	_titleLabel.backgroundColor = [UIColor clearColor];

    NSLayoutConstraint *titleWidth = [NSLayoutConstraint constraintWithItem:_titleLabel attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:_fromSecondMinuteLabel attribute:NSLayoutAttributeRight multiplier:1 constant:0];
    [self addConstraint:titleWidth];

    NSLayoutConstraint *centerConstraint = [NSLayoutConstraint constraintWithItem:_fromFirstHourLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:10];
    [self addConstraint:centerConstraint];
    
	NSArray *verticalPosition = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_titleLabel]-(0)-[_fromFirstHourLabel]" options:NSLayoutFormatAlignAllLeft metrics:nil views:NSDictionaryOfVariableBindings (_titleLabel, _fromFirstHourLabel)];
	[self addConstraints:verticalPosition];
    
	NSArray *fromLabelLaylout = [NSLayoutConstraint constraintsWithVisualFormat:@"[_fromFirstHourLabel][_fromSecondHourLabel][_fromColon][_fromFirstMinuteLabel][_fromSecondMinuteLabel]" options:NSLayoutFormatAlignAllCenterY metrics:nil views:NSDictionaryOfVariableBindings (_fromFirstHourLabel, _fromSecondHourLabel, _fromColon, _fromFirstMinuteLabel, _fromSecondMinuteLabel)];
    
    
	[self addConstraints:fromLabelLaylout];

    [self setDigitsAlignment:NSTextAlignmentLeft];
    
	[self layoutIfNeeded];
    
}

- (void)setTitle:(NSString *)title {
	_titleLabel.text = title;
}

- (void)setTitleAlignment:(NSTextAlignment)alignment {
	_titleLabel.textAlignment = alignment;
}

- (void)setDigitsAlignment:(NSTextAlignment)alignment {
    NSLayoutAttribute attribute = NSLayoutAttributeLeft;
    float constant = 20;
    UIView *item = _fromFirstHourLabel;
    
    if (alignment == NSTextAlignmentRight) {
        attribute =NSLayoutAttributeRight;
        constant = -20;
        item = _fromSecondMinuteLabel;
    }
    
    [self removeConstraint:_alignmentConstraint];
    _alignmentConstraint = [NSLayoutConstraint constraintWithItem:item attribute:attribute relatedBy:NSLayoutRelationEqual toItem:self attribute:attribute multiplier:1 constant:constant];
    [self addConstraint:_alignmentConstraint];
}

- (void)setTime:(NSString *)time {
	NSArray *split = [time componentsSeparatedByString:@":"];

	NSString *firstHalf = [split objectAtIndex:0];
	NSString *secondHalf = [split objectAtIndex:1];

	[self setDigit:[[firstHalf substringWithRange:NSMakeRange(0, 1)] intValue] atIndex:0];
	[self setDigit:[[firstHalf substringWithRange:NSMakeRange(1, 1)] intValue] atIndex:1];
	[self setDigit:[[secondHalf substringWithRange:NSMakeRange(0, 1)] intValue] atIndex:2];
	[self setDigit:[[secondHalf substringWithRange:NSMakeRange(1, 1)] intValue] atIndex:3];
}

#pragma mark - Helper Methods

- (void)setDigit:(NSInteger)digit atIndex:(NSInteger)index {
	if(index < 4) {
		UILabel *label = [[self fromLabels] objectAtIndex:index];
		label.text = [@ (digit)description];
	}
}

- (void)selectDigitAtIndex:(NSInteger)index {
	UIView *oldDigit = [[self fromLabels] objectAtIndex:_selectedDigitIndex];
    
	if(index < 4) {
		_selectedDigitIndex = index;
		[oldDigit stopBlinking];
        
		UIView *selectedDigitView = [[self fromLabels] objectAtIndex:_selectedDigitIndex];
		[selectedDigitView startBlinkingWithSpeed:0.4];
	}
}

- (void)setCurrentDigitAndCheckValidity:(NSInteger)digit {
	NSInteger oldDigit = [self digitAtIndex:_selectedDigitIndex];
    
	[self setDigit:digit atIndex:_selectedDigitIndex];
	if(_selectedDigitIndex == 2) {
		if(digit > 5) {
            [self restoreOldDigit:oldDigit];
			return;
		}
	}
    if (_selectedDigitIndex == 0) {
        if (digit > 2) {
            [self restoreOldDigit:oldDigit];
            return;
        }
    }
    
    if (_selectedDigitIndex == 1) {
        if ([self digitAtIndex:0] > 1) {
            if (digit > 4) {
                [self restoreOldDigit:oldDigit];
                return;
            }
        }
    }
    
    if (_selectedDigitIndex == 2 || _selectedDigitIndex == 3) {
        if ([self digitAtIndex:0] > 2 && [self digitAtIndex:1] > 3) {
            if (digit > 0) {
                [self restoreOldDigit:oldDigit];
                return;
            }
        }
    }
    
	[self goToNextDigit];
}

- (BOOL)validDigits {
    NSInteger firstHour = [self digitAtIndex:0] * 60 * 10;
    NSInteger secondHour = [self digitAtIndex:1] * 60;
    NSInteger firstMinute = [self digitAtIndex:2] * 10;
    NSInteger secondMinute = [self digitAtIndex:3];
        
    if ((firstHour  + secondHour + firstMinute + secondMinute) > 24 * 60) {
        return NO;
    }
    return YES;
}

- (NSInteger)digitAtIndex:(NSInteger)digitAtIndex {
    return [[[[self fromLabels]  objectAtIndex:digitAtIndex] text] intValue];
}

- (void)restoreOldDigit:(NSInteger)oldDigit {
    if(!_performingShakeAnimation) {
        UIView *selectedDigitView = [[self fromLabels] objectAtIndex:_selectedDigitIndex];
        
        [selectedDigitView stopBlinking];
        
        _performingShakeAnimation = YES;
        [selectedDigitView shakeAnimationWithDuration:0.1 repeatCount:4 completed:^{
            [self setDigit:oldDigit atIndex:_selectedDigitIndex];
            [selectedDigitView startBlinkingWithSpeed:0.4];
            _performingShakeAnimation = NO;
        }];
    }
}

- (void)setCurrentDigit:(NSInteger)digit {
	[self setDigit:digit atIndex:_selectedDigitIndex];
}

- (void)goToNextDigitStopAtLast:(BOOL)stop {
    if(_selectedDigitIndex < 3) {
		[self selectDigitAtIndex:_selectedDigitIndex + 1];
	} else {
        if (!stop) [self didFinishEditing];
        else [self selectDigitAtIndex:_selectedDigitIndex];
    }
}

- (void)goToNextDigit {
    [self goToNextDigitStopAtLast:NO];
}

- (void)goToPreviousDigit {
	if(_selectedDigitIndex > 0) {
		[self selectDigitAtIndex:_selectedDigitIndex - 1];
	} else {
        [self didFinishEditing];
    }
}

- (void)didFinishEditing {
    if ([_delegate respondsToSelector:@selector(timEntryViewShouldEndEditing:)]) {
        if ([_delegate timEntryViewShouldEndEditing:self]) {
            [self stopBlinking];
            if ([_delegate respondsToSelector:@selector(timeEntryViewDidFinishEditing:)]) {
                [_delegate timeEntryViewDidFinishEditing:self];
            }
        }
    }
}

- (void)stopBlinking {
    UIView *selectedDigitView = [[self fromLabels] objectAtIndex:_selectedDigitIndex];
    [selectedDigitView stopBlinking];
}

@end
