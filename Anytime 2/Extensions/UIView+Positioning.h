//
//  UIView+Positioning.h
//  SchunkTool
//
//  Created by Tudor Munteanu on 25/04/2012.
//  Copyright (c) 2012 Siller AG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Positioning)

//
// Adjusting positions and sizes of UIViews
//
- (void)alignRightWithMargin:(float)margin;
- (void)adjustSize:(CGSize)size;
- (void)adjustWidthTo:(float)width;
- (void)adjustHeightTo:(float)height;
- (void)adjustX:(CGFloat)x;
- (void)adjustY:(CGFloat)y;
- (void) adjustPosition:(CGPoint)position;

- (void)expandHeightBy:(float)height;
- (void)expandToBottom;
- (void)expandWidth:(float)width;
- (void)moveOriginTo:(CGPoint)origin;
- (void)moveToPoint:(CGPoint)point;
- (void)moveToPoint:(CGPoint)point offset:(CGSize)offset;
- (void)moveXBy:(float)offset;
- (void)moveYBy:(float)offset;
- (void)moveToBottomWithMargin:(float)margin;
- (void) moveTopToBottomWithMargin:(float)margin ;

- (void) centerHorizontalWithMargin:(float)margin;
- (void) adjustY:(CGFloat)y withDuration:(NSTimeInterval)duration;
//
// Positioning UIViews relative to other UIViews
//
- (void)moveOriginTo:(CGPoint)distancePoint rightOfView:(UIView *)view;
- (void)appendToView:(UIView *)view margin:(float)margin;
- (void)addToRightOfView:(UIView *)view withMargin:(float)margin;
- (void) addToLeftOfView:(UIView *)view withMargin:(float)margin;
- (void)centerVerticallyWithMargin:(float)margin;
- (void)appendToTopOfView:(UIView *)view margin:(float)margin;

- (void) centerHorizontalWithMargin:(float)margin inFrame:(CGRect)bounds;
- (void) centerVerticallyWithMargin:(float)margin inFrame:(CGRect)bounds;

- (void)alignRightWithOffset:(CGPoint)offset inFrame:(CGRect)frame;
- (void)alignLeftWithOffset:(CGPoint)offset inFrame:(CGRect)frame;

- (void)bringToFront;

//
// Animations
//
- (void)showAnimated:(BOOL)animated completion:(void (^)(void))completionBlock;
- (void)hideAnimated:(BOOL)animated completion:(void (^)(void))completionBlock;
//- (void) slideInAfter:(float)delay duration:(float)duration completion:(void (^)(void))completionBlock;

- (void) slideInView:(UIView *)view after:(float)delay duration:(float)duration completion:(void (^)(UIView *view))completionBlock;

- (void) slideOutAfter:(float)delay duration:(float)duration completion:(void (^)(void))completionBlock;
//
// Information about UIViews
//
- (CGPoint)pointAfterFrameWithOffset:(CGSize)offset;
- (float)bottom;



@end