//
//  ATCalenderMonthViewController.h
//  Anytime 2
//
//  Created by Josef Materi on 08.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ATCalendarMonthViewControllerDelegate;

@interface ATCalenderMonthViewController : UICollectionViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic, strong) NSDateComponents *calenderMonth;

@property (nonatomic, strong) NSDate *date;

@property (nonatomic) NSInteger lineOfCurentSelectedItem;

@property (nonatomic, weak) id <ATCalendarMonthViewControllerDelegate> delegate;

- (void)setDateWithComponents:(NSDateComponents *)comp;

- (void)reloadData;

- (void)highlightCellForDateComponent:(NSDateComponents *)dateComponents;

- (NSInteger)lineForWeekOfYear:(NSInteger)week;

- (void)setLineForSelectedDayHidden:(BOOL)hiden;

- (NSInteger)lineOfCurentSelectedItem;

- (void) selectDayForCurrentDateComponents;

- (void)deselectAllHighlightedDays;

@end

@protocol ATCalendarMonthViewControllerDelegate <NSObject>

- (void)calendarMonthViewController:(ATCalenderMonthViewController *)controller didSelectDate:(NSDateComponents *)component;

@end