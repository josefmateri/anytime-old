//
//  NSDate+Additions.m
//  Anytime 2
//
//  Created by Josef Materi on 25.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "NSDate+Additions.h"

@implementation NSDate (Additions)


- (NSInteger)day {
    
    NSDate *today = self;
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    
    NSDateComponents *weekdayComponents =
    [gregorian components:(NSDayCalendarUnit |
                           NSWeekdayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:today];

    return weekdayComponents.day;
}

- (NSDate *)dateByAddingDays:(NSInteger)days {
    
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDateComponents *comp = [[NSDateComponents alloc] init];
    comp.day = days;
    
    return [calendar dateByAddingComponents:comp toDate:self options:0];
    
}

- (NSDate *)beginningOWeek {
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDate *firstWeekDay = [self firstDayOfTheWeek];
    
    NSDateComponents *comp = [[NSDateComponents alloc] init];
    comp.week = 0;
    return [calendar dateByAddingComponents:comp toDate:firstWeekDay options:0];
}

- (NSDate *)beginningOfNextWeek {
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDate *firstWeekDay = [self firstDayOfTheWeek];
    
    NSDateComponents *comp = [[NSDateComponents alloc] init];
    comp.week = 1;
    return [calendar dateByAddingComponents:comp toDate:firstWeekDay options:0];
}

- (NSDate *)beginningOfPreviousWeek {
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    
    NSDate *firstWeekDay = [self firstDayOfTheWeek];
    
    NSDateComponents *comp = [[NSDateComponents alloc] init];
    comp.week = -1;
    return [calendar dateByAddingComponents:comp toDate:firstWeekDay options:0];
}

- (NSDate *)firstDayOfTheMonth {
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* comps = [calendar components:NSYearForWeekOfYearCalendarUnit |NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:self];
    
    [comps setDay:1];
    
    return [calendar dateFromComponents:comps];
}


- (NSDate *)firstDayOfTheWeek {
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    NSDateComponents* comps = [calendar components:NSYearForWeekOfYearCalendarUnit |NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:self];
    
    [comps setWeekday:[calendar firstWeekday]]; // 2: monday
    
    return [calendar dateFromComponents:comps];
}

- (NSInteger)numberOfWeek {
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* comps = [calendar components:NSYearForWeekOfYearCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekOfMonthCalendarUnit fromDate:self];
    return comps.weekOfMonth;
}

- (NSString *)longDescription {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateStyle = NSDateFormatterLongStyle;
    return [dateFormatter stringFromDate:self];
}

- (NSString *)currentWeekDescription {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* comps = [calendar components:NSYearForWeekOfYearCalendarUnit |NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:self];
   
    return [NSString stringWithFormat:@"KW %i", comps.week];
}

- (NSDateComponents *)componentsByAddingDays:(NSInteger)days {
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents *comp = [[NSDateComponents alloc] init];
    comp.day = days;
    
    NSDate *date = [calendar dateByAddingComponents:comp toDate:self options:0];
    
    NSDateComponents* comps = [calendar components:NSDayCalendarUnit |
                                                    NSWeekdayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:date];
    
    return comps;
}

@end
