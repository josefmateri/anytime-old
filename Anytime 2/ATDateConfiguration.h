//
//  ATDateConfiguration.h
//  Anytime 2
//
//  Created by Josef Materi on 13.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ATDateConfiguration : NSObject

@property (nonatomic) NSInteger firstDayOfTheWeek;

+ (instancetype)currentConfiguration;

- (NSDateComponents *)dateComponentsFromDate:(NSDate *)date;
- (NSDate *)dateFromDateComponents:(NSDateComponents *)dateComponents;

@end
