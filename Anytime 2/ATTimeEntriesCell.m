//
//  ATTimeEntriesCell.m
//  Anytime 2
//
//  Created by Josef Materi on 22.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATTimeEntriesCell.h"

@implementation ATTimeEntriesCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    
    if (self) {
        // Initialization code
                

        
        self.headline1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        self.headline2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        self.subline1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        self.subline2 = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
        self.minutes = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];

        
        self.headline1.translatesAutoresizingMaskIntoConstraints = NO;
        self.headline2.translatesAutoresizingMaskIntoConstraints = NO;
        self.subline1.translatesAutoresizingMaskIntoConstraints = NO;
        self.subline2.translatesAutoresizingMaskIntoConstraints = NO;
        self.minutes.translatesAutoresizingMaskIntoConstraints = NO;

        [self.contentView addSubview:_headline1];
        [self.contentView addSubview:_headline2];
        [self.contentView addSubview:_subline1];
//        [self.contentView addSubview:_subline2];
        [self.contentView addSubview:_minutes];

        
        self.headline1.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
        
        self.headline2.font = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
        
        self.subline1.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
        
        self.subline2.font = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption2];
        
        self.minutes.font = [UIFont systemFontOfSize:14];
        
        self.subline2.textAlignment = NSTextAlignmentRight;
        
        NSArray *constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_headline1]-(0)-[_headline2]-(0)-[_subline1]" options:0 metrics:Nil views:NSDictionaryOfVariableBindings(_headline1, _headline2, _subline1)];
        
        [self.contentView addConstraints:constraint];
        
        NSLayoutConstraint *heightConst = [NSLayoutConstraint constraintWithItem:_minutes attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:Nil attribute:NSLayoutAttributeHeight multiplier:1 constant:_minutes.font.pointSize + 8];
        [self.contentView addConstraint:heightConst];

        
        [self.contentView addConstraints:constraint];

         constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[_headline1]-20-[_minutes(50)]-10-|" options:0 metrics:Nil views:NSDictionaryOfVariableBindings(_headline1, _headline2, _subline2, _minutes)];
//
        [self.contentView addConstraints:constraint];
        
//        constraint = [NSLayoutConstraint constraintsWithVisualFormat:@"|-(10)-[_subline2(>=40)]-(10)-[_headline1]" options:NSLayoutFormatAlignAllTop metrics:Nil views:NSDictionaryOfVariableBindings(_headline1, _headline2, _subline2, _minutes)];
//        //
//        [self.contentView addConstraints:constraint];
//        
        
        
        NSLayoutConstraint *center = [NSLayoutConstraint constraintWithItem:_headline2 attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];

        [self.contentView addConstraint:center];

        
        center = [NSLayoutConstraint constraintWithItem:_minutes attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
        
        [self.contentView addConstraint:center];

        
//        center = [NSLayoutConstraint constraintWithItem:_subline2 attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.headline1 attribute:NSLayoutAttributeTop multiplier:1 constant:0];
//        
//        [self.contentView addConstraint:center];
        
        
        center = [NSLayoutConstraint constraintWithItem:_headline2 attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:_headline1 attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
        
        [self.contentView addConstraint:center];
        
        
        center = [NSLayoutConstraint constraintWithItem:_subline1 attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:_headline1 attribute:NSLayoutAttributeLeft multiplier:1 constant:0];
        
        [self.contentView addConstraint:center];

        [self.contentView layoutIfNeeded];
        
        
        
        // Style
        
        _minutes.backgroundColor = [UIColor colorWithRed:0.358 green:0.472 blue:0.590 alpha:1.000];
        _minutes.textColor = [UIColor whiteColor];
        _minutes.layer.cornerRadius = 10;
        _minutes.textAlignment = NSTextAlignmentCenter;
        
        
        
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    if (selected) {
        _minutes.backgroundColor = self.minutesBackgroundColor;
    }
    // Configure the view for the selected state
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    if (highlighted) {
        _minutes.backgroundColor = self.minutesBackgroundColor;
    }
}

- (void)setMinutesBackgroundColor:(UIColor *)minutesBackgroundColor {
    _minutesBackgroundColor = minutesBackgroundColor;
    self.minutes.backgroundColor = minutesBackgroundColor;
}
@end
