//
//  ATProjectsViewController.h
//  anytime
//
//  Created by Josef Materi on 28.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ATListViewController.h"

@interface ATProjectsViewController : ATListViewController
@property (nonatomic, weak) id <ATCategoryDatasourceProtocol> categoryDatasource;
@end
