// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MiteTimeEntry.h instead.

#import <CoreData/CoreData.h>


extern const struct MiteTimeEntryAttributes {
	__unsafe_unretained NSString *created_at;
	__unsafe_unretained NSString *customer_id;
	__unsafe_unretained NSString *customer_name;
	__unsafe_unretained NSString *date_at;
	__unsafe_unretained NSString *minutes;
	__unsafe_unretained NSString *project_id;
	__unsafe_unretained NSString *project_name;
	__unsafe_unretained NSString *remote_id;
	__unsafe_unretained NSString *service_id;
	__unsafe_unretained NSString *service_name;
	__unsafe_unretained NSString *trackingMinutes;
	__unsafe_unretained NSString *trackingSince;
	__unsafe_unretained NSString *updated_at;
} MiteTimeEntryAttributes;

extern const struct MiteTimeEntryRelationships {
	__unsafe_unretained NSString *project;
	__unsafe_unretained NSString *service;
} MiteTimeEntryRelationships;

extern const struct MiteTimeEntryFetchedProperties {
} MiteTimeEntryFetchedProperties;

@class MiteProject;
@class MiteService;















@interface MiteTimeEntryID : NSManagedObjectID {}
@end

@interface _MiteTimeEntry : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MiteTimeEntryID*)objectID;





@property (nonatomic, strong) NSDate* created_at;



//- (BOOL)validateCreated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* customer_id;



@property int64_t customer_idValue;
- (int64_t)customer_idValue;
- (void)setCustomer_idValue:(int64_t)value_;

//- (BOOL)validateCustomer_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* customer_name;



//- (BOOL)validateCustomer_name:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* date_at;



//- (BOOL)validateDate_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* minutes;



@property int64_t minutesValue;
- (int64_t)minutesValue;
- (void)setMinutesValue:(int64_t)value_;

//- (BOOL)validateMinutes:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* project_id;



@property int64_t project_idValue;
- (int64_t)project_idValue;
- (void)setProject_idValue:(int64_t)value_;

//- (BOOL)validateProject_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* project_name;



//- (BOOL)validateProject_name:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* remote_id;



@property int64_t remote_idValue;
- (int64_t)remote_idValue;
- (void)setRemote_idValue:(int64_t)value_;

//- (BOOL)validateRemote_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* service_id;



@property int64_t service_idValue;
- (int64_t)service_idValue;
- (void)setService_idValue:(int64_t)value_;

//- (BOOL)validateService_id:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* service_name;



//- (BOOL)validateService_name:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* trackingMinutes;



@property int64_t trackingMinutesValue;
- (int64_t)trackingMinutesValue;
- (void)setTrackingMinutesValue:(int64_t)value_;

//- (BOOL)validateTrackingMinutes:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* trackingSince;



//- (BOOL)validateTrackingSince:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* updated_at;



//- (BOOL)validateUpdated_at:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) MiteProject *project;

//- (BOOL)validateProject:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) MiteService *service;

//- (BOOL)validateService:(id*)value_ error:(NSError**)error_;





@end

@interface _MiteTimeEntry (CoreDataGeneratedAccessors)

@end

@interface _MiteTimeEntry (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveCreated_at;
- (void)setPrimitiveCreated_at:(NSDate*)value;




- (NSNumber*)primitiveCustomer_id;
- (void)setPrimitiveCustomer_id:(NSNumber*)value;

- (int64_t)primitiveCustomer_idValue;
- (void)setPrimitiveCustomer_idValue:(int64_t)value_;




- (NSString*)primitiveCustomer_name;
- (void)setPrimitiveCustomer_name:(NSString*)value;




- (NSDate*)primitiveDate_at;
- (void)setPrimitiveDate_at:(NSDate*)value;




- (NSNumber*)primitiveMinutes;
- (void)setPrimitiveMinutes:(NSNumber*)value;

- (int64_t)primitiveMinutesValue;
- (void)setPrimitiveMinutesValue:(int64_t)value_;




- (NSNumber*)primitiveProject_id;
- (void)setPrimitiveProject_id:(NSNumber*)value;

- (int64_t)primitiveProject_idValue;
- (void)setPrimitiveProject_idValue:(int64_t)value_;




- (NSString*)primitiveProject_name;
- (void)setPrimitiveProject_name:(NSString*)value;




- (NSNumber*)primitiveRemote_id;
- (void)setPrimitiveRemote_id:(NSNumber*)value;

- (int64_t)primitiveRemote_idValue;
- (void)setPrimitiveRemote_idValue:(int64_t)value_;




- (NSNumber*)primitiveService_id;
- (void)setPrimitiveService_id:(NSNumber*)value;

- (int64_t)primitiveService_idValue;
- (void)setPrimitiveService_idValue:(int64_t)value_;




- (NSString*)primitiveService_name;
- (void)setPrimitiveService_name:(NSString*)value;




- (NSNumber*)primitiveTrackingMinutes;
- (void)setPrimitiveTrackingMinutes:(NSNumber*)value;

- (int64_t)primitiveTrackingMinutesValue;
- (void)setPrimitiveTrackingMinutesValue:(int64_t)value_;




- (NSDate*)primitiveTrackingSince;
- (void)setPrimitiveTrackingSince:(NSDate*)value;




- (NSDate*)primitiveUpdated_at;
- (void)setPrimitiveUpdated_at:(NSDate*)value;





- (MiteProject*)primitiveProject;
- (void)setPrimitiveProject:(MiteProject*)value;



- (MiteService*)primitiveService;
- (void)setPrimitiveService:(MiteService*)value;


@end
