//
//  ATStopWatchDateSelection.m
//  Anytime 2
//
//  Created by Josef Materi on 23.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATStopWatchDateSelection.h"
#import "UIView+FLKAutoLayout.h"
#import "DMLazyScrollView.h"
#import "ATCalenderMonthViewController.h"
#import "ATCalendarWeekHeaderView.h"
#import <Masonry.h>

@interface ATStopWatchDateSelection() <ATCalendarMonthViewControllerDelegate, DMLazyScrollViewDelegate>
@property (nonatomic, strong) DMLazyScrollView *monthScollView;
@property (nonatomic, strong) NSMutableDictionary *monthViewControllers;
@property (nonatomic, strong) NSDateComponents *dateComponents;
@property (nonatomic, strong) UILabel *monthLabel;
@property (nonatomic, strong) UIView *bottomBorder;
@end

@implementation ATStopWatchDateSelection

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.dateView = [[ATDateView alloc] initWithFrame:CGRectMake(0, 280, self.bounds.size.width, 80)];
        [self addSubview:self.dateView];
        
        [self addConstraints:[self.dateView constrainHeight:@"80"]];
        [self addConstraints:[self.dateView constrainWidth:[@(self.bounds.size.width) description]]];
        
        self.dateView.translatesAutoresizingMaskIntoConstraints = NO;
        
        NSLayoutConstraint *dateSelectionView = [NSLayoutConstraint constraintWithItem:self.dateView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0];
        [self addConstraint:dateSelectionView];
        
        dateSelectionView = [NSLayoutConstraint constraintWithItem:self.dateView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
        [self addConstraint:dateSelectionView];
        
        self.dateView.dayLabel.text = @"18.";
        self.dateView.monthLabel.text = @"Juli";
        self.dateView.weekDayLabel.text = @"Sonntag";
        self.dateView.yearLabel.text = @"2013";
        self.dateView.textColor = [UIColor whiteColor];
        self.dateView.secondMonthLabel.hidden = YES;
        self.dateView.secondYearLabel.hidden = YES;
        
  
        
        
        self.clipsToBounds = YES;

        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1)];
        line.backgroundColor = [UIColor colorWithRed:0.376 green:0.412 blue:0.447 alpha:1.000];
        [self addSubview:line];
        line.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        
        self.bottomBorder = line;
        
    }
    return self;
}

- (void)setBottomBorderColor:(UIColor *)bottomBorderColor {
    self.bottomBorder.backgroundColor = bottomBorderColor;
}

- (void)loadMonthCalendar {
    
    self.monthViewControllers = [NSMutableDictionary dictionary];

    ATCalendarWeekHeaderView *weekHeaderView = [[ATCalendarWeekHeaderView alloc] initWithFrame:CGRectMake(0, self.dateView.frame.origin.y + self.dateView.frame.size.height, self.bounds.size.width, 30)];
    [self addSubview:weekHeaderView];
    weekHeaderView.alpha = 0.5;
    weekHeaderView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:[weekHeaderView constrainWidth:[@(self.bounds.size.width) description]]];
    [self addConstraints:[weekHeaderView constrainHeight:[@(30) description]]];
    [self addConstraints:[weekHeaderView constrainTopSpaceToView:self.dateView predicate:@"0"]];

    self.monthScollView = [[DMLazyScrollView alloc] initWithFrameAndDirection:CGRectMake(0, self.dateView.frame.origin.y + self.dateView.frame.size.height, self.bounds.size.width, 300) direction:DMLazyScrollViewDirectionHorizontal circularScroll:YES];
   
    [self addSubview:self.monthScollView];
   
    [self addConstraints:[self.monthScollView constrainWidth:[@(self.bounds.size.width) description]]];
    [self addConstraints:[self.monthScollView constrainHeight:[@(300) description]]];
    [self addConstraints:[self.monthScollView constrainTopSpaceToView:weekHeaderView predicate:@"0"]];
   
    self.monthScollView.controlDelegate = self;
   
    __weak __typeof(&*self)weakSelf = self;
    self.monthScollView.dataSource = ^(NSUInteger index) {
        return [weakSelf monthCollectionViewAtIndex:index];
    };
   
    self.monthScollView.numberOfPages = 3;

}

- (void)setMonthCalendarHidden:(BOOL)hidden {
    
    if (!self.monthViewControllers) {
        [self loadMonthCalendar];
    }
}

- (void)setMonthLabelAlpha:(float)alpha {
    
    float hidden = 1 - alpha;
    
    self.dateView.alpha = hidden;
    [self.dateView setSecondMonthLabelAlpha:alpha];
    
    [self highlightCurrentDayInMonthView];
}

- (void)configureWithDateComponents:(NSDateComponents *)comp {
    
    self.dateComponents = comp;
    
    [self.dateView updateWithDateComponents:comp];
    
    ATCalenderMonthViewController *cal = [self monthCollectionViewAtIndex:self.monthScollView.currentPage];

    [cal setDateWithComponents:comp];
    
    [self.monthScollView reloadData];
}

- (void)highlightCurrentDayInMonthView {

    ATCalenderMonthViewController *cal = [self monthCollectionViewAtIndex:self.monthScollView.currentPage];
    [cal highlightCellForDateComponent:self.dateComponents];
}


- (ATCalenderMonthViewController *)monthCollectionViewAtIndex:(NSInteger)index {
    
    ATCalenderMonthViewController *calendarMonthViewController = self.monthViewControllers[@(index % 3)];
    if (!calendarMonthViewController) {
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(40, 40);
        layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5);
        layout.minimumInteritemSpacing = 5.0f;
        layout.minimumLineSpacing = 5.0f;
        layout.scrollDirection =UICollectionViewScrollDirectionVertical;
        
        
        NSDateComponents *calenderMonth = [[NSDateComponents alloc] init];
        calenderMonth.month = 7 + index;
        calenderMonth.day = 1;
        calenderMonth.year = 2013;
        
        
        calendarMonthViewController = [[ATCalenderMonthViewController alloc] initWithCollectionViewLayout:layout];
        [calendarMonthViewController setDateWithComponents:calenderMonth];
        
        [calendarMonthViewController.collectionView reloadData];
        
        calendarMonthViewController.delegate = self;
        
        self.monthViewControllers[@(index)] = calendarMonthViewController;
    }
    
    return calendarMonthViewController;
    
}

- (void)calendarMonthViewController:(ATCalenderMonthViewController *)controller didSelectDate:(NSDateComponents *)component {
    
    [self.dateView updateWithDateComponents:component];
    
    [self setMonthNameFromDateComponent:component];
    
    self.dateComponents = component;
    
    __strong id <ATStopWatchDateSelection> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(stopWatchDateSelection:didSelectDateWithComponents:)]) {
        [delegate stopWatchDateSelection:self didSelectDateWithComponents:component];
    }
    
    [self.monthViewControllers enumerateKeysAndObjectsUsingBlock:^(id key, ATCalenderMonthViewController *obj, BOOL *stop) {
    
        if ([[obj.collectionView indexPathsForSelectedItems] count] > 0 && ![obj isEqual:controller]) {
            [obj.collectionView deselectItemAtIndexPath:[[obj.collectionView indexPathsForSelectedItems] firstObject] animated:NO];
        }
        
    }];
}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView currentPageChanged:(NSInteger)currentPageIndex {
    
    
    
}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView didChangeToNextPageWithViewController:(UIViewController *)viewController beforeViewController:(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController {
    
    [self didChangeToPage:(ATCalenderMonthViewController *)viewController after:(ATCalenderMonthViewController *)afterViewController before:(ATCalenderMonthViewController *)beforeViewController];
    
}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView didChangeToPrevPageWithViewController:(UIViewController *)viewController beforeViewController:(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController {
    
    [self didChangeToPage:(ATCalenderMonthViewController *)viewController after:(ATCalenderMonthViewController *)afterViewController before:(ATCalenderMonthViewController *)beforeViewController];
    
}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView didChangeToViewController:(UIViewController *)viewController beforeViewController:(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController {
    
    [self didChangeToPage:(ATCalenderMonthViewController *)viewController after:(ATCalenderMonthViewController *)afterViewController before:(ATCalenderMonthViewController *)beforeViewController];
    
}


- (void)didChangeToPage:(ATCalenderMonthViewController *)currentViewController after:(ATCalenderMonthViewController *)afterViewController before:(ATCalenderMonthViewController *)beforeViewController {
    
    
    ATCalenderMonthViewController *current = (ATCalenderMonthViewController *)currentViewController;
    NSDateComponents *component = current.calenderMonth;
    
    if (component.month == self.dateComponents.month) {
        [current highlightCellForDateComponent:self.dateComponents];
    }
    
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:component];
    NSDateComponents *addMonth = [[NSDateComponents alloc] init];
    addMonth.month = 1;
    
    NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:addMonth toDate:date options:0];
    
    NSDateComponents *nextMonth = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear fromDate:newDate];
    
    addMonth.month = -1;
    
    newDate = [[NSCalendar currentCalendar] dateByAddingComponents:addMonth toDate:date options:0];
    
    NSDateComponents *prevMonth = [[NSCalendar currentCalendar] components:NSCalendarUnitMonth | NSCalendarUnitYear fromDate:newDate];
    
    ATCalenderMonthViewController *after = (ATCalenderMonthViewController *)afterViewController;
    after.calenderMonth = nextMonth;
    [after reloadData];
    [after highlightCellForDateComponent:self.dateComponents];

    ATCalenderMonthViewController *before = (ATCalenderMonthViewController *)beforeViewController;
    before.calenderMonth = prevMonth;
    [before reloadData];
    [before highlightCellForDateComponent:self.dateComponents];

    [self setMonthNameFromDateComponent:component];
}

- (void)setMonthNameFromDateComponent:(NSDateComponents *)component {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
    
    dateFormatter.calendar = [NSCalendar currentCalendar];
    NSString *monthName = [[dateFormatter monthSymbols] objectAtIndex:component.month - 1];
    
    self.dateView.secondMonthLabel.text = monthName;
    self.dateView.secondYearLabel.text = [@(component.year) description];
}


- (void)registerTouchesBeganBlock:(void (^)())touchesBeganBlock touchesEndedBlock:(void (^)())touchesEndedBlock touchesMoved:(void (^)())touchesMovedBlock touchesCancled:(void (^)())touchesCancledBlock {
    self.touchesBeganBlock = touchesBeganBlock;
    self.touchesCancledBlock = touchesCancledBlock;
    self.touchesMovedBlock = touchesMovedBlock;
    self.touchesEndedBlock = touchesEndedBlock;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    if (self.touchesBeganBlock) self.touchesBeganBlock();
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    if (self.touchesEndedBlock) self.touchesEndedBlock();
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    if (self.touchesCancledBlock) self.touchesCancledBlock();
}

@end
