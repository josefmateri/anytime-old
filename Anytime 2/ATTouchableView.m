//
//  ATTouchableView.m
//  anytime
//
//  Created by Josef Materi on 26.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATTouchableView.h"

@implementation ATTouchableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)registerTouchesBeganBlock:(void (^)())touchesBeganBlock touchesEndedBlock:(void (^)())touchesEndedBlock touchesMoved:(void (^)())touchesMovedBlock touchesCancled:(void (^)())touchesCancledBlock {
    self.touchesBeganBlock = touchesBeganBlock;
    self.touchesCancledBlock = touchesCancledBlock;
    self.touchesMovedBlock = touchesMovedBlock;
    self.touchesEndedBlock = touchesEndedBlock;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    if (self.touchesBeganBlock) self.touchesBeganBlock();
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesEnded:touches withEvent:event];
    if (self.touchesEndedBlock) self.touchesEndedBlock();
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    [super touchesCancelled:touches withEvent:event];
    if (self.touchesCancledBlock) self.touchesCancledBlock();
}

@end
