//
//  BackgroundView.m
//  pocket
//
//  Created by Josef Materi on 04.01.11.
//  Copyright 2011 materi design. All rights reserved.
//

#import "BackgroundView.h"

#define gridPadding 8
#define gridLineWidth 2

@implementation BackgroundView
@synthesize gradientColor, position, cornerRadius, tintColor, gridColor;
@synthesize hideBackground;

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code.
		self.gradientColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
		self.backgroundColor = [UIColor colorWithRed: 0.808 green: 0.824 blue: 0.839 alpha: 0.000];
		self.tintColor = [UIColor colorWithRed: 0.808 green: 0.824 blue: 0.839 alpha: 1.000];
		self.cornerRadius = 10;
		self.position = BackgroundViewPositionMiddle;
		self.gridColor = [UIColor colorWithWhite:1 alpha:0.1];
	}
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    if (hideBackground) return;
	
	CGContextRef c = UIGraphicsGetCurrentContext();
	[[self backgroundColor] setFill];
    
	float lineWidth = 1;
	
	CGRect newRect = rect;
	newRect.size.width -= lineWidth;
	newRect.size.height -= lineWidth;
	newRect.origin.x += lineWidth / 2.0;
	newRect.origin.y += lineWidth / 2.0;
	
	CGFloat minx = CGRectGetMinX(rect), midx = CGRectGetMidX(rect), maxx = CGRectGetMaxX(rect);
	CGFloat miny = CGRectGetMinY(rect), midy = CGRectGetMidY(rect), maxy = CGRectGetMaxY(rect);
	
	float _topLeft = 0;
	float _topRight = 0;
	float _bottomRight = 0;
	float _bottomLeft = 0;
	
	if (position == BackgroundViewPositionTop) {
		_topLeft = cornerRadius;
		_topRight = cornerRadius;
	}
	
	if (position == BackgroundViewPositionBottom) {
		_bottomLeft = cornerRadius;
		_bottomRight = cornerRadius;
	}
	
	if (position == BackgroundViewPositionSingle) {
		_topLeft = cornerRadius;
		_topRight = cornerRadius;
		_bottomLeft = cornerRadius;
		_bottomRight = cornerRadius;
	}
	
	CGMutablePathRef path = CGPathCreateMutable();
	CGPathMoveToPoint(path, NULL, minx, midy);
	CGPathAddArcToPoint(path, NULL, minx, miny, midx, miny, _topLeft);
	CGPathAddArcToPoint(path, NULL, maxx, miny, maxx, midy, _topRight);
	CGPathAddArcToPoint(path, NULL, maxx, maxy, midx, maxy, _bottomRight);
	CGPathAddArcToPoint(path, NULL, minx, maxy, minx, midy, _bottomLeft);
	CGPathCloseSubpath(path);
	
	// Fill and stroke the path
	CGContextAddPath(c, path);
	CGContextClip(c);
	
	//[[self backgroundColor] setFill];
	[tintColor setFill];
	
	CGContextFillRect(c,rect);
	
	[self drawGradiantWithColor:[self gradientColor] toColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0] forRect:rect inContext:c];
	
	// Drawing code.
	
	for (int i = 0; i < 320; i = i + (gridLineWidth + gridPadding)) {
		
		CGContextMoveToPoint(c, i, 0);
		CGContextAddLineToPoint(c, i, rect.size.height);
		[[self gridColor] set];
		CGContextStrokePath(c);
		
	}
	CGPathRelease(path);
}


- (void)drawGradiantWithColor: (UIColor *) fromColor toColor: (UIColor *) secondColor forRect:(CGRect)rect inContext:(CGContextRef)c {
    
		CGColorRef startColor = [fromColor CGColor];
		const float *rgba = CGColorGetComponents(startColor);
		float r = rgba[0];
		float g = rgba[1];
		float b = rgba[2];
		float a = rgba[3];
		
		CGColorRef endColor = [secondColor CGColor];
		const float *rgba2 = CGColorGetComponents(endColor);
		float r2 = rgba2[0];
		float g2 = rgba2[1];
		float b2 = rgba2[2];
		float a2 = rgba2[3];
		
		CGColorSpaceRef rgb = CGColorSpaceCreateDeviceRGB();
		
		CGFloat colors[] = {
			r, g, b, a,
			r2, g2, b2, a2
		};
		
		CGGradientRef gradient =  CGGradientCreateWithColorComponents( rgb, colors, NULL, sizeof(colors) / (sizeof(colors[0]) * 3) );
		CGColorSpaceRelease(rgb);
		
    CGContextDrawRadialGradient(c, gradient, CGPointMake(round(rect.size.width / 2), 0), 0, CGPointMake(round(rect.size.width / 2), 0), rect.size.width * 0.75, 0);
	
    CGGradientRelease(gradient);

}


@end
