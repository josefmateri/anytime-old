//
//  UIView+Positioning.m
//  SchunkTool
//
//  Created by Tudor Munteanu on 25/04/2012.
//  Copyright (c) 2012 Siller AG. All rights reserved.
//

#import "UIView+Positioning.h"

#define RANDOM_INT(__MIN__, __MAX__) ( (__MIN__) + arc4random() % ((__MAX__ + 1) - (__MIN__)) )
#define DEGREES_TO_RADIANS(__ANGLE__) ((__ANGLE__) / 180.0 * M_PI)


@implementation UIView (Positioning)

- (void) moveOriginTo:(CGPoint)origin {
    
    [self setFrame:CGRectMake(origin.x, origin.y, self.frame.size.width, self.frame.size.height)];
}

- (void) moveOriginTo:(CGPoint)distancePoint rightOfView:(UIView *)view {
    
    [self setFrame:CGRectMake(view.frame.origin.x + view.frame.size.width + distancePoint.x, 
                              view.frame.origin.y + distancePoint.y, 
                              self.frame.size.width, 
                              self.frame.size.height)];
}

- (void) moveToPoint:(CGPoint)point {
    
    CGRect thisFrame = self.frame;
    thisFrame.origin = point;
    self.frame = thisFrame;
    
}

- (void) moveToPoint:(CGPoint)point offset:(CGSize)offset {
    CGRect thisFrame = self.frame;
    thisFrame.origin = point;
    thisFrame.origin.x += offset.width;
    thisFrame.origin.y += offset.height;
    self.frame = thisFrame;
}

- (void) moveXBy:(float)offset {
    
    CGRect thisFrame = self.frame;
    thisFrame.origin.x += offset;
    self.frame = thisFrame;
}

- (void) moveYBy:(float)offset {
    
    CGRect thisFrame = self.frame;
    thisFrame.origin.y += offset;
    self.frame = thisFrame;
}

- (void) appendToView:(UIView *)view margin:(float)margin {
    
    CGRect thisFrame = self.frame;
    thisFrame.origin.y = [view bottom] + margin;
    //thisFrame.origin.x = view.frame.origin.x;
    self.frame = CGRectIntegral(thisFrame);
}

- (void)appendToTopOfView:(UIView *)view margin:(float)margin {
    CGRect thisFrame = self.frame;
    thisFrame.origin.y = view.frame.origin.y - margin - thisFrame.size.height;
    self.frame = CGRectIntegral(thisFrame);
}

- (void) addToRightOfView:(UIView *)view withMargin:(float)margin {
    
    CGRect frame = self.frame;
    frame.origin.y = view.frame.origin.y;
    frame.origin.x = view.frame.origin.x + view.frame.size.width + margin;
    self.frame = frame;
}

- (void) addToLeftOfView:(UIView *)view withMargin:(float)margin {
    CGRect frame = self.frame;
    frame.origin.y = view.frame.origin.y;
    frame.origin.x = view.frame.origin.x - view.frame.size.width - margin;
    self.frame = frame;
}

- (float) bottom {
    return self.frame.origin.y + self.frame.size.height;
}

- (void) expandToBottom {
    CGRect thisFrame = self.frame;
    thisFrame.size.height = self.superview.frame.size.height - thisFrame.origin.y;
    self.frame = thisFrame;
}

- (void) moveTopToBottomWithMargin:(float)margin {
    CGRect thisFrame = self.frame;
    thisFrame.origin.y = self.superview.frame.size.height - margin;
    self.frame = thisFrame;
}

- (void) moveToBottomWithMargin:(float)margin {
    CGRect thisFrame = self.frame;
    thisFrame.origin.y = self.superview.frame.size.height - thisFrame.size.height - margin;
    self.frame = thisFrame;
}

- (void) adjustX:(CGFloat)x {
    CGRect thisFrame = self.frame;
    thisFrame.origin.x = x;
    self.frame = thisFrame;
}

- (void) adjustY:(CGFloat)y {
    
    CGRect thisFrame = self.frame;
    thisFrame.origin.y = y;
    self.frame = thisFrame;
}

- (void) adjustY:(CGFloat)y withDuration:(NSTimeInterval)duration {
    [UIView animateWithDuration:duration animations:^{
        [self adjustY:y];
    }];
}

- (void) adjustPosition:(CGPoint)position {
    CGRect thisFrame = self.frame;
    thisFrame.origin = position;
    self.frame = thisFrame;
}

- (void) expandWidth:(float)width {
    CGRect thisFrame = self.frame;
    thisFrame.size.width +=width;
    self.frame = thisFrame;
}

- (void) expandHeightBy:(float)height {
    CGRect thisFrame = self.frame;
    thisFrame.size.height +=height;
    self.frame = thisFrame;
}

- (void) adjustWidthTo:(float)width {
    
    CGRect thisFrame = self.frame;
    thisFrame.size.width = width;
    self.frame = thisFrame;    
}

- (void) alignRightWithMargin:(float)margin {
    
    CGRect thisFrame = self.frame;
    thisFrame.origin.x = self.superview.frame.size.width - thisFrame.size.width - margin;
    self.frame = thisFrame;
}

- (void)alignLeftWithOffset:(CGPoint)offset inFrame:(CGRect)frame {
    CGRect thisFrame = self.frame;
    thisFrame.origin.x = frame.origin.x + offset.x;
    thisFrame.origin.y = frame.origin.y + offset.y;
    self.frame = thisFrame;
}

- (void)alignRightWithOffset:(CGPoint)offset inFrame:(CGRect)frame {
    CGRect thisFrame = self.frame;
    thisFrame.origin.x = frame.origin.x + offset.x + frame.size.width - thisFrame.size.width;
    thisFrame.origin.y = frame.origin.y + offset.y;
    self.frame = thisFrame;
}


- (CGPoint) pointAfterFrameWithOffset:(CGSize)offset {
    CGRect thisFrame = self.frame;
    return CGPointMake(thisFrame.origin.x + thisFrame.size.width + offset.width, thisFrame.origin.y + offset.height);
}

- (void) adjustHeightTo:(float)height {
    CGRect thisFrame = self.frame;
    thisFrame.size.height = height;
    self.frame = thisFrame;   
}

- (void)bringToFront {
    [self.superview bringSubviewToFront:self];
}

- (void) showAnimated:(BOOL)animated completion:(void (^)(void))completionBlock {
    
    __block CGRect thisFrame = self.frame;
    
    float duration = 0;
    
    if (animated) duration = 0.35;
    
    thisFrame.origin.y = self.superview.bounds.size.height;
    self.frame = thisFrame;
    
    [UIView animateWithDuration:duration delay:0 options:0 animations:^{
        thisFrame.origin.y = self.superview.bounds.size.height - thisFrame.size.height;
        self.frame = thisFrame;
    } completion:^(BOOL finished) {
        completionBlock();
    }];
}

- (void) hideAnimated:(BOOL)animated completion:(void (^)(void))completionBlock {
    
    __block CGRect thisFrame = self.frame;
    
    float duration = 0;
    
    if (animated) duration = 0.35;
    
    [UIView animateWithDuration:duration delay:0 options:0 animations:^{
        thisFrame.origin.y = self.superview.bounds.size.height;
        self.frame = thisFrame;
    } completion:^(BOOL finished) {
        completionBlock();
    }];
}

- (void) centerVerticallyWithMargin:(float)margin {
    
    __block CGRect thisFrame = self.frame;
    CGRect superViewFrame = self.superview.frame;
    float top = (superViewFrame.size.height - thisFrame.size.height) / 2 + margin;
    
    [self adjustY:top];
}

- (void) centerHorizontalWithMargin:(float)margin {
    
    __block CGRect thisFrame = self.frame;
    CGRect superViewFrame = self.superview.frame;
    float left = (superViewFrame.size.width - thisFrame.size.width) / 2 + margin;
    
    [self adjustX:left];
}


- (void) centerHorizontalWithMargin:(float)margin inFrame:(CGRect)bounds {
    __block CGRect thisFrame = self.frame;
    CGRect superViewFrame = bounds;
    float left = superViewFrame.origin.x + (superViewFrame.size.width - thisFrame.size.width) / 2 + margin;
    [self adjustY:bounds.origin.y];
    [self adjustX:left];
}

- (void) centerVerticallyWithMargin:(float)margin inFrame:(CGRect)bounds {
    __block CGRect thisFrame = self.frame;
    CGRect superViewFrame = bounds;
    float left = (superViewFrame.size.height - thisFrame.size.height) / 2 + margin + bounds.origin.y;
    [self adjustY:left];
}

- (void)adjustSize:(CGSize)size {
    CGRect thisFrame = self.frame;
    thisFrame.size = size;
    self.frame = thisFrame;
}

- (void) slideInView:(UIView *)view after:(float)delay duration:(float)duration completion:(void (^)(UIView *view))completionBlock
 {
    
     [view addSubview:self];

    UIView *superview = self.superview;
     [self adjustY:superview.bounds.size.height + 50];
     
     float random = arc4random() % 20;
     float radius = random - 10;
     
     int ran = RANDOM_INT(-80, 80);
     [self adjustX:ran];
     
     CGPoint animateToCenter = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);
     
     self.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(radius));
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.center = animateToCenter;
        self.transform = CGAffineTransformIdentity;

    } completion:^(BOOL finished) {
        completionBlock(self);
    }];
    
}

- (void) slideOutAfter:(float)delay duration:(float)duration completion:(void (^)(void))completionBlock
{
    
    UIView *superview = self.superview;
    
    float random = arc4random() % 20;
    float radius = random - 10;
    
    int ran = RANDOM_INT(-80, 80);
    self.center = CGPointMake(self.bounds.size.width / 2, self.bounds.size.height / 2);

    
    CGPoint animateToCenter = CGPointMake(superview.bounds.size.width / 2 + ran, superview.bounds.size.height / 2 + superview.bounds.size.height + 50);
    
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
     
        self.center = animateToCenter;
        self.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(radius));
        
    } completion:^(BOOL finished) {
        self.transform = CGAffineTransformIdentity;
        completionBlock();
    }];
    
}






@end
