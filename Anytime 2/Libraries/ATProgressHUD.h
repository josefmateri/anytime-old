//
//  ATProgressHUD.h
//  anytime
//
//  Created by Josef Materi on 05.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "MBProgressHUD.h"

typedef enum  {
    ATProgressHUDTypeDelete
} ATProgressHUDType;

@interface ATProgressHUD : MBProgressHUD
@property (nonatomic) ATProgressHUDType hudType;


+ (instancetype) showProgressHUDWithType:(ATProgressHUDType)hudType inParent:(UIView *)parentView;

- (void)startSpinProgressBackgroundLayer;
- (void)stopSpinProgressBackgroundLayer;

@end
