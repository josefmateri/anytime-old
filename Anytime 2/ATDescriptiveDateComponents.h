//
//  ATDescriptiveDateComponents.h
//  Anytime 2
//
//  Created by Josef Materi on 13.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ATDescriptiveDateComponents : NSObject

@property (nonatomic, strong) NSString *paddedDay;
@property (nonatomic, strong) NSString *paddedDayWithDots;
@property (nonatomic, strong) NSString *month;
@property (nonatomic, strong) NSString *shortMonth;
@property (nonatomic, strong) NSString *weekDay;
@property (nonatomic, strong) NSString *shortWeedDay;
@property (nonatomic, strong) NSString *year;

+ (instancetype)descriptiveDateComponentsFromDateComponents:(NSDateComponents *)components;
+ (NSInteger)reginalWeekdayFromWeekday:(NSInteger)weekday;
+ (NSInteger)weekdayFromReginalWeekday:(NSInteger)weekDay;
@end
