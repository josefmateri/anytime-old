#import "MiteService.h"


@interface MiteService ()

// Private interface goes here.

@end


@implementation MiteService

// Custom logic goes here.

- (NSString *)title {
    return self.name;
}

- (NSNumber *)categoryIdentifier {
    return self.remote_id;
}

@end
