//
//  main.m
//  Anytime 2
//
//  Created by Josef Materi on 19.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ATAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([ATAppDelegate class]));
    }
}
