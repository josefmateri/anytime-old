//
//  ATCalendarViewController.h
//  Anytime 2
//
//  Created by Josef Materi on 22.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DMLazyScrollView.h"

@protocol ATCalendarViewDelegate;
@protocol ATCalendarViewDatasource;

@interface ATCalendarViewController : UIView

@property (nonatomic, strong) DMLazyScrollView *weekScrollView;

@property (nonatomic, weak) id <ATCalendarViewDelegate> delegate;
@property (nonatomic, weak) id <ATCalendarViewDatasource> datasource;

@property (nonatomic, strong) NSDate *currentDate;

- (id)initWithFrame:(CGRect)frame date:(NSDate *)date;

- (void)reloadData;

- (NSInteger)currentSelectedIndex;

- (NSDateComponents *)goToNextDay;
- (NSDateComponents *)goToPrevDay;

- (void) updateWithDateComponents:(NSDateComponents *)component;
- (void) setCurrentDayHighlighted:(BOOL)highlighted;

@end

@protocol ATCalendarViewDelegate <NSObject>


- (void)calenderViewController:(ATCalendarViewController *)calendarViewController willChangeToNextWeek:(NSDate *)date;
- (void)calenderViewController:(ATCalendarViewController *)calendarViewController willChangeToPrevWeek:(NSDate *)date;
- (void)calenderViewController:(ATCalendarViewController *)calendarViewController didSelectDateWithDateComponents:(NSDateComponents *)dateComponents;

@optional

- (void)calenderViewController:(ATCalendarViewController *)calendarViewController didChangeToWeek:(NSDate *)date;

@end

@protocol ATCalendarViewDatasource <NSObject>
@end