//
//  ATCalendarViewController.m
//  Anytime 2
//
//  Created by Josef Materi on 22.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATCalendarViewController.h"
#import "ATCalendarDayCell.h"
#import "ATCalendarWeekHeaderView.h"
#import "ATWeekCollectionViewController.h"
#import "NSDate+Additions.h"
#import <Masonry.h>

@interface ATCalendarViewController () <DMLazyScrollViewDelegate, ATWeekCollectionViewControllerDelegate>
@property (nonatomic, strong) UICollectionView *monthPickerCollectionView;
@property (nonatomic, strong) ATWeekCollectionViewController *collectionViewController;
@property (nonatomic, strong) UILabel *fullDateLabel;
@property (nonatomic, strong) ATCalendarWeekHeaderView *weekHeaderView;
@property (nonatomic, strong) NSMutableArray *weekCollectionViews;
@property (nonatomic) NSInteger currentPageIndex;
@end

@implementation ATCalendarViewController


- (id)initWithFrame:(CGRect)frame date:(NSDate *)date {
    self = [super initWithFrame:frame];
    if (self) {
        
        self.currentDate = date;
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(40, 36);
        layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5);
        layout.minimumInteritemSpacing = 5.0f;
        layout.minimumLineSpacing = 5.0f;
        layout.scrollDirection =UICollectionViewScrollDirectionHorizontal;
//        layout.headerReferenceSize = (CGSize){ 320, 20};
        
        self.weekCollectionViews = [NSMutableArray array];
        
        for (int i = 0; i < 3; i++) {
            ATWeekCollectionViewController *collectionView = [[ATWeekCollectionViewController alloc] initWithCollectionViewLayout:layout];
            collectionView.delegate = self;
            collectionView.firstDayOfTheWeek = [date firstDayOfTheWeek];
            [self.weekCollectionViews addObject:collectionView];
        }
        
        self.weekScrollView = [[DMLazyScrollView alloc] initWithFrameAndDirection:CGRectMake(0, 0, 320, 36) direction:DMLazyScrollViewDirectionHorizontal circularScroll:YES];
        
        self.weekScrollView.controlDelegate = self;
        
        __weak __typeof(&*self)weakSelf = self;
        _weekScrollView.dataSource = ^(NSUInteger index) {
            return [weakSelf weekCollectionViewAtIndex:index];
        };
        
        _weekScrollView.numberOfPages = 3;


        [self addSubview:_weekScrollView];

        self.weekScrollView.translatesAutoresizingMaskIntoConstraints = NO;
        
        self.backgroundColor = [UIColor clearColor];
        
        self.weekScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
    
        self.monthPickerCollectionView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self.weekScrollView makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(self.width);
            make.height.equalTo(@(40));
            make.top.equalTo(self.top);
            make.centerX.equalTo(self.centerX);
        }];


        //        [self.monthPickerCollectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:YES scrollPosition:0];
        
        // Do any additional setup after loading the view.

    }
    return self;
    
}

- (void)reloadData {
    [self.weekScrollView reloadData];
}

- (void)updateWithDateComponents:(NSDateComponents *)component {
    
    ATWeekCollectionViewController *current = (ATWeekCollectionViewController *)[self.weekScrollView currentViewController];
    [current updateWithDateComponents:component];

}

- (void)setCurrentDayHighlighted:(BOOL)highlighted {

    ATWeekCollectionViewController *current = (ATWeekCollectionViewController *)[self.weekScrollView currentViewController];
    current.firstDayOfTheWeek = [self.currentDate firstDayOfTheWeek];

    if (highlighted) {
       [current selectDay:[self.currentDate day] animated:NO];
   } else {
       [current deselectDay:[self.currentDate day] animated:NO];
   }

}

- (ATWeekCollectionViewController *)weekCollectionViewAtIndex:(NSInteger)index {
    return _weekCollectionViews[index];
}


- (NSDateComponents *)goToNextDay {
   
    ATWeekCollectionViewController *currentController = (ATWeekCollectionViewController *)[self.weekScrollView currentViewController];
    
    NSIndexPath *currentIndexPath = [[currentController.collectionView indexPathsForSelectedItems] objectAtIndex:0];
    
    NSInteger newIndex = currentIndexPath.row + 1;
    
    if (newIndex > 6) {
        
        newIndex = 0;
    
        [currentController.collectionView deselectItemAtIndexPath:currentIndexPath animated:YES];
        
        NSInteger currentPage = [self.weekScrollView pageIndexByAdding:1 from:self.weekScrollView.currentPage];

        currentController = [self weekCollectionViewAtIndex:currentPage];
        
        
        [_weekScrollView goToNextPage];
        
//        [_weekScrollView moveByPages:1 animated:YES];
    
    }
        
        NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:newIndex inSection:currentIndexPath.section];
       [currentController selectIndexPath:nextIndexPath];
    
    
//    [self weekCollectionViewController:currentController didSelectDateWithDateComponents:[currentController dateComponentsForSelectedIndexPath]];

    return [currentController dateComponentsForSelectedIndexPath];
}

- (NSDateComponents *)goToPrevDay {
    
    ATWeekCollectionViewController *currentController = (ATWeekCollectionViewController *)[self.weekScrollView currentViewController];
    
    NSIndexPath *currentIndexPath = [[currentController.collectionView indexPathsForSelectedItems] firstObject];
    
    NSInteger newIndex = currentIndexPath.row - 1;
    
    if (newIndex < 0) {
        
        newIndex = 6;
        
        [currentController.collectionView deselectItemAtIndexPath:currentIndexPath animated:YES];

        NSInteger currentPage = [self.weekScrollView pageIndexByAdding:-1 from:self.weekScrollView.currentPage];

        currentController = [self weekCollectionViewAtIndex:currentPage];
        
        [_weekScrollView goToPreviousPage];
        
//        [_weekScrollView moveByPages:-1 animated:YES];

    }
    
    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:newIndex inSection:currentIndexPath.section];
    
    [currentController selectIndexPath:nextIndexPath];

    
//    [self weekCollectionViewController:currentController didSelectDateWithDateComponents:[currentController dateComponentsForSelectedIndexPath]];
    return [currentController dateComponentsForSelectedIndexPath];
}

- (NSInteger)currentSelectedIndex {
    UICollectionViewController *currentCollectionView = (UICollectionViewController *)[self.weekScrollView currentViewController];
    return [[[currentCollectionView.collectionView indexPathsForSelectedItems] firstObject] row];
}

#pragma mark - DMLazyScrollViewDelegate

- (void)lazyScrollViewWillEndDragging:(DMLazyScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
   
    ATWeekCollectionViewController *currentController = (ATWeekCollectionViewController *)[self.weekScrollView currentViewController];
    
    
    NSIndexPath *currentIndexPath = [[currentController.collectionView indexPathsForSelectedItems] firstObject];
  
    CGPoint off = *targetContentOffset;
    
    CGPoint currentOff = scrollView.currentPageOffset;
    
    float diff = fabs(off.x - currentOff.x);
    
    BOOL willBouncedBack = NO;
    
    if (diff != scrollView.bounds.size.width && diff < scrollView.bounds.size.width) willBouncedBack = YES;
    
    
    if (!willBouncedBack) {
        
        if (off.x > scrollView.contentOffset.x) {


            NSInteger currentPage = [scrollView pageIndexByAdding:1 from:scrollView.currentPage];


            ATWeekCollectionViewController *nextController = (ATWeekCollectionViewController *)[self weekCollectionViewAtIndex:currentPage];
//            [nextController.collectionView selectItemAtIndexPath:currentIndexPath animated:YES scrollPosition:0];
            
            
            [nextController selectIndexPath:currentIndexPath];
            

            if ([self.delegate respondsToSelector:@selector(calenderViewController:willChangeToNextWeek:)]) {
                [self.delegate calenderViewController:self willChangeToNextWeek:[self.currentDate beginningOfNextWeek]];
            }

            
        } else {
            
            NSInteger currentPage = [scrollView pageIndexByAdding:-1 from:scrollView.currentPage];
            
            ATWeekCollectionViewController *nextController = (ATWeekCollectionViewController *)[self weekCollectionViewAtIndex:currentPage];
            
            
            [nextController selectIndexPath:currentIndexPath];
                    
            
//            [nextController.collectionView selectItemAtIndexPath:currentIndexPath animated:YES scrollPosition:0];
            
            if ([self.delegate respondsToSelector:@selector(calenderViewController:willChangeToPrevWeek:)]) {
                [self.delegate calenderViewController:self willChangeToPrevWeek:[self.currentDate beginningOfPreviousWeek]];
            }
            
        }
        
    }
}


- (void)lazyScrollView:(DMLazyScrollView *)pagingView willChangeToNextPageWithViewController:(UIViewController *)viewController beforeViewController:
(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController {
    
    ATWeekCollectionViewController *thisController = (ATWeekCollectionViewController *)viewController;
    thisController.firstDayOfTheWeek = [self.currentDate beginningOfNextWeek];
    [thisController.collectionView reloadData];
    
}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView willChangeToPrevPageWithViewController:(UIViewController *)viewController beforeViewController:
(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController {
    
    ATWeekCollectionViewController *thisController = (ATWeekCollectionViewController *)viewController;
    thisController.firstDayOfTheWeek = [self.currentDate beginningOfPreviousWeek];
    [thisController.collectionView reloadData];
    
}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView didChangeToNextPageWithViewController:(UIViewController *)viewController beforeViewController:(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController {
    self.currentDate = [self.currentDate beginningOfNextWeek];
    
    ATWeekCollectionViewController *thisController = (ATWeekCollectionViewController *)viewController;
    NSDateComponents *dateComponents = [thisController dateComponentsForSelectedIndexPath];
    
    __strong id <ATCalendarViewDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(calenderViewController:didSelectDateWithDateComponents:)]) {
        [delegate calenderViewController:self didSelectDateWithDateComponents:dateComponents];
    }
    
}

- (void)lazyScrollView:(DMLazyScrollView *)pagingView didChangeToPrevPageWithViewController:(UIViewController *)viewController beforeViewController:(UIViewController *)beforeViewController afterViewController:(UIViewController *)afterViewController {
    self.currentDate = [self.currentDate beginningOfPreviousWeek];
    
    ATWeekCollectionViewController *thisController = (ATWeekCollectionViewController *)viewController;

    NSDateComponents *dateComponents = [thisController dateComponentsForSelectedIndexPath];
    
    __strong id <ATCalendarViewDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(calenderViewController:didSelectDateWithDateComponents:)]) {
        [delegate calenderViewController:self didSelectDateWithDateComponents:dateComponents];
    }
}

#pragma mark - ATWeekCollectionViewControllerDelegate

- (void)weekCollectionViewController:(ATWeekCollectionViewController *)controller didSelectDateWithDateComponents:(NSDateComponents *)dateComponents {
    
    __strong id <ATCalendarViewDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(calenderViewController:didSelectDateWithDateComponents:)]) {
        [delegate calenderViewController:self didSelectDateWithDateComponents:dateComponents];
    }
    
}
@end
