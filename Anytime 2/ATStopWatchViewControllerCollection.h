//
//  ATStopWatchViewControllerCollection.h
//  anytime
//
//  Created by Josef Materi on 22.09.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATStopWatchViewController.h"
#import "ATDatasourceProtocol.h"

@protocol ATStopWatchViewControllerCollectionDelegate;

@interface ATStopWatchViewControllerCollection : NSObject

@property (nonatomic, strong) UIView *parentContainerView;
@property (nonatomic, weak) id <ATDatasourceProtocol> remoteDatasource;
@property (nonatomic, weak) id <ATStopWatchViewControllerCollectionDelegate> delegate;

+ (instancetype)sharedCollection;

- (void)hideStopwatchesAnimated:(BOOL)animated completion:(void (^)(BOOL finished))completion;
- (void)showStopwatchesAnimated:(BOOL)animated completion:(void (^)(BOOL finished))completion;

- (void)showStopWatchViewControllerWithTimeEntry:(id <ATTimeEntryProtocol>)timeEntry configure:(void (^)(ATStopWatchViewController *controller))configurationBlock completion:(void (^)())completionBlock;

- (void)showInitialStopWatchViewController:(void (^)(ATStopWatchViewController *controller))configurationBlock completion:(void (^)())completionBlock;

@end

@protocol ATStopWatchViewControllerCollectionDelegate <NSObject>
- (void)stopWatchViewControllerCollectionWillShowMenu:(ATStopWatchViewControllerCollection *)collection;
@end