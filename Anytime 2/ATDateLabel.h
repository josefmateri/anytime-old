//
//  ATDateLabel.h
//  anytime
//
//  Created by Josef Materi on 02.09.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ATDateLabel : UIView

@property (nonatomic) BOOL imageViewHidden;

@property (nonatomic, strong) UIImageView *imageView;

- (void) updateWithDateComponents:(NSDateComponents *)component;


@end
