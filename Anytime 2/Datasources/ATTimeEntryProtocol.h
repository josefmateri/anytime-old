//
//  ATTimeEntryProtocol.h
//  anytime
//
//  Created by Josef Materi on 14.09.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATCategoryProtocol.h"

@protocol ATTimeEntryProtocol <NSObject>


- (NSDate *)date;
- (void)setDate_at:(NSDate *)date;

- (NSNumber *)timeEntryIdentifier;

- (NSNumber *)durationInMinutes;
- (void)setDurationInMinutes:(NSNumber *)minutes;


- (NSNumber *)trackingDurationInMinutes;
- (void)setTrackingDurationInMinutes:(NSNumber *)tracking;

- (NSDate *)trackingSince;
- (void)setTrackingSince:(NSDate *)date;

- (void)stopTimer;
- (void)startTimer;

- (BOOL)isInserted;


- (void)delete;


#pragma mark - Category

- (BOOL)hasSecondaryCategory;

- (id <ATCategoryProtocol>)mainCategory;
- (id <ATCategoryProtocol>)secondaryCategory;
- (void)setMainCategory:(id <ATCategoryProtocol>)mainCategory;
- (void)setSecondaryCategory:(id <ATCategoryProtocol>)secondaryCategory;

- (NSString *)mainCategoryTitle;
- (NSString *)secondaryCategoryTitle;

- (NSString *)missingMainCategoryTitle;
- (NSString *)missingSecondaryCategoryTitle;

@end
