//
//  JMKeyboardViewController.h
//  Anytime
//
//  Created by Josef Materi on 01.10.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMKeyboardViewCell.h"

@protocol JMKeyboardViewDatasource;
@protocol JMKeyboardViewDelegate;

@interface JMKeyboardViewController : UICollectionViewController <UICollectionViewDelegateFlowLayout>
@property (nonatomic, weak) id <JMKeyboardViewDatasource> datasource;

@property (nonatomic, weak) id <JMKeyboardViewDelegate> delegate;

@end

@protocol JMKeyboardViewDatasource <NSObject>

- (void)keyboardViewController:(JMKeyboardViewController *) kvc configureItem:(JMKeyboardViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;

@end

@protocol JMKeyboardViewDelegate <NSObject>

- (void)keyboardViewController:(JMKeyboardViewController *) kvc didSelectItemAtIndexPath:(NSIndexPath *)indexPath;

@end