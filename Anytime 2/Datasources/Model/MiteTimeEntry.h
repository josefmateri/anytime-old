#import "_MiteTimeEntry.h"
#import "ATStopWatch.h"
#import "ATTimeEntryProtocol.h"

@interface MiteTimeEntry : _MiteTimeEntry <ATTimeEntryProtocol> {}
// Custom logic goes here.
@property (nonatomic, strong) ATStopWatch *stopWatch;

@end
