//
//  ATDateView.m
//  Anytime 2
//
//  Created by Josef Materi on 10.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATDateView.h"
#import "UIView+FLKAutoLayout.h"
#import "ATDescriptiveDateComponents.h"

@implementation ATDateView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
        self.textColor = [UIColor colorWithWhite:0.1 alpha:1];
        
        self.weekDayLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.dayLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.yearLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.monthLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.secondMonthLabel = [[UILabel alloc] initWithFrame:CGRectZero];
        self.secondYearLabel = [[UILabel alloc] initWithFrame:CGRectZero];

        self.weekDayLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:14];
        self.dayLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
        self.monthLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
        self.yearLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:24];
        self.secondMonthLabel.font = [UIFont fontWithName:@"Helvetica" size:24];
        self.secondYearLabel.font = [UIFont fontWithName:@"Helvetica-Light" size:24];

        
        self.dayLabel.textColor = self.textColor;
        self.yearLabel.textColor = self.textColor;
        self.weekDayLabel.textColor = self.textColor;
        self.monthLabel.textColor = [self.textColor colorWithAlphaComponent:0.8];
        self.secondMonthLabel.textColor = self.tintColor;
        self.secondYearLabel.textColor = [self.textColor colorWithAlphaComponent:0.8];

        self.weekDayLabel.alpha = 0.5;
        
        self.weekDayLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.dayLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.yearLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.monthLabel.translatesAutoresizingMaskIntoConstraints = NO;
        self.secondMonthLabel.translatesAutoresizingMaskIntoConstraints = NO;

        [self.weekDayLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self.weekDayLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        
        [self.dayLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];

        [self.dayLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self.dayLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        
        [self.monthLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self.monthLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self.monthLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];

        
        [self.yearLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self.yearLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];

        
        [self.secondMonthLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self.secondMonthLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self.secondMonthLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
    
        
        [self.secondYearLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self.secondYearLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
        [self.secondYearLabel setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
            
//        self.secondMonthLabel.text = @"Juli";
//        self.secondYearLabel.text = @"2013";

        [self addSubview:self.secondMonthLabel];
        
        [self addSubview:self.secondYearLabel];

        
        [self addSubview:_yearLabel];
        [self addSubview:_weekDayLabel];
        [self addSubview:_dayLabel];
        [self addSubview:_monthLabel];
        // Initialization code
              
    }
    return self;
}

- (void)setContentInsets:(UIEdgeInsets)contentInsets {
    _contentInsets = contentInsets;
 
    [self updateLayout];
}

- (void)updateLayout {
    
    [self removeConstraints:self.constraints];

   [self addConstraints:[self.secondYearLabel alignAttribute:NSLayoutAttributeRight toAttribute:NSLayoutAttributeRight ofView:self predicate:[@(-self.contentInsets.right) description]]];
   
   [self addConstraints:[self.secondMonthLabel alignAttribute:NSLayoutAttributeRight toAttribute:NSLayoutAttributeLeft ofView:self.secondYearLabel predicate:@"-5"]];
    
    NSString *bottomOffset = [NSString stringWithFormat:@"-%f",self.contentInsets.bottom];
    NSLog(@"%@", bottomOffset);

    [self addConstraints:[self.secondMonthLabel alignAttribute:NSLayoutAttributeBottom toAttribute:NSLayoutAttributeBottom ofView:self predicate:bottomOffset]];
   
   [self addConstraints:[self.secondYearLabel alignAttribute:NSLayoutAttributeBottom toAttribute:NSLayoutAttributeBottom ofView:self predicate:bottomOffset]];
    
    NSArray *horizontalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"[_dayLabel]-(0)-[_monthLabel]-(5)-[_yearLabel]|" options:NSLayoutFormatAlignAllBaseline metrics:Nil views:NSDictionaryOfVariableBindings(_dayLabel, _yearLabel, _monthLabel)];
    
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:self.weekDayLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:self.contentInsets.top];
    [self addConstraint:topConstraint];
    
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:self.weekDayLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeft multiplier:1 constant:self.contentInsets.left];
    
    [self addConstraint:left];
    
    NSArray *verticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_weekDayLabel]-(0)-[_dayLabel]" options:NSLayoutFormatAlignAllLeft metrics:nil views:NSDictionaryOfVariableBindings(_weekDayLabel,_dayLabel)];
    
    [self addConstraints:horizontalConstraints];
    [self addConstraints:verticalConstraints];
    
    [self layoutIfNeeded];

}
- (void)setTextColor:(UIColor *)textColor {
    _textColor = textColor;
    
    [[self subviews] enumerateObjectsUsingBlock:^(UILabel *obj, NSUInteger idx, BOOL *stop) {
        if ([obj respondsToSelector:@selector(setTextColor:)]) {
            obj.textColor = textColor;
        }
    }];
}

- (CGSize)intrinsicContentSize {
    return CGSizeMake(300, 45);
}

- (void)setAlpha:(CGFloat)alpha {
  
    self.weekDayLabel.alpha = alpha;
    self.dayLabel.alpha = alpha;
    self.monthLabel.alpha = alpha;
    self.yearLabel.alpha = alpha;
    
    
}

- (void) setSecondMonthLabelAlpha:(CGFloat)alpha {
    self.secondMonthLabel.alpha = alpha;
    self.secondYearLabel.alpha = alpha;
    self.secondMonthLabel.hidden = NO;
    self.secondYearLabel.hidden = NO;
}

- (void) updateWithDateComponents:(NSDateComponents *)component {

    if (!component) return;
    
    ATDescriptiveDateComponents *descriptiveDateComponents = [ATDescriptiveDateComponents descriptiveDateComponentsFromDateComponents:component];
    
    self.dayLabel.text = descriptiveDateComponents.paddedDayWithDots;
    self.monthLabel.text = descriptiveDateComponents.month;
    self.weekDayLabel.text = descriptiveDateComponents.weekDay;
    
    self.secondMonthLabel.text = descriptiveDateComponents.month;
    self.secondYearLabel.text = descriptiveDateComponents.year;
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
