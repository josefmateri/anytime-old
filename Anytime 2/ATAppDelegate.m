//
//  ATAppDelegate.m
//  Anytime 2
//
//  Created by Josef Materi on 19.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATAppDelegate.h"
#import "ATCalendarWithTimeEntriesViewController.h"
#import <CoreData/CoreData.h>
#import "ATStopWatchViewController.h"
#import "ATStopWatchViewControllerCollection.h"
#import "ATMiteDatasource.h"
#import "ATMiteMenuViewController.h"
#import <Masonry.h>
#import "ATMainCategoryViewController.h"
#import "ATMiteServiceConfiguration.h"

@interface ATAppDelegate() <ATStopWatchViewControllerDelegate, ATStopWatchViewControllerCollectionDelegate, ATMenuViewControllerDelegate>

@property (nonatomic, strong) ATCalendarWithTimeEntriesViewController *rootViewController;
@property (nonatomic, strong) ATStopWatchViewController *stopWatchViewController;
@property (nonatomic, strong) ATMainCategoryViewController *mainCategoryViewController;
@end

@implementation ATAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    CGRect mainFrame = [[UIScreen mainScreen] bounds];
    
    self.window = [[UIWindow alloc] initWithFrame:mainFrame];
    
    ATMiteServiceConfiguration *serviceConfiguration = [ATServiceConfiguration serviceConfigurationForIdentifier:ATMiteServiceIdentifier];
    ATMiteMenuViewController *test = (ATMiteMenuViewController *)[serviceConfiguration menuViewController];
    test.delegate = self;
    test.remoteDatasource = [serviceConfiguration remoteDatasource];
    
    self.navigationController = [[UINavigationController alloc] initWithRootViewController:test.calendarViewController];
    [self.navigationController setNavigationBarHidden:YES];
    
    
    UINavigationController *navc = [[UINavigationController alloc]  initWithRootViewController:test];
    
    REFrostedViewController *frosted = [[REFrostedViewController alloc] initWithContentViewController:self.navigationController menuViewController:navc];
    self.mainViewController = frosted;
    frosted.blurTintColor = [UIColor colorWithRed:0.026 green:0.129 blue:0.257 alpha:0.8];
    frosted.minimumMenuViewSize = CGSizeMake(mainFrame.size.width, mainFrame.size.height);
    frosted.direction = REFrostedViewControllerDirectionLeft;
    frosted.animationDuration = 0.15;
    frosted.liveBlurBackgroundStyle = REFrostedViewControllerLiveBackgroundStyleDark;
    self.window.rootViewController = frosted;
    
    
    [self.window makeKeyAndVisible];
    
    self.window.tintColor = [UIColor colorWithRed:0.409 green:0.606 blue:0.774 alpha:1.000];
    
    [[ATStopWatchViewControllerCollection sharedCollection] setRemoteDatasource:[serviceConfiguration remoteDatasource]];
    //
    [[ATStopWatchViewControllerCollection sharedCollection] setDelegate:self];
    
    [[ATStopWatchViewControllerCollection sharedCollection] setParentContainerView:self.mainViewController.view];
    
    [[ATStopWatchViewControllerCollection sharedCollection] showInitialStopWatchViewController:^(ATStopWatchViewController *controller) {
        
        [controller resetPosition];
        controller.delegate = self;
        
    } completion:^{
        
        
    }];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

}



- (void)loadStopWatchViewController {

    self.stopWatchViewController = [[ATStopWatchViewController alloc] initWithNibName:nil bundle:nil];
    
    [self.window addSubview:self.stopWatchViewController.view];
    
    
    //    self.toolbarTopConstraint = [NSLayoutConstraint constraintWithItem:self.stopWatchViewController.view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeTop multiplier:1 constant:self.view.bounds.size.height - self.initialToolbarBottomOffset];
    //
    //    [self.view addConstraint:self.toolbarTopConstraint];
    
    [self.stopWatchViewController.view makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.window);
        make.height.equalTo(self.window).offset(50);
    }];
    
    [self.window layoutIfNeeded];
    
    [self.stopWatchViewController resetPosition];

    self.stopWatchViewController.delegate = self;

}

#pragma mark - ATStopWatchViewControllerDelegate

- (void)stopWatchViewControllerDidAppear:(ATStopWatchViewController *)stopWatchViewController {
    
}

- (void)stopWatchViewControllerWillAppear:(ATStopWatchViewController *)stopWatchViewController {
    
}

- (void)stopWatchViewControllerWillDisappear:(ATStopWatchViewController *)stopWatchViewController {
    
}

- (void)stopWatchViewControllerDidDisappear:(ATStopWatchViewController *)stopWatchViewController {
    
}

#pragma mark - ATStopWatchViewControllerCollectionDelegate


- (void)stopWatchViewControllerCollectionWillShowMenu:(ATStopWatchViewControllerCollection *)collection {
    [self.mainViewController presentMenuViewController];
}


#pragma mark - ATMenuViewControllerDelegate

- (void)menuViewController:(ATMiteMenuViewController *)menuViewController didSelectViewController:(UIViewController *)viewController {
   
    [self.navigationController setViewControllers:@[viewController]];

    [self.mainViewController hideMenuViewController];
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


@end
