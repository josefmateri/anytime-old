//
//  NSDate+Additions.h
//  Anytime 2
//
//  Created by Josef Materi on 25.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Additions)


// Date presentation
- (NSString *)longDescription;
- (NSString *)currentWeekDescription;

// Date calculations
- (NSDate *)dateByAddingDays:(NSInteger)days;


// Date Components
- (NSInteger)day;
- (NSInteger)weekOfMonth;

// Special Days
- (NSDate *)firstDayOfTheWeek;
- (NSDate *)firstDayOfTheMonth;
- (NSDate *)beginningOfNextWeek;
- (NSDate *)beginningOfPreviousWeek;

- (NSDateComponents *)componentsByAddingDays:(NSInteger)days;

@end
