//
//  ATTimeEntriesDatasourceProtocol.h
//  anytime
//
//  Created by Josef Materi on 19.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ATTimeEntriesDatasourceProtocol <NSObject>

- (NSFetchRequest *)fetchRequestForTimeEntriesWithDateComponents:(NSDateComponents *)dateComponents;
- (NSFetchRequest *)fetchRequestForTimeEntriesWithStartDate:(NSDate *)startDate endDate:(NSDate *)endDate;
- (void)getTimeEntriesWithDateComponents:(NSDateComponents *)dateComponents complete:(void (^)(NSArray *results))completeBlock failed:(void (^)(NSError *failed))failedBlock;

@end
