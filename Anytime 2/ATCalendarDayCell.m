//
//  ATCalendarDayCell.m
//  Anytime 2
//
//  Created by Josef Materi on 22.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATCalendarDayCell.h"

@implementation ATCalendarDayCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        _dayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,10,10)];
        
        _dayLabel.translatesAutoresizingMaskIntoConstraints = NO;
        
        _selectionRadius = 20;
        
        [self.contentView addSubview:_dayLabel];
        
        
        self.contentView.backgroundColor = [UIColor clearColor];
        
        _dayLabel.textAlignment = NSTextAlignmentCenter;

        NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:_dayLabel attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeWidth multiplier:1 constant:0];
        
        
        NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:_dayLabel attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeHeight multiplier:1 constant:0];
        
        
        NSLayoutConstraint *centerConstraint = [NSLayoutConstraint constraintWithItem:_dayLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
        
        NSLayoutConstraint *centerYConstraint = [NSLayoutConstraint constraintWithItem:_dayLabel attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
        
        [self addConstraint:heightConstraint];
        [self addConstraint:widthConstraint];
        [self addConstraint:centerConstraint];
        [self addConstraint:centerYConstraint];

        [self layoutIfNeeded];

        self.selectedBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        self.selectedBackgroundView.backgroundColor = [UIColor whiteColor];
        
        self.backgroundView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
//        self.backgroundView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.1];
        
        self.selectedBackgroundView.frame = CGRectMake(0, 0, _selectionRadius * 2, _selectionRadius * 2);
        
        self.selectedBackgroundView.layer.cornerRadius = _selectionRadius;
        
        self.selectedBackgroundView.center = CGPointMake(self.bounds.size.width / 2.0f, self.bounds.size.height / 2.0f);
        
        self.backgroundView.layer.cornerRadius = _selectionRadius;
        
        self.dayLabel.textColor = [UIColor whiteColor];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)updateTextLabelColor {
    
    if (self.selected || self.highlighted) {
        self.dayLabel.textColor = [UIColor colorWithRed:0.510 green:0.580 blue:0.663 alpha:1.000];
    } else {
        if (self.dimmed) {
            self.dayLabel.textColor = [UIColor colorWithRed:0.749 green:0.771 blue:0.794 alpha:1.000];
        } else {
            self.dayLabel.textColor = [UIColor whiteColor];
        }
    }
    
}
- (void)setDimmed:(BOOL)dimmed {
    _dimmed = dimmed;
    
    [self updateTextLabelColor];
    
}

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    
}

- (void) setSelected:(BOOL)selected {
    [super setSelected:selected];
    
    [self updateTextLabelColor];

}


@end
