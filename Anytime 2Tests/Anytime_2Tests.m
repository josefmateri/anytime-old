//
//  Anytime_2Tests.m
//  Anytime 2Tests
//
//  Created by Josef Materi on 19.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface Anytime_2Tests : XCTestCase

@end

@implementation Anytime_2Tests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
