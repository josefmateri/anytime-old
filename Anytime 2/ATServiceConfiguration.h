//
//  ATServiceConfiguration.h
//  anytime
//
//  Created by Josef Materi on 13.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATCategorySelectionViewController.h"

static NSString *ATMiteServiceIdentifier = @"ATMiteServiceIdentifier";

@protocol ATServiceConfigurationProtocol <NSObject>

- (id <ATDatasourceProtocol>)remoteDatasource;

- (UIViewController *)menuViewController;

@end

@interface ATServiceConfiguration : NSObject


+ (id <ATServiceConfigurationProtocol>)serviceConfigurationForIdentifier:(NSString *)identifier;


@end

