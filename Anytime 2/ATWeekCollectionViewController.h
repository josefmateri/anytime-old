//
//  ATWeekCollectionViewController.h
//  Anytime 2
//
//  Created by Josef Materi on 23.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ATWeekCollectionViewControllerDelegate;

@interface ATWeekCollectionViewController : UICollectionViewController

@property (nonatomic, strong) NSDate *firstDayOfTheWeek;
@property (nonatomic, weak) id <ATWeekCollectionViewControllerDelegate> delegate;
@property (nonatomic, strong) NSDateComponents *dateComponents;

- (void)selectDay:(NSInteger)day animated:(BOOL)animated;
- (void)deselectDay:(NSInteger)day animated:(BOOL)animated;

- (void)selectIndexPath:(NSIndexPath *)indexPath;

- (void)reloadData;

- (void) updateWithDateComponents:(NSDateComponents *)component;

- (NSDateComponents *)dateComponentsForSelectedIndexPath;

@end

@protocol ATWeekCollectionViewControllerDelegate <NSObject>

- (void)weekCollectionViewController:(ATWeekCollectionViewController *)controller didSelectDateWithDateComponents:(NSDateComponents *)dateComponents;

@end