//
//  ATProgressHUD.m
//  anytime
//
//  Created by Josef Materi on 05.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATProgressHUD.h"
#import "FFCircularProgressView.h"
#import "UIImage+Tint.h"
@interface ATProgressHUD()
@property (nonatomic, strong) FFCircularProgressView *circularProgressView;
@end
@implementation ATProgressHUD

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (instancetype)initWithView:(UIView *)view {
    self = [super initWithView:view];
    if (self) {
        self.tintColor = [UIColor colorWithRed:0.357 green:0.471 blue:0.588 alpha:1.000];
    }
    return self;
}

+ (instancetype) showProgressHUDWithType:(ATProgressHUDType)hudType inParent:(UIView *)parentView {
    
    
    ATProgressHUD *progressHud = [ATProgressHUD showHUDAddedTo:parentView animated:YES];
    [progressHud setMode:MBProgressHUDModeCustomView];
    
    [progressHud setLabelColor:progressHud.tintColor];
    [progressHud setLabelText:[progressHud labelTextForHUDType:hudType]];
    
    FFCircularProgressView *circular = [[FFCircularProgressView alloc] initWithFrame:CGRectMake(0, 0, 37, 37)];
    //    [circular setProgress:0.5];
    [progressHud setCustomView:circular];
    
    UIImage *image = [progressHud imageForHUDType:hudType];
    
    [circular setTintColor:progressHud.tintColor];
    
    [circular setIcon:image];
    [circular startSpinProgressBackgroundLayer];

    progressHud.circularProgressView = circular;
    
    return progressHud;
}

- (void)startSpinProgressBackgroundLayer {
    [self.circularProgressView startSpinProgressBackgroundLayer];
}

- (void)stopSpinProgressBackgroundLayer {
    [self.circularProgressView stopSpinProgressBackgroundLayer];
}

- (void)setProgress:(float)progress {
    [super setProgress:progress];
    [self.circularProgressView setProgress:progress];
}

- (UIImage *)imageForHUDType:(ATProgressHUDType)hudType {
    UIImage *image = nil;
    switch (hudType) {
        case ATProgressHUDTypeDelete:
            image = [UIImage imageNamed:@"trash"];
            break;
        default:
            image = [UIImage imageNamed:@"trash"];
            break;
    }
    return [image imageTintedWithColor:self.tintColor];
}

- (NSString *)labelTextForHUDType:(ATProgressHUDType)hudType {
    NSString *string = nil;
    switch (hudType) {
        case ATProgressHUDTypeDelete:
            string = NSLocalizedString(@"Deleting ...", nil);
            break;
        default:
            string = NSLocalizedString(@"Deleting ...", nil);
            break;
    }
    return string;
}



/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
