//
//  ATDateConfiguration.m
//  Anytime 2
//
//  Created by Josef Materi on 13.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATDateConfiguration.h"

@implementation ATDateConfiguration

+ (instancetype)currentConfiguration {
    
    static dispatch_once_t once;
    static ATDateConfiguration *singleton;
    dispatch_once(&once, ^ {
        singleton = [[ATDateConfiguration alloc] init];
    });
    return singleton;
    
}

- (instancetype) init {
    self = [super init];
    if (self) {
        
        NSString *calendarId = [[NSLocale currentLocale] objectForKey:NSLocaleCalendar];
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:calendarId];
        NSUInteger firstDay = [calendar firstWeekday];
        
        self.firstDayOfTheWeek = firstDay;
    }
    return self;
}


- (NSDateComponents *)dateComponentsFromDate:(NSDate *)date {
    
    NSCalendar *currentCalendar = [NSCalendar currentCalendar];
    
    return [currentCalendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear fromDate:date];
    
}

- (NSDate *)dateFromDateComponents:(NSDateComponents *)dateComponents {
    NSCalendar *currentCalender = [NSCalendar currentCalendar];
    return [currentCalender dateFromComponents:dateComponents];
}

@end
