// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MiteTimeEntry.m instead.

#import "_MiteTimeEntry.h"

const struct MiteTimeEntryAttributes MiteTimeEntryAttributes = {
	.created_at = @"created_at",
	.customer_id = @"customer_id",
	.customer_name = @"customer_name",
	.date_at = @"date_at",
	.minutes = @"minutes",
	.project_id = @"project_id",
	.project_name = @"project_name",
	.remote_id = @"remote_id",
	.service_id = @"service_id",
	.service_name = @"service_name",
	.trackingMinutes = @"trackingMinutes",
	.trackingSince = @"trackingSince",
	.updated_at = @"updated_at",
};

const struct MiteTimeEntryRelationships MiteTimeEntryRelationships = {
	.project = @"project",
	.service = @"service",
};

const struct MiteTimeEntryFetchedProperties MiteTimeEntryFetchedProperties = {
};

@implementation MiteTimeEntryID
@end

@implementation _MiteTimeEntry

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MiteTimeEntry" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MiteTimeEntry";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MiteTimeEntry" inManagedObjectContext:moc_];
}

- (MiteTimeEntryID*)objectID {
	return (MiteTimeEntryID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"customer_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"customer_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"minutesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"minutes"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"project_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"project_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"remote_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"remote_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"service_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"service_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"trackingMinutesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"trackingMinutes"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic created_at;






@dynamic customer_id;



- (int64_t)customer_idValue {
	NSNumber *result = [self customer_id];
	return [result longLongValue];
}

- (void)setCustomer_idValue:(int64_t)value_ {
	[self setCustomer_id:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveCustomer_idValue {
	NSNumber *result = [self primitiveCustomer_id];
	return [result longLongValue];
}

- (void)setPrimitiveCustomer_idValue:(int64_t)value_ {
	[self setPrimitiveCustomer_id:[NSNumber numberWithLongLong:value_]];
}





@dynamic customer_name;






@dynamic date_at;






@dynamic minutes;



- (int64_t)minutesValue {
	NSNumber *result = [self minutes];
	return [result longLongValue];
}

- (void)setMinutesValue:(int64_t)value_ {
	[self setMinutes:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveMinutesValue {
	NSNumber *result = [self primitiveMinutes];
	return [result longLongValue];
}

- (void)setPrimitiveMinutesValue:(int64_t)value_ {
	[self setPrimitiveMinutes:[NSNumber numberWithLongLong:value_]];
}





@dynamic project_id;



- (int64_t)project_idValue {
	NSNumber *result = [self project_id];
	return [result longLongValue];
}

- (void)setProject_idValue:(int64_t)value_ {
	[self setProject_id:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveProject_idValue {
	NSNumber *result = [self primitiveProject_id];
	return [result longLongValue];
}

- (void)setPrimitiveProject_idValue:(int64_t)value_ {
	[self setPrimitiveProject_id:[NSNumber numberWithLongLong:value_]];
}





@dynamic project_name;






@dynamic remote_id;



- (int64_t)remote_idValue {
	NSNumber *result = [self remote_id];
	return [result longLongValue];
}

- (void)setRemote_idValue:(int64_t)value_ {
	[self setRemote_id:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveRemote_idValue {
	NSNumber *result = [self primitiveRemote_id];
	return [result longLongValue];
}

- (void)setPrimitiveRemote_idValue:(int64_t)value_ {
	[self setPrimitiveRemote_id:[NSNumber numberWithLongLong:value_]];
}





@dynamic service_id;



- (int64_t)service_idValue {
	NSNumber *result = [self service_id];
	return [result longLongValue];
}

- (void)setService_idValue:(int64_t)value_ {
	[self setService_id:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveService_idValue {
	NSNumber *result = [self primitiveService_id];
	return [result longLongValue];
}

- (void)setPrimitiveService_idValue:(int64_t)value_ {
	[self setPrimitiveService_id:[NSNumber numberWithLongLong:value_]];
}





@dynamic service_name;






@dynamic trackingMinutes;



- (int64_t)trackingMinutesValue {
	NSNumber *result = [self trackingMinutes];
	return [result longLongValue];
}

- (void)setTrackingMinutesValue:(int64_t)value_ {
	[self setTrackingMinutes:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveTrackingMinutesValue {
	NSNumber *result = [self primitiveTrackingMinutes];
	return [result longLongValue];
}

- (void)setPrimitiveTrackingMinutesValue:(int64_t)value_ {
	[self setPrimitiveTrackingMinutes:[NSNumber numberWithLongLong:value_]];
}





@dynamic trackingSince;






@dynamic updated_at;






@dynamic project;

	

@dynamic service;

	






@end
