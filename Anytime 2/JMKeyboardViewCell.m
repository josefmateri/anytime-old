//
//  JMKeyboardViewCell.m
//  Anytime
//
//  Created by Josef Materi on 02.10.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import "JMKeyboardViewCell.h"
#import "JMBackgroundView.h"

@implementation JMKeyboardViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
        self.backgroundView = [[UIView alloc] initWithFrame:CGRectInset(self.bounds, 10,10)];
        self.backgroundView.backgroundColor = [UIColor clearColor];
        
        _textLabel = [[UILabel alloc] initWithFrame:self.bounds];
        
        _textLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self.contentView addSubview:_textLabel];
        
        _textLabel.textColor = [UIColor whiteColor];
        _textLabel.font = [UIFont boldSystemFontOfSize:20];
        _textLabel.backgroundColor = [UIColor clearColor];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        
        
        _imageView = [[UIImageView alloc] initWithFrame:self.contentView.bounds];
        _imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _imageView.contentMode = UIViewContentModeCenter;
        
        self.backgroundView.layer.cornerRadius = self.bounds.size.height / 2.0;
        self.backgroundView.layer.borderWidth = 1;
        self.backgroundView.layer.borderColor = [[UIColor whiteColor] CGColor];;
        
        [self.contentView addSubview:_imageView];
        
        self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
        self.selectedBackgroundView.layer.cornerRadius = self.bounds.size.height / 2.0;
        self.selectedBackgroundView.layer.borderWidth = 1;
        self.selectedBackgroundView.layer.borderColor = [[UIColor whiteColor] CGColor];;
        self.selectedBackgroundView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
        
    }
    return self;
}

- (UIImageView *)customBackgroundImageView {
    return [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"shadow-gradient-light-bg"]
                                        resizableImageWithCapInsets:UIEdgeInsetsMake(0, 1, 0, 1) resizingMode:UIImageResizingModeStretch]];
}

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
