//
//  ATStopWatch.m
//  Anytime 2
//
//  Created by Josef Materi on 20.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATStopWatch.h"
#import "MPStopwatch.h"
#import "NSNumber+Time.h"
@interface ATStopWatch()

@property (nonatomic, strong) MPStopwatch *trackHistory;
@property (nonatomic, strong) NSThread *timerThread;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic) BOOL freeEntryMode;
@property (nonatomic) NSTimeInterval runningTime;
@property (nonatomic) NSTimeInterval startedWithTimeInterval;
@property (nonatomic, strong) NSDate *startTime;

@end


@implementation ATStopWatch


- (id)init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (void)startWithDate:(NSDate *)date andRunningTime:(NSTimeInterval)runningTime {
    
    NSLog(@"starting with date! %@",[@(runningTime / 60) durationValue]);
    
    self.startTime = date;
    self.runningTime = runningTime;
    self.startedWithTimeInterval = runningTime;
    
    [self activateTimerOnThread];
}

- (void)start {

    self.startTime = [NSDate date];
    self.startedWithTimeInterval = self.runningTime;
    
    [self activateTimerOnThread];
    
    NSLog(@"Total pause = %f", [self.trackHistory totalPause]);
    
}

- (void)resume {
    
    self.startedWithTimeInterval = self.runningTime;
    
    [self activateTimerOnThread];
}

- (void)stop {
    self.startedWithTimeInterval = self.runningTime;
    [self deactivateTimerOnThread];
}


- (void) count:(NSTimer *)timer {
    
//    NSTimeInterval totalTimeSinceFirstStart = [self.trackHistory totalTime];
    
//    NSInteger totalSeconds = round(totalTimeSinceFirstStart) / 1;
    
    NSTimeInterval timeInterval = fabs(self.startedWithTimeInterval + [[NSDate date] timeIntervalSinceDate:self.startTime]);
    
    NSTimeInterval seconds = timeInterval;
    
    self.runningTime = seconds;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        __strong id <ATStopWatchDelegate> delegate = self.delegate;
        if ([delegate respondsToSelector:@selector(stopWatch:didCountToRunningSeconds:totalSeconds:)]) {
            [delegate stopWatch:self didCountToRunningSeconds:seconds totalSeconds:seconds];
        }
        
    });
}

- (void)setRunningTime:(NSTimeInterval)runningTime {
    self.freeEntryMode = YES;
    _runningTime = runningTime;
}

- (void)setStartTime:(NSDate *)startTime {
    _startTime = startTime;
}

#pragma mark - NSTimer Thread

- (void)deactivateTimerOnThread {
    
    [self.timerThread cancel];
    [self.timer invalidate];
    
}

- (void)activateTimerOnThread {

    [self.timerThread cancel];
    self.timerThread = [[NSThread alloc] initWithTarget:self selector:@selector(startTimerThread)  object:nil];
    [self.timerThread start];

}

- (void) startTimerThread {
	NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
	self.timer = [NSTimer scheduledTimerWithTimeInterval: 1.0
                                                  target: self
                                                selector: @selector(count:)
                                                userInfo: nil
                                                 repeats: YES];
    
	[runLoop run];
}

-(void)invalidateTimer {
    [self.timer invalidate];
    [self.timerThread cancel];
    self.timerThread = nil;
}

- (void)dealloc {
    [self invalidateTimer];
}

@end
