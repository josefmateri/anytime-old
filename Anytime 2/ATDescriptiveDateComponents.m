//
//  ATDescriptiveDateComponents.m
//  Anytime 2
//
//  Created by Josef Materi on 13.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATDescriptiveDateComponents.h"
#import "NSDateFormatter+Helper.h"

@implementation ATDescriptiveDateComponents


+ (instancetype)descriptiveDateComponentsFromDateComponents:(NSDateComponents *)components {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSUInteger firstDay = [calendar firstWeekday];
    if (firstDay != 0) firstDay -= 1;
    
    ATDescriptiveDateComponents *descriptiveDateComponent = [[ATDescriptiveDateComponents alloc] init];

    if (components.day != NSNotFound) {
        NSString *day = [NSString stringWithFormat:@"%2i",components.day];
        NSString *dayWithDot = [NSString stringWithFormat:@"%2i.",components.day];
        descriptiveDateComponent.paddedDay = day;
        descriptiveDateComponent.paddedDayWithDots = dayWithDot;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
    dateFormatter.calendar = [NSCalendar currentCalendar];

    if (components.month != NSNotFound) {
        NSString *monthName = [[dateFormatter monthSymbols] objectAtIndex:components.month - 1];
        NSString *shortMonth = [[dateFormatter shortMonthSymbols] objectAtIndex:components.month - 1];

        descriptiveDateComponent.month = monthName;
        descriptiveDateComponent.shortMonth = shortMonth;
    }
   
    if (components.weekday != NSNotFound) {
        NSString *weekDay = [[dateFormatter reginalWeekdaySymbols] objectAtIndex:[self reginalWeekdayFromWeekday:components.weekday]];
        descriptiveDateComponent.weekDay = weekDay;
        
        NSString *shortWeekDay = [[dateFormatter shortWeekdaySymbols] objectAtIndex:[self reginalWeekdayFromWeekday:components.weekday]];
        descriptiveDateComponent.shortWeedDay = shortWeekDay;
    }

    
    if (components.year != NSNotFound) {
        descriptiveDateComponent.year = [@(components.year) stringValue];
    }

    
    return descriptiveDateComponent;
}

// 6 == 4
// 5 == 3
// 4 == 2
// 3 == 1
// 2 == 0
// 1 == 6
// 0 == 5

+ (NSInteger)reginalWeekdayFromWeekday:(NSInteger)weekday {
    
    NSInteger firstDay = [[NSCalendar currentCalendar] firstWeekday];
    
    NSRange range = [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay inUnit:NSCalendarUnitWeekOfMonth forDate:[NSDate date]];
    
    if (weekday < firstDay) {
        return weekday + range.length - firstDay;
    } else {
        return weekday - firstDay;
    }
    
}

+ (NSInteger)weekdayFromReginalWeekday:(NSInteger)weekDay {
    NSInteger firstDay = [[NSCalendar currentCalendar] firstWeekday];
 
    if (weekDay >= 7 - firstDay) {
        return weekDay - (7 - firstDay);
    } else {
        return weekDay + firstDay;
    }
}

@end
