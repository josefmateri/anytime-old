//
//  NSDateFormatter+Helper.h
//  Anytime 2
//
//  Created by Josef Materi on 13.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (Helper)

- (NSArray *)reginalWeekdaySymbols;
- (NSArray *)reginalShortWeekdaySymbols;

@end
