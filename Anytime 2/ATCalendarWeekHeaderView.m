//
//  ATCalendarWeekHeaderView.m
//  Anytime 2
//
//  Created by Josef Materi on 22.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATCalendarWeekHeaderView.h"
#import "ATCalendarDayCell.h"
#import "NSDateFormatter+Helper.h"

@interface ATCalendarWeekHeaderView ()
@property (nonatomic, strong) NSMutableArray *weekDayLabels;
@end

@implementation ATCalendarWeekHeaderView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        _weekDayLabels = [[NSMutableArray alloc] initWithCapacity:7];
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
        dateFormatter.calendar = [NSCalendar currentCalendar];
        NSArray *shortWeekDaySymbols = [dateFormatter reginalShortWeekdaySymbols];
        
        
        [shortWeekDaySymbols enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            
            float leftPadding = 2;
            
            float itemWidth = (self.bounds.size.width - leftPadding * 2.0f) / 7.0f;
           
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(leftPadding + itemWidth * idx, 0, itemWidth, self.bounds.size.height)];
            
            label.text = obj;
            
            label.backgroundColor = [UIColor clearColor];
            
            [self addSubview:label];
            
            label.textAlignment = NSTextAlignmentCenter;
            
            label.font = [UIFont systemFontOfSize:10];
            
            label.textColor = [UIColor whiteColor];
            
            [_weekDayLabels addObject:label];
        }];
        
        
    }
    return self;
}

- (void)layoutSubviews {
    
    [_weekDayLabels enumerateObjectsUsingBlock:^(UILabel *obj, NSUInteger idx, BOOL *stop) {
        
        float leftPadding = 2;

        float itemWidth = (self.bounds.size.width - leftPadding * 2.0f) / 7.0f;

        obj.frame = CGRectMake(leftPadding + itemWidth * idx, 0, itemWidth, self.bounds.size.height);
        
    }];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


@end
