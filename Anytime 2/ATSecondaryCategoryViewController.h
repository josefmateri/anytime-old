//
//  ATSecondaryCategoryViewController.h
//  anytime
//
//  Created by Josef Materi on 19.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATListViewController.h"
#import "ATCategoryDatasourceProtocol.h"

@interface ATSecondaryCategoryViewController : ATListViewController
@property (nonatomic, weak) id <ATCategoryDatasourceProtocol> categoryDatasource;
@end
