//
//  ATCalenderMonthViewController.m
//  Anytime 2
//
//  Created by Josef Materi on 08.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATCalenderMonthViewController.h"
#import "ATCalendarDayCell.h"
#import "ATDescriptiveDateComponents.h"
#import "NSDate+Additions.h"

@interface ATCalenderMonthViewController()
@property (nonatomic) NSUInteger firstDayInWeek;
@property (nonatomic) NSRange daysInCurrentMonth;
@property (nonatomic) NSRange daysInPreviousMonth;
@property (nonatomic) BOOL beginsWithSunday;
@property (nonatomic) NSRange daysInCurrentWeek;
@property (nonatomic) BOOL hideWeekForSelectedDay;
@end

@implementation ATCalenderMonthViewController


- (id)initWithCollectionViewLayout:(UICollectionViewLayout *)layout {
    self = [super initWithCollectionViewLayout:layout];
    if (self) {

        
    }
    return self;
}

- (void)setDateWithComponents:(NSDateComponents *)comp {
    
    if (self.calenderMonth.month == comp.month) {

        self.calenderMonth = comp;
        
        [self reloadData];
        
        NSIndexPath *indexPath = [self indexPathForDateComponents:comp];
        
        [self.collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
        
        return;
    }

    self.calenderMonth = comp;

    self.date = [[NSCalendar currentCalendar] dateFromComponents:self.calenderMonth];

    NSDate *curDate = self.date;
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    self.daysInCurrentMonth = [calendar rangeOfUnit:NSDayCalendarUnit
                                             inUnit:NSMonthCalendarUnit
                                            forDate:curDate];
    
    
    NSDateComponents* nextComps = [self.calenderMonth copy];
    // set last of month
    [nextComps setMonth:[nextComps month] - 1];
    
    NSDate *nextMonth = [calendar dateFromComponents:nextComps];
    
    NSDate *today = nextMonth;
    
    self.daysInPreviousMonth = [calendar rangeOfUnit:NSDayCalendarUnit
                                              inUnit:NSMonthCalendarUnit
                                             forDate:today];
    

    NSDate *firstDayOfTheMonth = [self.date firstDayOfTheMonth];
    
    NSDateComponents *components = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear | NSCalendarUnitWeekday  fromDate:firstDayOfTheMonth];
    
    self.firstDayInWeek = [ATDescriptiveDateComponents reginalWeekdayFromWeekday:components.weekday];
    
    [self reloadData];
//
    NSIndexPath *indexPath = [self indexPathForDateComponents:comp];
    
    [self.collectionView selectItemAtIndexPath:indexPath animated:YES scrollPosition:UICollectionViewScrollPositionNone];
    
}



- (void)viewWillAppear:(BOOL)animated {

}

- (void)viewDidAppear:(BOOL)animated {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.collectionView registerClass:[ATCalendarDayCell class] forCellWithReuseIdentifier:@"DayCell"];
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    self.collectionView.dataSource = self;
    //
    self.collectionView.delegate = self;
    
    [self.view layoutIfNeeded];
    
    self.view.clipsToBounds = YES;
    
//    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.collectionView.contentInset = UIEdgeInsetsZero;
    
    self.automaticallyAdjustsScrollViewInsets = NO;
}

- (void)reloadData {
    [self.collectionView reloadData];
}

- (void)deselectAllHighlightedDays {
    __weak ATCalenderMonthViewController  *weakSelf = self;
    
    [self.collectionView.indexPathsForSelectedItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        __strong ATCalenderMonthViewController *strongSelf = weakSelf;
    
        [strongSelf.collectionView deselectItemAtIndexPath:obj animated:NO];
        
    }];
}

- (void)highlightCellForDateComponent:(NSDateComponents *)dateComponents {
    [self.collectionView selectItemAtIndexPath:[self indexPathForDateComponents:dateComponents] animated:YES scrollPosition:UICollectionViewScrollPositionNone];
}

- (NSInteger)lineOfCurentSelectedItem {
    NSIndexPath *indexPath = [self indexPathForDateComponents:self.calenderMonth];
    return indexPath.row / 7;
}

- (void)setLineForSelectedDayHidden:(BOOL)hiden {
//    self.hideWeekForSelectedDay = hiden;
//    NSInteger line = [self lineOfCurentSelectedItem];
//    self.daysInCurrentWeek = NSMakeRange(line * 7, 7);
//    [self reloadData];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    
    NSRange weeks = [cal rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:self.date];
    
    NSInteger moreThanSeven = weeks.length + self.firstDayInWeek;
    
    NSInteger over = moreThanSeven % 7;
    
    return moreThanSeven + (7 - over);
}

- (NSInteger)lineForWeekOfYear:(NSInteger)week {
    
    NSDateComponents *compForFirstItem = [self dateComponentsForIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:compForFirstItem];
    
    compForFirstItem = [[NSCalendar currentCalendar]  components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekday | NSCalendarUnitDay | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear fromDate:date];
    
    return week - compForFirstItem.weekOfYear ;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ATCalendarDayCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"DayCell" forIndexPath:indexPath];
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    cell.dayLabel.backgroundColor = [UIColor clearColor];
    
    if (indexPath.row >= self.daysInCurrentWeek.location && indexPath.row < (self.daysInCurrentWeek.location + self.daysInCurrentWeek.length) && self.hideWeekForSelectedDay) {
        cell.alpha = 0;
    } else {
        cell.alpha = 1;
    }
    
    NSDateComponents *comp = [self dateComponentsForIndexPath:indexPath];
    
    cell.dayLabel.text = [NSString stringWithFormat:@"%d", comp.day];
    
    if (comp.month != self.calenderMonth.month) {
        [cell setDimmed:YES];
    } else [cell setDimmed:NO];
    
    [cell setHidden:NO];
    
    return cell;
}

- (NSDateComponents *)dateComponentsForIndexPath:(NSIndexPath *)indexPath {
 
    NSDateComponents *comps = [self.calenderMonth copy];
    
    if (indexPath.row < self.firstDayInWeek) {
        
        comps.month = comps.month - 1;
        comps.day = self.daysInPreviousMonth.length + (indexPath.row - self.firstDayInWeek + 1);
        
    } else {
        
        NSInteger currentDay = (indexPath.row - self.firstDayInWeek + 1);
        
        if (currentDay > self.daysInCurrentMonth.length) {
            comps.day = currentDay - self.daysInCurrentMonth.length;
            comps.month+=1;
        } else {
            comps.day = currentDay;
        }
        
    }
    
    comps.weekday = [ATDescriptiveDateComponents weekdayFromReginalWeekday:indexPath.row % 7];
    
    NSDate *date = [[NSCalendar currentCalendar] dateFromComponents:comps];
    
    NSDateComponents *realComp = [[NSCalendar currentCalendar]  components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekday | NSCalendarUnitDay | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear fromDate:date];
    
    return realComp;
}

- (NSIndexPath *)indexPathForDateComponents:(NSDateComponents *)dateComponents {
    
    if (dateComponents.month == self.calenderMonth.month && dateComponents.year == self.calenderMonth.year) {
        return [NSIndexPath indexPathForItem:dateComponents.day + self.firstDayInWeek - 1 inSection:0];
    }
    else return nil;

}


- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    __strong id <ATCalendarMonthViewControllerDelegate> delegate = self.delegate;
    
    self.calenderMonth = [self dateComponentsForIndexPath:indexPath];
    
    if ([delegate respondsToSelector:@selector(calendarMonthViewController:didSelectDate:)]) {
        [delegate calendarMonthViewController:self didSelectDate:self.calenderMonth];
    }
    
}

- (void)selectDayForCurrentDateComponents {
    
    __strong id <ATCalendarMonthViewControllerDelegate> delegate = self.delegate;

    if ([delegate respondsToSelector:@selector(calendarMonthViewController:didSelectDate:)]) {
        [delegate calendarMonthViewController:self didSelectDate:self.calenderMonth];
    }
    
}

@end
