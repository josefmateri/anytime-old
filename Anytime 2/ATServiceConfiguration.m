//
//  ATServiceConfiguration.m
//  anytime
//
//  Created by Josef Materi on 13.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATServiceConfiguration.h"
#import "ATDatasources.h"
#import "ATMiteServiceConfiguration.h"


@implementation ATServiceConfiguration

+ (id <ATServiceConfigurationProtocol>)serviceConfigurationForIdentifier:(NSString *)identifier {
    if ([identifier isEqualToString:ATMiteServiceIdentifier]) {
        return [[ATMiteServiceConfiguration alloc] init];
    }
    return nil;
}

@end
