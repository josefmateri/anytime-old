//
//  ATMainCategoryViewController.h
//  anytime
//
//  Created by Josef Materi on 06.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ATListViewController.h"
#import "ATCategoryDatasourceProtocol.h"

@interface ATMainCategoryViewController : ATListViewController
@property (nonatomic, weak) id <ATCategoryDatasourceProtocol> categoryDatasource;
@end
