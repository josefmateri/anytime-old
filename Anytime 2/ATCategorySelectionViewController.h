//
//  ATCategorySelectionViewController.h
//  anytime
//
//  Created by Josef Materi on 06.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ATDetailCategoryLabel.h"
#import "ATCategoryProtocol.h"
#import "ATCategoryConfiguration.h"
#import "ATCategoryDatasourceProtocol.h"

@protocol ATCategorySelectionViewControllerDelegate;
@protocol ATCategorySelectionViewControllerDatasource;

typedef enum {
    CategorySelectionModeMainCategory,
    CategorySelectionModeSecondaryCategory
} CategorySelectionMode;

@interface ATCategorySelectionViewController : UIViewController

@property (nonatomic, strong) ATDetailCategoryLabel *detailCategoryLabel;

@property (nonatomic, weak) id <ATCategorySelectionViewControllerDelegate> delegate;
@property (nonatomic, weak) id <ATCategorySelectionViewControllerDatasource> datasource;

@property (nonatomic, weak) id <ATCategoryDatasourceProtocol> categoryDatasource;

- (void)setSelectionElementsHidden:(BOOL)hidden;

- (void)reloadData;

- (void)setBottomBorderColor:(UIColor *)color;

@end

@protocol ATCategorySelectionViewControllerDatasource <NSObject>

- (ATCategoryConfiguration *)mainCategoryConfigurationForCategorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController;

- (ATCategoryConfiguration *)secondaryCategoryConfigurationForCategorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController;

- (id <ATCategoryProtocol>)mainCategoryForCategorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController;

- (id <ATCategoryProtocol>)secondaryCategoryForCategorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController;

- (NSString *)mainCategoryTitleForCategorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController;

- (NSString *)secondaryCategoryTitleForCategorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController;

- (BOOL)categorySelectionViewControllerHasSecondaryCategory:(ATCategorySelectionViewController *)categorySelectionViewController;

@end

@protocol ATCategorySelectionViewControllerDelegate <NSObject>

- (void)categorySelectionViewControllerWillShowMainCategorySelection:(ATCategorySelectionViewController *)categorySelectionViewController;

- (void)categorySelectionViewControllerWillShowSecondaryCategorySelection:(ATCategorySelectionViewController *)categorySelectionViewController;

- (void)categorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController didSelectMainCategory:(id <ATCategoryProtocol>)category;

- (void)categorySelectionViewController:(ATCategorySelectionViewController *)categorySelectionViewController didSelectSecondaryCategory:(id <ATCategoryProtocol>)category;

@end