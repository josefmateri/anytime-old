//
//  DigitsInputView.m
//  Anytime
//
//  Created by Josef Materi on 18.09.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import "DigitsInputView.h"
#import "UIView+Positioning.h"
#import "UIView+Animations.h"
#import "NSNumber+Time.h"
#import "UIView+FLKAutoLayout.h"
#import "NSObject+Delay.h"

@interface DigitsInputView ()

@property (nonatomic, strong) NSArray *digitViews;
@property (nonatomic, assign) NSInteger selectedDigitIndex;
@property (nonatomic, strong) JMKeyboardViewController *keyboardViewController;
@property (nonatomic, strong) NSLayoutConstraint *heightConstraint;
@property (nonatomic, strong) NSLayoutConstraint *digitContainerViewTopPositionConstraint;
@property (nonatomic, strong) NSLayoutConstraint *keyboardBottomPositionConstraint;

@property (nonatomic, strong) NSMutableArray *digits;
@property (nonatomic, assign) BOOL performingShakeAnimation;
@property (nonatomic, strong) UIView *digitContainerView;
@property (nonatomic, strong) UIImageView *glossyBackgroundUpperPart;
@end

@implementation DigitsInputView

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
	if(self) {
		// Initialization code


        self.clipsToBounds = NO;
        
		self.userInteractionEnabled = YES;

        
        _glossyBackgroundUpperPart = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"dark-bg"]];
        [_glossyBackgroundUpperPart adjustY:-44];
        [_glossyBackgroundUpperPart adjustHeightTo:104];
        [_glossyBackgroundUpperPart adjustWidthTo:self.bounds.size.width];
        [self addSubview:_glossyBackgroundUpperPart];
        
		_digitContainerView = [[UIView alloc] initWithFrame:frame];
		_digitContainerView.translatesAutoresizingMaskIntoConstraints = NO;

        [self addSubview:_digitContainerView];

		NSLayoutConstraint *_digitContainerViewHeightConstraints = [NSLayoutConstraint constraintWithItem:_digitContainerView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:120];

		[self addConstraint:_digitContainerViewHeightConstraints];
//
		NSLayoutConstraint *_digitContainerViewWidthConstraints = [NSLayoutConstraint constraintWithItem:_digitContainerView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:320];
		[self addConstraint:_digitContainerViewWidthConstraints];
//
//
		_digitContainerViewTopPositionConstraint = [NSLayoutConstraint constraintWithItem:_digitContainerView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:0];
		[self addConstraint:_digitContainerViewTopPositionConstraint];

//        [self addConstraints:[_digitContainerView alignAttribute:NSLayoutAttributeLeft toAttribute:NSLayoutAttributeLeft ofView:self predicate:@""]];
        
		_firstHour = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
		_secondHour = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
		_firstMinute = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
		_secondMinute = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
		_colon = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
                
//		_firstMinute.image = [UIImage imageNamed:@"0"];
//		_secondHour.image = [UIImage imageNamed:@"0"];
//		_secondMinute.image = [UIImage imageNamed:@"0"];
//		_firstHour.image = [UIImage imageNamed:@"0"];
//		_colon.image = [UIImage imageNamed:@"dot"];
        [_firstHour setTitle:@"0" forState:UIControlStateNormal];
        [_firstMinute setTitle:@"0" forState:UIControlStateNormal];
        [_secondHour setTitle:@"0" forState:UIControlStateNormal];
        [_secondMinute setTitle:@"0" forState:UIControlStateNormal];
        [_colon setTitle:@":" forState:UIControlStateNormal];
        
        NSLog(@"%@", [UIFont fontNamesForFamilyName:@"HelveticaNeue"]);
        
        _firstHour.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:100];
        
        _firstMinute.titleLabel.font = _firstHour.titleLabel.font;
        _secondMinute.titleLabel.font = _firstHour.titleLabel.font;
        _secondHour.titleLabel.font = _firstHour.titleLabel.font;
        _colon.titleLabel.font = _firstHour.titleLabel.font;
        
        
		_digitViews = @[_firstHour, _secondHour, _firstMinute, _secondMinute, _colon];

		_digits = [[NSMutableArray alloc] initWithArray:@[@ (0), @ (0), @ (0), @ (0)]];

   		[_digitViews enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL * stop) {
            
            [obj setContentMode:UIViewContentModeCenter];
            [obj setTranslatesAutoresizingMaskIntoConstraints:NO];
            [obj setUserInteractionEnabled:YES];
            [obj setContentHuggingPriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
            [obj setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisVertical];
            
            obj.titleLabel.textColor = [UIColor whiteColor];
            [_digitContainerView addSubview:obj];

            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
            [obj addGestureRecognizer:tap];
            

		 }];

		NSLayoutConstraint *constraint = [NSLayoutConstraint constraintWithItem:_colon attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:_digitContainerView attribute:NSLayoutAttributeCenterY multiplier:1 constant:0];
		[self addConstraint:constraint];

		NSLayoutConstraint *constraint2 = [NSLayoutConstraint constraintWithItem:_colon attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:_digitContainerView attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];

		[self addConstraint:constraint2];

		NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"[_firstHour]-(-5)-[_secondHour]-(5)-[_colon]-(5)-[_firstMinute]-(-5)-[_secondMinute]" options:NSLayoutFormatAlignAllCenterY metrics:nil views:NSDictionaryOfVariableBindings (_firstHour, _secondHour, _colon, _firstMinute, _secondMinute)];

		[self addConstraints:constraints];



//		UIImageView *topBorderView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"shadow-gradient-bg"]];
//		topBorderView.translatesAutoresizingMaskIntoConstraints = NO;
//		[self addSubview:topBorderView];
//		topBorderView.alpha = 1;
//
//		NSLayoutConstraint *topBorderViewConstraint = [NSLayoutConstraint constraintWithItem:topBorderView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:120];
//
//		[self addConstraint:topBorderViewConstraint];

        UICollectionViewFlowLayout *flowLayout =[[UICollectionViewFlowLayout alloc] init];
        
        
        flowLayout.itemSize = CGSizeMake(self.bounds.size.width / 3.5, 44);
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10);
        flowLayout.minimumInteritemSpacing = 0.0f;
        flowLayout.minimumLineSpacing = 5.0f;
        
		_keyboardViewController = [[JMKeyboardViewController alloc] initWithCollectionViewLayout:flowLayout];
		_keyboardViewController.delegate = self;
		_keyboardViewController.datasource = self;

		_keyboardViewController.collectionView.alpha = 0;

        [_keyboardViewController.view setHidden:YES];
        
		[self addSubview:_keyboardViewController.view];

		self.clipsToBounds = NO;

    
		_keyboardBottomPositionConstraint = [NSLayoutConstraint constraintWithItem:self.keyboardViewController.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:270];
		_keyboardBottomPositionConstraint.priority = 100;

		[self addConstraint:_keyboardBottomPositionConstraint];

        [self inital];

        [self layoutIfNeeded];
                
	}
	return self;
}

- (void)moveToInitialPosition {
    [self setScale:self.minimumScale andPosition:CGPointMake(self.center.x, self.minimumOffset.y)];
}

- (void)setScale:(float)scale andPosition:(CGPoint)position {
    if (self.animationsDisabled) return;
    
    [self.layer removeAllAnimations];
    
   self.layer.transform = CATransform3DIdentity;
   self.layer.transform = CATransform3DMakeScale(scale, scale, scale);
   self.layer.position = position;
    //   self.layer.transform = CATransform3DConcat(CATransform3DMakeTranslation(0, position.y, 0), self.layer.transform  );
    
}

//- (void)setUserInteractionEnabled:(BOOL)userInteractionEnabled {
//    [super setIsAccessibilityElement:userInteractionEnabled];
//}

- (void)setAnimationsDisabled:(BOOL)animationsDisabled {
    _animationsDisabled = animationsDisabled;
    [self.layer removeAllAnimations];
    self.layer.transform = CATransform3DIdentity;
  //  _digitContainerView.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
}

- (void)inital {
    
    self.minimumOffset = CGPointMake(0, 21);
    self.minimumScale = 0.25;
    
    self.digitContainerView.alpha = 1;
    
//    [self setScale:1 andPosition:self.maximumOffset];

//    [self performBlock:^{
//        
//
//        [self setScale:self.minimumScale andPosition:self.minimumOffset];
//
//        self.backgroundColor = [UIColor redColor];
//        
//    } afterDelay:0.1];
//    [self setScale:0.4 andPosition:CGPointMake(0, 0)];

}

#pragma mark - Numbers

- (void)setFormattedDuration:(NSString *)string {
	NSArray *split = [string componentsSeparatedByString:@":"];

	NSString *firstHalf = [split objectAtIndex:0];
	NSString *secondHalf = [split objectAtIndex:1];

	[self setDigit:[[firstHalf substringWithRange:NSMakeRange(0, 1)] intValue] atIndex:0];
	[self setDigit:[[firstHalf substringWithRange:NSMakeRange(1, 1)] intValue] atIndex:1];
	[self setDigit:[[secondHalf substringWithRange:NSMakeRange(0, 1)] intValue] atIndex:2];
	[self setDigit:[[secondHalf substringWithRange:NSMakeRange(1, 1)] intValue] atIndex:3];
}

- (void)setDurationInMinutes:(NSNumber *)duration {

    int hoursTotal = [duration intValue] / 60;
    
    int hoursTen = hoursTotal / 10;
    int hours = hoursTotal - hoursTen;
    
    int totalMinutes = [duration intValue] - hoursTotal;
    
    int minutesTen = totalMinutes / 10;
    int minutes = totalMinutes - minutesTen;
    
    [self setDigit:hoursTen atIndex:0];
	[self setDigit:hours atIndex:1];
	[self setDigit:minutesTen atIndex:2];
	[self setDigit:minutes atIndex:3];
}

- (NSNumber *)durationInMinutes {
	int hoursTen = [[_digits objectAtIndex:0] intValue];
	int hours = [[_digits objectAtIndex:1] intValue];
	int minutesTen = [[_digits objectAtIndex:2] intValue];
	int minutes = [[_digits objectAtIndex:3] intValue];

	return @ ((hoursTen * 10 + hours) * 60 + minutesTen * 10 + minutes);
}

- (NSString *)formattedDuration {
	int hoursTen = [[_digits objectAtIndex:0] intValue];
	int hours = [[_digits objectAtIndex:1] intValue];
	int minutesTen = [[_digits objectAtIndex:2] intValue];
	int minutes = [[_digits objectAtIndex:3] intValue];

	return [NSString stringWithFormat:@"%i%i:%i%i", hoursTen, hours, minutesTen, minutes];
}

#pragma mark - Editing

- (void)startEditing {
	UIView *selectedDigitView = [_digitViews objectAtIndex:0];

	if(selectedDigitView) {
		NSInteger newSelectedDigitViewIndex = [_digitViews indexOfObject:selectedDigitView];

		[self selectDigitAtIndex:newSelectedDigitViewIndex];


		if([_delegate respondsToSelector:@selector(digitsInputView:didSelectDigitAtIndex:)]) {
			[_delegate digitsInputView:self didSelectDigitAtIndex:_selectedDigitIndex];
		}

    
        
		if(!_editing) {
			_editing = YES;
			_keyboardBottomPositionConstraint.constant = 0;

            _keyboardViewController.collectionView.layer.shouldRasterize = YES;
            _keyboardViewController.collectionView.layer.rasterizationScale = 2.0;
            
            [UIView animateWithDuration:3 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
				 _keyboardViewController.collectionView.alpha = 1;
				 [self layoutIfNeeded];
			 } completion:^(BOOL finished) {
                 _keyboardViewController.collectionView.layer.shouldRasterize = NO;
			 }];

			if([_delegate respondsToSelector:@selector(digitsInputViewDidStartEditing:)]) {
				[_delegate digitsInputViewDidStartEditing:self];
			}
		}
	}
}

- (void)setEditingMode:(DigitsInputViewEditingMode)editingMode {
	switch(editingMode) {
		case DigitsInputViewEditingModeDuration :
			[self startEditing];
			break;
		case DigitsInputViewEditingModeTime : {
			
        }
		break;
		default : {
            
            if (_editingMode == DigitsInputViewEditingModeTime) {
                [_digitContainerView startBlinkingWithSpeed:0.25 stopAfterDelay:0.25 completion:^{
                }];
            }

			UIView *oldDigit = [_digitViews objectAtIndex:_selectedDigitIndex];
			[oldDigit stopBlinking];
			_keyboardBottomPositionConstraint.constant = 270;
            _keyboardViewController.collectionView.layer.shouldRasterize = YES;
            _keyboardViewController.collectionView.layer.rasterizationScale = [[UIScreen mainScreen] scale];
            
			[UIView animateWithDuration:0.7 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
                _keyboardViewController.collectionView.alpha = 0;
                [self layoutIfNeeded];
            } completion:^(BOOL finished) {
                _keyboardViewController.collectionView.layer.shouldRasterize = NO;
            }];


			if(_editing) {
				if([_delegate respondsToSelector:@selector(digitsInputViewDidEndEditing:)]) {
					[_delegate digitsInputViewDidEndEditing:self];
				}
				_editing = NO;
			}
		}
		break;
	}
	_editingMode = editingMode;

}

#pragma mark - Gestures

- (void)handleTap:(UITapGestureRecognizer *)tap {
    
    if ([_datasource respondsToSelector:@selector(digitsInputviewShouldStartEditing:)]) {
        if (![_datasource digitsInputviewShouldStartEditing:self]) {
            return;
        }
    }
    
	CGPoint touchPoint = [tap locationInView:self];

	UIView *selectedDigitView = [self hitTest:touchPoint withEvent:nil];

	if(selectedDigitView) {
		NSInteger newSelectedDigitViewIndex = [_digitViews indexOfObject:selectedDigitView];

		[self selectDigitAtIndex:newSelectedDigitViewIndex];


		if([_delegate respondsToSelector:@selector(digitsInputView:didSelectDigitAtIndex:)]) {
			[_delegate digitsInputView:self didSelectDigitAtIndex:_selectedDigitIndex];
		}
    
		if(!_editing) {
            
			_editing = YES;

            if([_delegate respondsToSelector:@selector(digitsInputViewWillStartEditing:)]) {
                [_delegate digitsInputViewWillStartEditing:self];
            }
            
            [self layoutIfNeeded];

    
			_keyboardBottomPositionConstraint.constant = 0;
            _keyboardViewController.collectionView.layer.shouldRasterize = YES;
            _keyboardViewController.collectionView.layer.rasterizationScale = 2.0;
            
            [_keyboardViewController.view setHidden:NO];
            
            [UIView animateWithDuration:0.7 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
				 _keyboardViewController.collectionView.alpha = 1;
				 [self layoutIfNeeded];
			 } completion:^(BOOL finished) {
                 _keyboardViewController.collectionView.layer.shouldRasterize = NO;

			 }];

			if([_delegate respondsToSelector:@selector(digitsInputViewDidStartEditing:)]) {
				[_delegate digitsInputViewDidStartEditing:self];
			}
		}
	}
}

#pragma mark
#pragma mark - Change digits

- (void)selectDigitAtIndex:(NSInteger)index {
	UIView *oldDigit = [_digitViews objectAtIndex:_selectedDigitIndex];

	if(index < 4) {
		_selectedDigitIndex = index;
		[oldDigit stopBlinking];

		UIView *selectedDigitView = [_digitViews objectAtIndex:_selectedDigitIndex];
		[selectedDigitView startBlinkingWithSpeed:0.4];
	}
}

- (void)setCurrentDigitAndCheckValidity:(NSInteger)digit {
	NSInteger oldDigit = [[_digits objectAtIndex:_selectedDigitIndex] intValue];

	[self setDigit:digit atIndex:_selectedDigitIndex];
    
	if(_selectedDigitIndex == 2) {
		if(digit > 5) {
			if(!_performingShakeAnimation) {
				UIView *selectedDigitView = [_digitViews objectAtIndex:_selectedDigitIndex];

				[selectedDigitView stopBlinking];

				_performingShakeAnimation = YES;
				[selectedDigitView shakeAnimationWithDuration:0.1 repeatCount:4 completed:^{
					 [self setDigit:oldDigit atIndex:_selectedDigitIndex];
					 [selectedDigitView startBlinkingWithSpeed:0.4];
					 _performingShakeAnimation = NO;
				 }];
			}

			return;
		}
	}
    
    __strong id <DigitsInputViewDelegate> delegate = self.delegate;
    
    if ([delegate respondsToSelector:@selector(digitsInputView:didSelectDigit:atIndex:)]) {
        [delegate digitsInputView:self didSelectDigit:digit atIndex:_selectedDigitIndex];
    }

	[self goToNextDigit];
}

- (void)setCurrentDigit:(NSInteger)digit {
	[self setDigit:digit atIndex:_selectedDigitIndex];
}

- (void)setDigit:(NSInteger)digit atIndex:(NSInteger)index {
	if(index < 4) {
        
		UIButton *buttonAtIndex = [_digitViews objectAtIndex:index];
		[buttonAtIndex setTitle:[@(digit) stringValue] forState:UIControlStateNormal];

		if(_selectedDigitIndex == 2) {
			if(digit > 5) {
				return;
			}
		}

		[_digits replaceObjectAtIndex:index withObject:@ (digit)];
        
        
    }
}

- (void)goToNextDigit {
	if(_selectedDigitIndex < 3) {
		[self selectDigitAtIndex:_selectedDigitIndex + 1];
	}
}

- (void)goToPreviousDigit {
	if(_selectedDigitIndex > 0) {
		[self selectDigitAtIndex:_selectedDigitIndex - 1];
	}
}

#pragma mark - Animation

- (void)startColonAnimation {
	[_colon startBlinkingWithSpeed:0.5];
}

- (void)stopColonAnimation {
	[_colon stopAnimations];
}

/*
 * // Only override drawRect: if you perform custom drawing.
 * // An empty implementation adversely affects performance during animation.
 * - (void)drawRect:(CGRect)rect
 * {
 *  // Drawing code
 * }
 */

#pragma mark - JMKeyboardViewController

- (void)keyboardViewController:(JMKeyboardViewController *)kvc configureItem:(JMKeyboardViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
	if(indexPath.row == 11) {
		cell.imageView.image = [UIImage imageNamed:@"keyboardArrowLeft"];
	} else if(indexPath.row == 12) {
		cell.textLabel.text = NSLocalizedString(@"Enter", nil);
	} else {
		cell.textLabel.text = [@ ([self digitForIndexPath:indexPath])stringValue];
	}
}

- (void)keyboardViewController:(JMKeyboardViewController *)kvc didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	if(indexPath.row == 11) {
		[self setCurrentDigit:0];
		[self goToPreviousDigit];
	} else if(indexPath.row == 12) {
	} else {
		[self setCurrentDigitAndCheckValidity:[self digitForIndexPath:indexPath]];
	}
}

- (NSInteger)digitForIndexPath:(NSIndexPath *)indexPath {
	if(indexPath.row < 9) {
		return indexPath.row + 1;
	}
	if(indexPath.row == 10) {
		return 0;
	}
	return 0;
}

@end
