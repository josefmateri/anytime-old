//
//  ATTimeEntriesViewController.m
//  Anytime 2
//
//  Created by Josef Materi on 23.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATTimeEntriesListViewController.h"
#import "ATTimeEntriesCell.h"
#import "NSDate+Additions.h"
#import <CoreData/CoreData.h>
#import <RestKit.h>
#import "MiteTimeEntry.h"
#import "MiteDateTransformers.h"
#import "ATMiteDatasource.h"
#import "NSNumber+Time.h"
#import "ATStopWatchViewController.h"

@interface ATTimeEntriesListViewController () <NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, strong) NSArray *deletedObjects;
@end

@implementation ATTimeEntriesListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     
    
    [self.tableView registerClass:[ATTimeEntriesCell class] forCellReuseIdentifier:@"TimeEntriesCell"];

    // Uncomment the following line to preserve selection between presentations.
     self.clearsSelectionOnViewWillAppear = YES;
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
      
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    [refreshControl addTarget:self action:@selector(refreshControlDidChangeState:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = refreshControl;
    
    self.tableView.allowsMultipleSelectionDuringEditing = YES;

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadData {

    NSFetchRequest *fetchRequest = [[ATDatasources currentDatasource] fetchRequestForTimeEntriesWithDateComponents:self.dateComponents];
                                    
    fetchRequest.sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"created_at" ascending:YES selector:@selector(compare:)]];
    
    self.fetchedResultsController = nil;
    
    NSManagedObjectContext *context = [[ATDatasources currentDatasource] mainQueueManagedObjectContext];
    
    self.fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
    
    self.fetchedResultsController.delegate = self;
    
    [self.fetchedResultsController performFetch:nil];
    
    [self.tableView reloadData];
    
    
 //   [self refreshSelectedTimeEntries];

    
}

- (void)refreshSelectedTimeEntries {
    __strong id <ATListViewControllerDatasource> datasource = self.datasource;
    
    if ([datasource respondsToSelector:@selector(selectedObjectsForListViewController:)]) {
        
        NSSet *selected = [datasource selectedObjectsForListViewController:self];
        
        
        [selected enumerateObjectsUsingBlock:^(id <ATTimeEntryProtocol> timeEntry, BOOL *stop) {
            
            NSIndexPath *indexPath = [self.fetchedResultsController indexPathForObject:timeEntry];
            
            NSLog(@"Selecting indexPath: %@", indexPath);
            
            [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            
        }];
        
    }
}

- (void)refreshData {

    // Add our descriptors to the manager
    id <ATDatasourceProtocol> datasource = [ATDatasources currentDatasource];
    
    [datasource getTimeEntriesWithDateComponents:self.dateComponents complete:^(NSArray *results) {
        
        [self.refreshControl endRefreshing];
        
    } failed:^(NSError *failed) {
        
        [self.refreshControl endRefreshing];
        
    }];
    
}



- (void)refreshControlDidChangeState:(UIRefreshControl *)refreshControl {
    
    if ([refreshControl isRefreshing]) {
        [self refreshData];
    }
}

#pragma mark - Table view data source

#pragma mark - UITableView Delegate and Datasource

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ATTimeEntriesCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TimeEntriesCell"];
    
    
    NSManagedObject <ATTimeEntryProtocol> *managedObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *task_name = [managedObject valueForKey:@"service_name"];
    
    
    if (task_name) {
        cell.headline1.text = task_name;
    } else {
        cell.headline1.text = NSLocalizedString(@"No task", nil);
    }
    
    NSString *project_name = [managedObject valueForKey:@"project_name"];
    
    if (project_name) {
        cell.headline2.text = project_name;
    } else {
        cell.headline2.text = NSLocalizedString(@"No project", nil);
    }
    
    NSString *customer_name = [managedObject valueForKey:@"customer_name"];
    if (customer_name) {
        cell.subline1.text = customer_name;
    } else {
        cell.subline1.text = NSLocalizedString(@"No customer", nil);
    }
    
    NSNumber *minutes = [managedObject durationInMinutes];
    

    NSNumber *trackingMinutes = [managedObject trackingDurationInMinutes];
    
    NSDate *trackingSince = [managedObject trackingSince];
    
    
    if (trackingSince) {
        cell.minutes.text = [trackingMinutes durationValue];
        cell.minutesBackgroundColor = [UIColor colorWithRed:0.715 green:0.334 blue:0.358 alpha:1.000];
    } else {
        cell.minutes.text = [minutes durationValue];
        cell.minutesBackgroundColor = [UIColor colorWithRed:0.358 green:0.472 blue:0.590 alpha:1.000];
    }
   
    if ([self isEditing]) {
        __strong id <ATListViewControllerDatasource> datasource = self.datasource;
        if ([datasource respondsToSelector:@selector(selectedObjectsForListViewController:)]) {
            
            NSSet *selected = [datasource selectedObjectsForListViewController:self];
                        
            if ([selected containsObject:managedObject]) {
                [self.tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */


@end
