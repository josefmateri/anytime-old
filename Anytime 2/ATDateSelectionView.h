//
//  ATDateSelectionView.h
//  Anytime 2
//
//  Created by Josef Materi on 07.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ATDateView.h"
#import "ATCalenderMonthViewController.h"
#import "DMLazyScrollView.h"
#import "ATDateLabel.h"

typedef enum  {
    DateSelectionViewStateClosed,
    DateSelectionViewStateWillOpen,
    DateSelectionViewStateWillClose,
    DateSelectionViewStateOpen
} DateSelectionViewState;

@protocol ATDateSelectionViewDelegate;
@protocol ATDateSelectionViewDatasource;

@interface ATDateSelectionView : UIView <ATCalendarMonthViewControllerDelegate, DMLazyScrollViewDelegate>

@property (nonatomic, weak) id <ATDateSelectionViewDelegate> delegate;
@property (nonatomic, weak) id <ATDateSelectionViewDatasource> datasource;

@property (nonatomic, strong) ATDateView *dateView;
@property (nonatomic, strong) ATDateLabel *dateLabel;
@property (nonatomic, strong) NSDateComponents *dateComponents;
@property (nonatomic) UIEdgeInsets contentInsets;
@property (nonatomic) float initialHeight;

- (void)setTransparentBackground:(BOOL)transparent;

- (void)didMoveToPoint:(CGPoint)p withState:(UIGestureRecognizerState)state andVelocity:(CGPoint)velo;

- (void)setCurrentDate:(NSDate *)date;
- (void)updateWithDateComponents:(NSDateComponents *)components;

- (void)removeAllAnimators;

- (void)expand:(BOOL)expand animated:(BOOL)animated;

- (void)goToNextDay;
- (void)goToPrevDay;

- (void)reloadNavigationItems;

- (void)updateMonthView;

@end

@protocol ATDateSelectionViewDatasource <NSObject>
- (UINavigationItem *)navigationItemsForDataSelectionView:(ATDateSelectionView *)dateSelectionview;
@end

@protocol ATDateSelectionViewDelegate <NSObject>

- (void)dateSelectionViewDidBeginBouncing:(ATDateSelectionView *)selectionView;
- (void)dateSelectionView:(ATDateSelectionView *)selectionView didSelectDateWithComponents:(NSDateComponents *)components;
- (void)dateSelectionViewDidToggleMenu:(ATDateSelectionView *)selectionView;
- (void)dateSelectionView:(ATDateSelectionView *)selectionView setEditing:(BOOL)editing;

@optional

- (void)dateSelectionViewDidChangeBounds:(ATDateSelectionView *)selectionView;

@end