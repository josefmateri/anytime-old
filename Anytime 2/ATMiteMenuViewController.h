//
//  ATMenuViewController.h
//  anytime
//
//  Created by Josef Materi on 26.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//


@protocol ATMenuViewControllerDelegate;

#import "ATMainCategoryViewController.h"
#import "ATCalendarWithTimeEntriesViewController.h"
#import "ATProjectsViewController.h"

@interface ATMiteMenuViewController : UIViewController

@property (nonatomic, weak) id <ATDatasourceProtocol> remoteDatasource;
@property (nonatomic, weak) id <ATMenuViewControllerDelegate> delegate;

@property (nonatomic, strong) ATCalendarWithTimeEntriesViewController *calendarViewController;
@property (nonatomic, strong) ATProjectsViewController *mainCategoryViewController;


@end

@protocol ATMenuViewControllerDelegate <NSObject>

- (void)menuViewController:(ATMiteMenuViewController *)controller didSelectViewController:(UIViewController *)viewController;

@end