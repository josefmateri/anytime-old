//
//  ATCalendarWeekHeaderView.h
//  Anytime 2
//
//  Created by Josef Materi on 22.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ATCalendarWeekHeaderView : UICollectionReusableView

@end
