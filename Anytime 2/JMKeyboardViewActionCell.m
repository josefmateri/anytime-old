//
//  JMKeyboardViewActionCell.m
//  Anytime
//
//  Created by Josef Materi on 06.10.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import "JMKeyboardViewActionCell.h"

@implementation JMKeyboardViewActionCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
    
        
    }
    return self;
}

- (UIImageView *)customBackgroundImageView {
    return [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"default-navigation-cell-selected-bg"]
                                               resizableImageWithCapInsets:UIEdgeInsetsMake(0, 1, 0, 1) resizingMode:UIImageResizingModeStretch]];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
