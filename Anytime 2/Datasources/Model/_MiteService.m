// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MiteService.m instead.

#import "_MiteService.h"

const struct MiteServiceAttributes MiteServiceAttributes = {
	.created_at = @"created_at",
	.name = @"name",
	.remote_id = @"remote_id",
	.updated_at = @"updated_at",
};

const struct MiteServiceRelationships MiteServiceRelationships = {
};

const struct MiteServiceFetchedProperties MiteServiceFetchedProperties = {
};

@implementation MiteServiceID
@end

@implementation _MiteService

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MiteService" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MiteService";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MiteService" inManagedObjectContext:moc_];
}

- (MiteServiceID*)objectID {
	return (MiteServiceID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"remote_idValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"remote_id"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic created_at;






@dynamic name;






@dynamic remote_id;



- (int64_t)remote_idValue {
	NSNumber *result = [self remote_id];
	return [result longLongValue];
}

- (void)setRemote_idValue:(int64_t)value_ {
	[self setRemote_id:[NSNumber numberWithLongLong:value_]];
}

- (int64_t)primitiveRemote_idValue {
	NSNumber *result = [self primitiveRemote_id];
	return [result longLongValue];
}

- (void)setPrimitiveRemote_idValue:(int64_t)value_ {
	[self setPrimitiveRemote_id:[NSNumber numberWithLongLong:value_]];
}





@dynamic updated_at;











@end
