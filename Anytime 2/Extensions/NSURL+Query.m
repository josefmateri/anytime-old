//
//  NSURL+Query.m
//  anytime
//
//  Created by Josef Materi on 14.09.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "NSURL+Query.h"

@implementation NSURL (Query)

- (NSDictionary *)queryDictionary {
    NSString* queryStr = [self query];
    NSMutableDictionary* result = [NSMutableDictionary dictionary];

    NSArray* parameterArrs = [queryStr componentsSeparatedByString:@"&"];
    for (NSString* pair in parameterArrs) {
        NSArray* pairArr = [pair componentsSeparatedByString:@"="];
        if ([pairArr count] > 1) {
            [result setValue:[pairArr objectAtIndex:1] forKey:[pairArr objectAtIndex:0]];
        }
    }
    return result;
}

@end
