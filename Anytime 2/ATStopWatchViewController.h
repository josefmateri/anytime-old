//
//  ATStopWatchViewController.h
//  Anytime 2
//
//  Created by Josef Materi on 13.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ATDynamicBouncingDelegate;
@protocol ATStopWatchViewControllerDelegate;
@protocol ATStopWatchViewControllerNavigationDelegate;
@protocol ATTimeEntryProtocol;
@protocol ATCategoryDatasourceProtocol;

typedef enum {
    TimerButtonStateStart,
    TimerButtonStateStop,
    TimerButtonStateEnter,
    TimerButtonStateCreate
} TimerButtonState;

typedef enum {
    StopWatchModeTimerReady,
    StopWatchModeTimerRunning,
    StopWatchModeTimerStopped
} StopWatchMode;

typedef enum {
    NavigationModeDefault,
    NavigationModeNew,
    NavigationModeEdit,
    NavigationModeGlobal,
    NavigationModeDateSelection
} NavigationMode;

typedef enum {
    TimeRangeModeNotEditing,
    TimeRangeModeEditingFrom,
    TimeRangeModeEditingTo,
    TimeRangeModeEditingPauses,
    TimeRangeModeEditingDuration,
    TimeRangeModeEditingDate,
    TimeRangeModeEditingCategory
} TimeRangeEditingMode;

@interface ATStopWatchViewController : UIViewController

@property (nonatomic, weak) id <ATStopWatchViewControllerDelegate> delegate;
@property (nonatomic, weak) id <ATStopWatchViewControllerNavigationDelegate> navigationDelegate;

@property (nonatomic, strong) UIImageView *backgroundImage;
@property (nonatomic, weak) NSLayoutConstraint *positionContraint;
@property (nonatomic, weak) NSLayoutConstraint *parentPositionConstraint;
@property (nonatomic) BOOL presenting;
@property (nonatomic) float initialBottomOffset;
@property (nonatomic) TimerButtonState timerButtonState;
@property (nonatomic) TimeRangeEditingMode timeRangeEditingMode;
@property (nonatomic) StopWatchMode stopWatchMode;
@property (nonatomic) NavigationMode navigationMode;
@property (nonatomic) BOOL hideCompletetly;
@property (nonatomic, strong) NSDateComponents *dateComponents;

@property (nonatomic, weak) id <ATDatasourceProtocol, ATCategoryDatasourceProtocol> remoteDatasource;

@property (nonatomic, strong) id <ATTimeEntryProtocol> timeEntry;

#pragma mark - Public


+ (instancetype)sharedStopwatchViewController;
- (void)presentInViewController:(UIViewController *)viewController animated:(BOOL)animated completion:(void (^)())complete;
- (void)resetPosition;

- (void)show;
- (void)showAnimatedWithCompletion:(void (^)())completionBlock;

- (void)hideAnimatedWithCompletion:(void (^)())completionBlock;

- (void)hideWithSpeed:(float)speed completion:(void (^)())completionBlock;

- (void)reload;
- (void)showTimerDigits;

#pragma mark - TimeEntry Configuration

- (void)configureWithTimeEntry:(id <ATTimeEntryProtocol>)timeEntry;
- (BOOL)isTracking;

@end

@protocol ATStopWatchViewControllerDelegate <NSObject>
@optional
- (void)stopWatchViewControllerDidAppear:(ATStopWatchViewController *)stopWatchViewController;
- (void)stopWatchViewControllerWillAppear:(ATStopWatchViewController *)stopWatchViewController;
- (void)stopWatchViewControllerWillDisappear:(ATStopWatchViewController *)stopWatchViewController;
- (void)stopWatchViewControllerDidDisappear:(ATStopWatchViewController *)stopWatchViewController;

@end

@protocol ATStopWatchViewControllerNavigationDelegate <NSObject>

- (void)stopWatchViewControllerWillShowMenu:(ATStopWatchViewController *)stopWatchViewController;

- (void)stopWatchViewControllerWillClose:(ATStopWatchViewController *)stopWatchViewController withSpeed:(float)speed;
- (BOOL)stopWatchViewControllerShouldHideCompletely:(ATStopWatchViewController *)stopWatchViewController;
- (void)stopWatchViewControllerWillAddNewTimeEntry:(ATStopWatchViewController *)stopWatchViewController;

- (void)stopWatchViewController:(ATStopWatchViewController *)stopWatchViewController willStartTimeEntry:(id <ATTimeEntryProtocol>)timeEntry success:(void (^)())success failure:(void (^)(NSError *error))failure;
- (void)stopWatchViewController:(ATStopWatchViewController *)stopWatchViewController willStopTimeEntry:(id <ATTimeEntryProtocol>)timeEntry success:(void (^)())success failure:(void (^)(NSError *error))failure;

@end


@protocol ATDynamicBouncingDelegate <NSObject>

- (void)dynamicBouncer:(UIViewController *)viewController isBouncingToFrame:(CGRect)frame;
- (void)dynamicAnimatorDidPause:(UIDynamicAnimator*)animator;

@end