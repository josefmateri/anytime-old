//
//  DigitsInputView.h
//  Anytime
//
//  Created by Josef Materi on 18.09.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JMKeyboardViewController.h"
#import "JMKeyboardViewActionCell.h"

@protocol DigitsInputViewDelegate;
@protocol DigitsInputViewDatasource;

typedef enum {
    DigitsInputViewEditingModeNotEditing,
    DigitsInputViewEditingModeDuration,
    DigitsInputViewEditingModeTime
} DigitsInputViewEditingMode;

@interface DigitsInputView : UIView <JMKeyboardViewDelegate, JMKeyboardViewDatasource>

@property (nonatomic, strong) UIButton *firstHour;
@property (nonatomic, strong) UIButton *secondHour;
@property (nonatomic, strong) UIButton *firstMinute;
@property (nonatomic, strong) UIButton *secondMinute;
@property (nonatomic, strong) UIButton *colon;
@property (nonatomic, assign) CGSize digitSize;
@property (nonatomic, assign, readonly, getter = isEditing) BOOL editing;
@property (nonatomic, weak) id <DigitsInputViewDatasource> datasource;
@property (nonatomic, weak) id <DigitsInputViewDelegate> delegate;
@property (nonatomic) float minimumScale;
@property (nonatomic) CGPoint minimumOffset;
@property (nonatomic) CGPoint maximumOffset;

@property (nonatomic) BOOL animationsDisabled;

@property (nonatomic, assign) DigitsInputViewEditingMode editingMode;

- (void)moveToInitialPosition;

- (void) startColonAnimation;
- (void) stopColonAnimation;

- (void)setCurrentDigit:(NSInteger)digit;

- (void)setDigit:(NSInteger)digit atIndex:(NSInteger)index;

- (void)startEditing;

- (void)setFormattedDuration:(NSString *)string;

- (NSNumber *)durationInMinutes;
- (NSString *)formattedDuration;

- (void)setScale:(float)scale andPosition:(CGPoint)position;

@end

@protocol DigitsInputViewDelegate <NSObject>


- (void)digitsInputView:(DigitsInputView *)digitsInputView didSelectDigit:(NSInteger)digit atIndex:(NSInteger)index;

- (void)digitsInputViewWillStartEditing:(DigitsInputView *)digitsInputView;
- (void)digitsInputViewWillEndEditing:(DigitsInputView *)digitsInputView;
- (void)digitsInputViewDidStartEditing:(DigitsInputView *)digitsInputView;
- (void)digitsInputViewDidEndEditing:(DigitsInputView *)digitsInputView;

@optional

- (void)digitsInputView:(DigitsInputView *)digitsInputView didSelectDigitAtIndex:(NSInteger)index;

@end

@protocol DigitsInputViewDatasource <NSObject>
- (BOOL)digitsInputviewShouldStartEditing:(DigitsInputView *)digitsInputView;
- (NSInteger)digitsInputView:(DigitsInputView *)digitsInputView digitAtIndex:(NSInteger)index;

@end