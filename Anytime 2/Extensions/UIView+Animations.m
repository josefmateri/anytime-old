//
//  UIView+Animations.m
//  Anytime
//
//  Created by Josef Materi on 23.10.11.
//  Copyright (c) 2011 materi design. All rights reserved.
//

#import "UIView+Animations.h"
#import <QuartzCore/QuartzCore.h>
#import "NSObject+Delay.h"
#import "NSTimer+LTKAdditions.h"

@implementation UIView (Animations)


- (void)startBlinkingWithSpeed:(CGFloat)speed {
    CABasicAnimation *fullRotation = [CABasicAnimation animationWithKeyPath: @"opacity"];
    
    fullRotation.fromValue = [NSNumber numberWithFloat: 0];
    fullRotation.toValue = [NSNumber numberWithFloat: 1];
    fullRotation.duration = speed;
    fullRotation.repeatCount = 1e38f;
    fullRotation.autoreverses = YES;
    
    [[self layer] addAnimation: fullRotation forKey: @"opacity"];
}

-(void)startBlinkingWithSpeed:(CGFloat)speed stopAfterDelay:(float)delay completion:(CompletionHandler)handler {
    
    [self startBlinkingWithSpeed:speed];
    
    [self performBlock:^{
        [self stopBlinking];
        handler();
    } afterDelay:delay];
    
}

-(void)stopBlinkingAfterDelay:(float)delay {
    [self performSelector:@selector(stopBlinking) withObject:nil afterDelay:delay];
}

-(void)stopBlinking {
    [self stopAnimations];
}

- (void)stopAnimations {
    [[self layer] removeAllAnimations];
}

- (void)shakeAnimationWithDuration:(float)duration repeatCount:(NSUInteger)repeatCount completed:(CompletionHandler)handler
{
	CAKeyframeAnimation *animation;
	animation = [CAKeyframeAnimation animationWithKeyPath: @"transform.rotation.z"];
	[animation setDuration: duration];
	[animation setRepeatCount: repeatCount];
	// Try to get the animation to begin to start with a small offset // that makes it shake out of sync with other layers. f
	srand([[NSDate date] timeIntervalSince1970]);
	float rand = (float)random();
	
	[animation setBeginTime: CACurrentMediaTime() + rand * .0000000001];
	
	NSMutableArray *values = [NSMutableArray array]; // Turn right
	[values addObject: [NSNumber numberWithFloat: -2 * M_PI / 180]]; // Turn left
	[values addObject: [NSNumber numberWithFloat: 2 * M_PI / 180]]; // Turn right
	[values addObject: [NSNumber numberWithFloat: -2 * M_PI / 180]]; // Set the values for the animation
	[animation setValues: values];
	
	[[self layer] addAnimation: animation forKey: @"shake"];
    
    [self performBlock:^{
        handler();
    } afterDelay:duration * repeatCount];
}


- (void)fadeToAlpha:(float)alpha withDuration:(NSTimeInterval)duration completed:(CompletionHandler)handler {
    
    [UIView animateWithDuration:duration delay:0 options:0 animations:^{
        self.alpha = alpha;
    } completion:^(BOOL finished) {
        handler();
    }];
}

- (void)animateToScale:(float)scale andMoveByOffset:(CGPoint)offset duration:(NSTimeInterval)duration complete:(void (^)())complete {
    
    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithFloat:duration] forKey:kCATransactionAnimationDuration];
    [CATransaction setCompletionBlock:^{
        if (complete) complete();
    }];
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    
    animation.duration = duration;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.fromValue = [[self.layer presentationLayer] valueForKeyPath:@"transform.scale"];

    animation.toValue = [NSNumber numberWithFloat:scale];
    animation.fillMode = kCAFillModeForwards;  // This line will stop the animation at the end position of animation`enter code here`
    animation.autoreverses = NO;
    animation.removedOnCompletion = NO;
    animation.cumulative = YES;
    
    self.layer.rasterizationScale = 2.0;
    self.layer.contentsScale = 2.0;
    
    CGPoint newPosition = self.frame.origin;
    newPosition.x += offset.x;
    newPosition.y += offset.y;
    
    CABasicAnimation *positionAnimation = [CABasicAnimation animationWithKeyPath:@"transform.translation"];
    positionAnimation.fromValue = [[self.layer presentationLayer] valueForKeyPath:@"transform.translation"];
    positionAnimation.toValue = [NSValue valueWithCGPoint:newPosition];
    positionAnimation.duration = duration;
    positionAnimation.fillMode = kCAFillModeForwards;
    positionAnimation.removedOnCompletion = NO;
    positionAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    self.layer.rasterizationScale = 2.0;

    
    CAAnimationGroup* group = [CAAnimationGroup animation];
    group.duration = duration;
    group.animations = @[animation, positionAnimation];
    group.fillMode = kCAFillModeForwards;
    group.removedOnCompletion = NO;
    group.delegate = self;

    [self.layer addAnimation:group forKey:@"scaleAndMove"];
    
    [CATransaction commit];
    
}

- (void)animateToScale:(float)scale  duration:(NSTimeInterval)duration complete:(void (^)())complete {
    
    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithFloat:duration] forKey:kCATransactionAnimationDuration];
    [CATransaction setCompletionBlock:^{
        if (complete) complete();
    }];
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    
    animation.duration = duration;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.fromValue = [[self.layer presentationLayer] valueForKeyPath:@"transform.scale"];
    
    animation.toValue = [NSNumber numberWithFloat:scale];
    animation.fillMode = kCAFillModeForwards;  // This line will stop the animation at the end position of animation`enter code here`
    animation.autoreverses = NO;
    animation.removedOnCompletion = NO;
    animation.cumulative = YES;
    
    self.layer.rasterizationScale = 2.0;
    self.layer.contentsScale = 2.0;
    
    CAAnimationGroup* group = [CAAnimationGroup animation];
    group.duration = duration;
    group.animations = @[animation];
    group.fillMode = kCAFillModeForwards;
    group.removedOnCompletion = NO;
    group.delegate = self;
    
    [self.layer addAnimation:group forKey:@"scaleAndMove"];
    
    [CATransaction commit];
}

- (void)animateToScale:(float)scale andMoveToPosition:(CGPoint)offset duration:(NSTimeInterval)duration complete:(void (^)())complete {
    
    [CATransaction begin];
    [CATransaction setValue:[NSNumber numberWithFloat:duration] forKey:kCATransactionAnimationDuration];
    [CATransaction setCompletionBlock:^{
        if (complete) complete();
    }];
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    
    animation.duration = duration;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.fromValue = [[self.layer presentationLayer] valueForKeyPath:@"transform.scale"];
    
    animation.toValue = [NSNumber numberWithFloat:scale];
    animation.fillMode = kCAFillModeForwards;  // This line will stop the animation at the end position of animation`enter code here`
    animation.autoreverses = NO;
    animation.removedOnCompletion = NO;
    animation.cumulative = YES;
    
    self.layer.rasterizationScale = 2.0;
    self.layer.contentsScale = 2.0;
    
    CGPoint newPosition = self.frame.origin;
    newPosition.x += offset.x;
    newPosition.y += offset.y;
    
    CABasicAnimation *positionAnimation = [CABasicAnimation animationWithKeyPath:@"position"];
    positionAnimation.fromValue = [[self.layer presentationLayer] valueForKeyPath:@"position"];
    positionAnimation.toValue = [NSValue valueWithCGPoint:newPosition];
    positionAnimation.duration = duration;
    positionAnimation.fillMode = kCAFillModeForwards;
    positionAnimation.removedOnCompletion = NO;
    positionAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    self.layer.rasterizationScale = 2.0;
    
    CAAnimationGroup* group = [CAAnimationGroup animation];
    group.duration = duration;
    group.animations = @[animation, positionAnimation];
    group.fillMode = kCAFillModeForwards;
    group.removedOnCompletion = NO;
    group.delegate = self;
    
    [self.layer addAnimation:group forKey:@"scaleAndMove"];
    
    [CATransaction commit];
    
}


- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {

    
}

@end
