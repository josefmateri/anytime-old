//
//  ATStopWatch.h
//  Anytime 2
//
//  Created by Josef Materi on 20.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ATStopWatchDelegate;

@interface ATStopWatch : NSObject

@property (nonatomic, weak) id <ATStopWatchDelegate> delegate;


- (void)start;
- (void)stop;

- (void)setStartTime:(NSDate *)startTime;

- (void)startWithDate:(NSDate *)date andRunningTime:(NSTimeInterval)runningTime;

- (void)resume;

- (void)setRunningTime:(NSTimeInterval)runningTimer;

@end

@protocol ATStopWatchDelegate <NSObject>

- (void)stopWatch:(ATStopWatch *)stopWatch didCountToRunningSeconds:(NSInteger)runningSeconds totalSeconds:(NSInteger)totalSeconds;

@end
