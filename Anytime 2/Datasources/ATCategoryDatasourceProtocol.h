//
//  ATCategoryDatasourceProtocol.h
//  anytime
//
//  Created by Josef Materi on 19.10.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ATCategoryConfiguration.h"

@protocol ATCategoryDatasourceProtocol <NSObject>

- (NSFetchRequest *)fetchRequestForMainCategory;

- (NSFetchRequest *)fetchRequestForSecondaryCategory;

- (ATCategoryConfiguration *)configurationForMainCategory;

- (ATCategoryConfiguration *)configurationForSecondaryCategory;

- (void)getMainCategoryObjectsComplete:(void (^)(NSArray *results))completeBlock failed:(void (^)(NSError *failed))failedBlock;

- (void)getSecondaryCategoryObjectsComplete:(void (^)(NSArray *results))completeBlock failed:(void (^)(NSError *failed))failedBlock;

@end
