//
//  JMBackgroundView.m
//  Anytime
//
//  Created by Josef Materi on 19.09.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import "JMBackgroundView.h"

@implementation JMBackgroundView

- (id)init {
    self = [super init];
    if (self) {
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _radiiCorners = UIRectCornerAllCorners;
        _shadowBlur = 3;
        _shadowOffset = CGSizeMake(0, 1);
        _cornerRadius = 0;
        _shadowColor = [UIColor colorWithWhite:0 alpha:1];
        _strokeColor = [UIColor clearColor];
        _innerStrokeColor = [UIColor clearColor];
        _innerStrokeWidth = 2;
        self.contentMode = UIViewContentModeBottom;
        self.opaque = NO;
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    CGContextRef context = UIGraphicsGetCurrentContext();
    [[UIColor clearColor] setFill];
    CGContextFillRect(context, rect);
    
  
    CGContextAddPath(context,  [UIBezierPath bezierPathWithRoundedRect:CGRectInset(rect, 0, 0) byRoundingCorners:_radiiCorners cornerRadii:CGSizeMake(_cornerRadius, _cornerRadius)].CGPath);
   
    CGContextClip(context);
    
    [_fillColor setFill];

    CGContextFillRect(context, rect);

    [self drawLinearGradientWithContext:context inRect:rect startColor:_gradientStartColor.CGColor endColor:_gradientEndColor.CGColor];
    
    if (_backgroundImage && !_backgroundImageHidden) {
        if (self.contentMode == UIViewContentModeScaleToFill)
        [_backgroundImage drawInRect:rect];
        else if (self.contentMode == UIViewContentModeBottomLeft) {
            [_backgroundImage drawAtPoint:CGPointMake(0, -fabs(rect.size.height - _backgroundImage.size.height))];
        } else if (self.contentMode == UIViewContentModeBottomRight) {
            [_backgroundImage drawAtPoint:CGPointMake(- fabs(rect.size.width - _backgroundImage.size.width), -fabs(rect.size.height - _backgroundImage.size.height))];
        } else if (self.contentMode == UIViewContentModeBottom) {
            [_backgroundImage drawAtPoint:CGPointMake(rect.size.width - _backgroundImage.size.width, -fabs(rect.size.height - _backgroundImage.size.height))];
        }  else if (self.contentMode == UIViewContentModeTop) {
            [_backgroundImage drawAtPoint:CGPointMake((rect.size.width - _backgroundImage.size.width), 0)];
        }
    }
       
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectInset(rect, .5, .5) byRoundingCorners:_radiiCorners cornerRadii:CGSizeMake(_cornerRadius, _cornerRadius)];
    CGContextAddPath(context, path.CGPath);
    
    
    CGContextSaveGState(context);
    [_strokeColor set];
    
    CGContextSetShadowWithColor(context, _shadowOffset, _shadowBlur, _shadowColor.CGColor);
    
    CGContextStrokePath(context);
    
    CGContextRestoreGState(context);
//    

    CGContextAddPath(context,  [UIBezierPath bezierPathWithRoundedRect:CGRectInset(rect, 1.5, 1.5) byRoundingCorners:_radiiCorners cornerRadii:CGSizeMake(_cornerRadius - 1, _cornerRadius - 1)].CGPath);
//    
    [_innerStrokeColor set];

    CGContextStrokePath(context);

//    CGContextAddPath(context,  [UIBezierPath bezierPathWithRoundedRect:CGRectInset(rect, 1.5,  1.5) byRoundingCorners:_radiiCorners cornerRadii:CGSizeMake(_cornerRadius - 1, _cornerRadius - 1)].CGPath);
//    


    //CGContextStrokePath(context);
}


#pragma mark - Helpers

- (void)drawLinearGradientWithContext:(CGContextRef)context inRect:(CGRect)rect startColor:(CGColorRef)startColor endColor:(CGColorRef)endColor {
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGFloat locations[] = { 0.0, 1.0 };
    
    NSArray *colors = [NSArray arrayWithObjects:(__bridge id)startColor, (__bridge id)endColor, nil];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace,
                                                        (__bridge CFArrayRef) colors, locations);
    
    CGPoint startPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMinY(rect));
    CGPoint endPoint = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    CGContextSaveGState(context);
    CGContextAddRect(context, rect);
    CGContextClip(context);
    
    CGContextDrawLinearGradient(context, gradient, startPoint, endPoint, 0);
    CGContextRestoreGState(context);
    
    CGGradientRelease(gradient);
    CGColorSpaceRelease(colorSpace);
}

@end
