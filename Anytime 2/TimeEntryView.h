//
//  TimeEntryView.h
//  Anytime
//
//  Created by Josef Materi on 21.10.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TimeEntryViewDelegate;

@interface TimeEntryView : UIView

@property (nonatomic, assign) id <TimeEntryViewDelegate> delegate;

@property (nonatomic, strong) UITapGestureRecognizer *tapGestureRecognizer;
@property (nonatomic, assign) NSInteger selectedDigitIndex;
@property (nonatomic, readonly, strong) UILabel *titleLabel;

- (void)setTitle:(NSString *)title;
- (void)setTitleAlignment:(NSTextAlignment)alignment;
- (void)setTime:(NSString *)time;

- (void)setCurrentDigitAndCheckValidity:(NSInteger)digit ;
- (void)selectDigitAtIndex:(NSInteger)index ;
- (void)setDigit:(NSInteger)digit atIndex:(NSInteger)index;
- (void)setCurrentDigit:(NSInteger)digit;
- (void)goToNextDigit;
- (void)goToPreviousDigit;
- (void)stopBlinking;
- (BOOL)validDigits;
- (void)didFinishEditing;
- (void)setDigitsAlignment:(NSTextAlignment)alignment;
- (void)goToNextDigitStopAtLast:(BOOL)stop;
@end

@protocol TimeEntryViewDelegate <NSObject>

- (BOOL)timEntryViewShouldEndEditing:(TimeEntryView *)TimeEntryView;
- (void)timeEntryViewDidStartEditing:(TimeEntryView *)timeEntryView;
- (void)timeEntryViewDidFinishEditing:(TimeEntryView *)timeEntryView;

@end