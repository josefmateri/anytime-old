//
//  TimeEntryViewContainer.m
//  Anytime
//
//  Created by Josef Materi on 21.10.12.
//  Copyright (c) 2012 Josef Materi. All rights reserved.
//

#import "TimeEntryViewContainer.h"
#import "UIView+FLKAutoLayout.h"

@interface TimeEntryViewContainer()
@property (nonatomic, strong) TimeEntryView *fromTimeEntryView;
@property (nonatomic, strong) TimeEntryView *toTimeEntryView;
@property (nonatomic, strong) TimeEntryView *pauseTimeEntryView;

@property (nonatomic, strong) UILabel *netValueLabel;
@property (nonatomic, strong) UILabel *pauseValueLabel;

@property (nonatomic, strong) JMKeyboardViewController *keyboardViewController;
@property (nonatomic, strong) NSLayoutConstraint *keyboardBottomPositionConstraint;
@property (nonatomic, strong) NSLayoutConstraint *timeEntryHeightConstraint;
@property (nonatomic, assign, getter = isEditing) BOOL editing;
@property (nonatomic, weak) TimeEntryView *activeTimeEntryView;
@property (nonatomic, strong) UIView *bottomBorder;
@end

@implementation TimeEntryViewContainer

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        self.backgroundColor = [UIColor clearColor];
             
        _fromTimeEntryView = [[TimeEntryView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        _fromTimeEntryView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_fromTimeEntryView];
        _fromTimeEntryView.delegate = self;
        [_fromTimeEntryView setTitleAlignment:NSTextAlignmentRight];

        _toTimeEntryView = [[TimeEntryView alloc] initWithFrame:CGRectMake(140, 0, 100, 100)];
        _toTimeEntryView.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:_toTimeEntryView];
        _toTimeEntryView.delegate = self,
        
        [_fromTimeEntryView setTitle:NSLocalizedString(@"FROM", nil)];
        [_toTimeEntryView setTitle:NSLocalizedString(@"TO", nil)];
        [_toTimeEntryView setTitleAlignment:NSTextAlignmentLeft];
        [_toTimeEntryView setDigitsAlignment:NSTextAlignmentRight];

        NSArray *horizontalLayout = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(10)-[_fromTimeEntryView]-(0)-[_toTimeEntryView(==_fromTimeEntryView)]-(10)-|" options:NSLayoutFormatAlignAllBaseline metrics:nil views:NSDictionaryOfVariableBindings(_fromTimeEntryView, _toTimeEntryView)];
        [self addConstraints:horizontalLayout];
        
        
        _timeEntryHeightConstraint = [NSLayoutConstraint constraintWithItem:_fromTimeEntryView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:80];
        [self addConstraint:_timeEntryHeightConstraint];
        
        
        NSArray *verticalLayout = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(0)-[_fromTimeEntryView]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_fromTimeEntryView)];
        [self addConstraints:verticalLayout];
        
        NSArray *heightConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_toTimeEntryView(==_fromTimeEntryView)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_fromTimeEntryView, _toTimeEntryView)];
        [self addConstraints:heightConstraint];

        
        UICollectionViewFlowLayout *flowLayout =[[UICollectionViewFlowLayout alloc] init];
        
        
        flowLayout.itemSize = CGSizeMake(self.bounds.size.width / 3.5, 44);
        flowLayout.sectionInset = UIEdgeInsetsMake(0, 10, 0, 10);
        flowLayout.minimumInteritemSpacing = 0.0f;
        flowLayout.minimumLineSpacing = 5.0f;

        
        _keyboardViewController = [[JMKeyboardViewController alloc] initWithCollectionViewLayout:flowLayout];
		_keyboardViewController.delegate = self;
		_keyboardViewController.datasource = self;
        
		_keyboardViewController.collectionView.alpha = 0;
        
		[self addSubview:_keyboardViewController.view];
        
		self.clipsToBounds = YES;
        

        
		_keyboardBottomPositionConstraint = [NSLayoutConstraint constraintWithItem:self.keyboardViewController.view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:270];
		_keyboardBottomPositionConstraint.priority = 100;
        
		[self addConstraint:_keyboardBottomPositionConstraint];

        
        
        
        
//        self.pauseTimeEntryView = [[TimeEntryView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
//        self.pauseTimeEntryView.translatesAutoresizingMaskIntoConstraints = NO;
//        self.pauseTimeEntryView.titleLabel.text = @"PAUSE";
//    //    [self addSubview:self.pauseTimeEntryView];
//
//        [self addConstraints:[self.pauseTimeEntryView constrainHeight:@"80"]];
//        [self addConstraints:[self.pauseTimeEntryView constrainWidth:@"100"]];
//
//        NSLayoutConstraint *pauseTopConstraint = [NSLayoutConstraint constraintWithItem:self.pauseTimeEntryView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:180];
//        [self addConstraint:pauseTopConstraint];
//        
//        NSLayoutConstraint *pauseCenterXConstraint = [NSLayoutConstraint constraintWithItem:self.pauseTimeEntryView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1 constant:0];
//        [self addConstraint:pauseCenterXConstraint];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1)];
        line.backgroundColor = [UIColor colorWithRed:0.376 green:0.412 blue:0.447 alpha:1.000];
        [self addSubview:line];
        self.bottomBorder = line;
        
        
        UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, 1)];
        topLine.backgroundColor = [UIColor colorWithRed:0.376 green:0.412 blue:0.447 alpha:1.000];
        [self addSubview:topLine];
        
        [self layoutIfNeeded];
        
    }
    return self;
}

- (void)setBottomBorderColor:(UIColor *)bottomBorderColor {
    self.bottomBorder.backgroundColor = bottomBorderColor;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)stopEditingWithCompletionHandler:(void (^)())completionHandler {
    
    _keyboardBottomPositionConstraint.constant = 270;

    _timeEntryHeightConstraint.constant = 80;
    
    [_activeTimeEntryView stopBlinking];
    
    _toTimeEntryView.alpha = 1;
    _fromTimeEntryView.alpha = 1;

    self.editing = NO;

    _activeTimeEntryView = nil;
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{

        [self layoutIfNeeded];
    
    } completion:^(BOOL finished) {
        
        completionHandler();

    }];
    
}

- (void)startEditingWithCompletionHandler:(void (^)())completionHandler {
 
    _keyboardBottomPositionConstraint.constant = 0;
//    _timeEntryHeightConstraint.constant = 130;
    
    _keyboardViewController.collectionView.layer.shouldRasterize = YES;
    _keyboardViewController.collectionView.layer.rasterizationScale = 2.0;
    
    [UIView animateWithDuration:1 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{
        _keyboardViewController.collectionView.alpha = 1;
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {

        _keyboardViewController.collectionView.layer.shouldRasterize = NO;
        
        self.editing = YES;
        completionHandler();
    }];
}

#pragma mark -
#pragma mark TimeEntryView

- (void)timeEntryViewDidStartEditing:(TimeEntryView *)timeEntryView {
   
    //_timeEntryHeightConstraint.constant = 120;
    
//    [UIView animateWithDuration:0.3 animations:^{
//        [self layoutIfNeeded];
//    }];
    
    if ([timeEntryView isEqual:_fromTimeEntryView]) {
        if ([_toTimeEntryView validDigits]) {
            [self tappedOnFrom];
        }
    } else {
        if ([_fromTimeEntryView validDigits]) {
            [self tappedOnTo];
        }
    }
    
    
}

- (void)timeEntryViewDidFinishEditing:(TimeEntryView *)timeEntryView {
    
    if ([timeEntryView isEqual:_fromTimeEntryView]) {
        if (timeEntryView.selectedDigitIndex == 3)
        [self tappedOnTo];
    }
    
    if ([timeEntryView isEqual:_toTimeEntryView]) {
        if (timeEntryView.selectedDigitIndex == 0)
            [self tappedOnFromSelectDigitAtIndex:3];
    }
}

- (BOOL)timEntryViewShouldEndEditing:(TimeEntryView *)timeEntryView {
    if (![timeEntryView validDigits]) {
        return NO;
    }
    
    if ([timeEntryView isEqual:_toTimeEntryView]) {
        if (_toTimeEntryView.selectedDigitIndex == 0) {
            return YES;
        } else {
            return NO;
        }
    } else  if ([timeEntryView isEqual:_fromTimeEntryView]) {
        if (_fromTimeEntryView.selectedDigitIndex == 0) {
            return NO;
        } else {
            return YES;
        }
    }
    return YES;
}

#pragma mark - From
- (void)tappedOnFromSelectDigitAtIndex:(NSInteger)index {
    
    [_activeTimeEntryView stopBlinking];
    
    
    if ([_activeTimeEntryView isEqual:_fromTimeEntryView]) {
        [_fromTimeEntryView goToNextDigitStopAtLast:YES];
    } else {
        [_fromTimeEntryView selectDigitAtIndex:0];
    }

    _activeTimeEntryView = _fromTimeEntryView;

    
    [UIView animateWithDuration:0.2 animations:^{
        _toTimeEntryView.alpha = 0.5;
        _fromTimeEntryView.alpha = 1;
    }];
    
        if ([_delegate respondsToSelector:@selector(timeEntryViewContainerDidTapOnFrom:)]) {
            [_delegate timeEntryViewContainerDidTapOnFrom:self];
        }
    
    [self reloadKeys];

}

- (void)tappedOnFrom {
    
    [self tappedOnFromSelectDigitAtIndex:0];
}

#pragma mark - To

- (void)tappedOnTo {
    
    
    [_activeTimeEntryView stopBlinking];
    
    if ([_activeTimeEntryView isEqual:_toTimeEntryView]) {
        [_toTimeEntryView goToNextDigitStopAtLast:YES];
    } else {
        [_toTimeEntryView selectDigitAtIndex:0]; 
    }

    _activeTimeEntryView = _toTimeEntryView;
    
    
    
    [UIView animateWithDuration:0.2 animations:^{
        _fromTimeEntryView.alpha = 0.5;
        _toTimeEntryView.alpha = 1;
    }];
    
    if ([_delegate respondsToSelector:@selector(timeEntryViewContainerDidTapOnTo:)]) {
        [_delegate timeEntryViewContainerDidTapOnTo:self];
    }
    
    [self reloadKeys];
    
}

#pragma mark - JMKeyboardViewController



- (void)keyboardViewController:(JMKeyboardViewController *)kvc configureItem:(JMKeyboardViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
	
    cell.textLabel.textColor = [UIColor colorWithWhite:0.75 alpha:1];
    
//    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15];
    
    if (_keyboardMode == TimeEntryKeyboardModeDigits) {
        if(indexPath.row == 11) {
            cell.imageView.image = [UIImage imageNamed:@"keyboardArrowLeft"];
        } else if(indexPath.row == 12) {
            cell.textLabel.text = NSLocalizedString(@"Enter", nil);
        } else if (indexPath.row == 9){
            cell.textLabel.textColor = [UIColor colorWithWhite:1 alpha:1];
            cell.textLabel.text = @"...";
        } else {
            cell.textLabel.text = [@ ([self digitForIndexPath:indexPath])stringValue];
        }
        cell.textLabel.textColor = [UIColor colorWithWhite:1 alpha:1];

    } else {
        if (indexPath.row == 9) {
            cell.textLabel.textColor = [UIColor colorWithWhite:0.5 alpha:1];

            cell.textLabel.text = @"0..9";
        } else {
            cell.textLabel.text = [self timeForRowAtIndexPath:indexPath];
        }
    }
}



- (void)keyboardViewController:(JMKeyboardViewController *)kvc didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
	

    if (indexPath.row == 9) {
        [self toggleKeyboardMode];
    } else {
        
        if (_keyboardMode == TimeEntryKeyboardModeDigits) {
            if(indexPath.row == 11) {
                
                [self setCurrentDigit:0];
                [self goToPreviousDigit];
                
            } else if(indexPath.row == 12) {
                
            } else {
                [self setCurrentDigitAndCheckValidity:[self digitForIndexPath:indexPath]];
            }
        } else {
            [_activeTimeEntryView setTime:[self timeForRowAtIndexPath:indexPath]];
            if ([_activeTimeEntryView isEqual:_fromTimeEntryView]) {
                [self tappedOnTo];
            } else {
                [_toTimeEntryView selectDigitAtIndex:3];
            }
        }
    }
}


- (NSString *)timeForRowAtIndexPath:(NSIndexPath *)indexPath {
    int start = 7;
    
    if ([_activeTimeEntryView isEqual:_fromTimeEntryView]) {
        start = 7;
    } else {
        start = 15;
    }
    
    return [NSString stringWithFormat:@"%2d:00", start + indexPath.row];
    return @"09:30";
}

- (void)setCurrentDigit:(NSInteger)currentDigit {
    [_activeTimeEntryView setCurrentDigit:currentDigit];
}

- (void)goToPreviousDigit {
    [_activeTimeEntryView goToPreviousDigit];
}

- (void)setCurrentDigitAndCheckValidity:(NSInteger)digit {
    [_activeTimeEntryView setCurrentDigitAndCheckValidity:digit];
}

- (NSInteger)digitForIndexPath:(NSIndexPath *)indexPath {
	if(indexPath.row < 9) {
		return indexPath.row + 1;
	}
	if(indexPath.row == 10) {
		return 0;
	}
	return 0;
}

- (void)toggleKeyboardMode {
    if (_keyboardMode == TimeEntryKeyboardModeDigits) {
        _keyboardMode = TimeEntryKeyboardModeAssist;
    } else {
        _keyboardMode = TimeEntryKeyboardModeDigits;
    }
    
    [_keyboardViewController.collectionView reloadData];
}

- (void)reloadKeys {
    [_keyboardViewController.collectionView reloadData];
}

@end
