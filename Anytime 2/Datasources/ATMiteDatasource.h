//
//  ATMiteObjectManager.h
//  anytime
//
//  Created by Josef Materi on 12.09.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <RestKit.h>
#import "RKObjectManager.h"
#import "ATTimeEntryProtocol.h"
#import "ATDatasourceProtocol.h"
#import "ATCategoryDatasourceProtocol.h"
#import "ATTimeEntriesDatasourceProtocol.h"

@interface ATMiteDatasource : RKObjectManager <ATDatasourceProtocol, ATCategoryDatasourceProtocol, ATTimeEntriesDatasourceProtocol>

+ (instancetype)sharedDatasource;


@end
