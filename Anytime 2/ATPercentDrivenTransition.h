//
//  ATPercentDrivenTransition.h
//  Anytime 2
//
//  Created by Josef Materi on 14.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ATPercentDrivenTransition : UIPercentDrivenInteractiveTransition
@property (nonatomic, weak) UINavigationController *navigationController;
@property (nonatomic, weak) UIViewController *presentedViewController;
- (instancetype)initWithNavigationController:(UINavigationController *)navigationController;

@end
