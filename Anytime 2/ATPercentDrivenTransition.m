//
//  ATPercentDrivenTransition.m
//  Anytime 2
//
//  Created by Josef Materi on 14.07.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATPercentDrivenTransition.h"
#import "UIView+Position.h"

@implementation ATPercentDrivenTransition


- (instancetype)initWithNavigationController:(UINavigationController *)navigationController {
    self = [super init];
    
    if (self) {
        self.navigationController = navigationController;
        
    }
    
    return self;
}


- (CGFloat)completionSpeed {
    return 3;
}
@end
