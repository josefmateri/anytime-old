//
//  NSURL+Query.h
//  anytime
//
//  Created by Josef Materi on 14.09.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Query)

- (NSDictionary *)queryDictionary;


@end
