//
//  ATWeekCollectionViewController.m
//  Anytime 2
//
//  Created by Josef Materi on 23.06.13.
//  Copyright (c) 2013 Josef Materi. All rights reserved.
//

#import "ATWeekCollectionViewController.h"
#import "ATCalendarDayCell.h"
#import "NSDate+Additions.h"

@interface ATWeekCollectionViewController ()
@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonatomic, strong) NSDateComponents *dateComponentsForSelectedDay;

@end

@implementation ATWeekCollectionViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.collectionView registerClass:[ATCalendarDayCell class] forCellWithReuseIdentifier:@"Cell"];
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    self.collectionView.scrollEnabled = NO;
    
    
    [self.collectionView selectItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0] animated:NO scrollPosition:UICollectionViewScrollPositionLeft];

    [self.view layoutIfNeeded];
    
    self.firstDayOfTheWeek = [[NSDate date] firstDayOfTheWeek];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)reloadData {
    [self.collectionView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    
}

- (void)viewDidAppear:(BOOL)animated {
    
}

- (void)setFirstDayOfTheWeek:(NSDate *)date {
    
    if (!date) {
        return;
    }
    
    _firstDayOfTheWeek = date;
    
	NSCalendar *calendar = [NSCalendar currentCalendar];
	
    self.dateComponents = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekday | NSCalendarUnitDay | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear fromDate:date];
}

- (void) updateWithDateComponents:(NSDateComponents *)component {

    if (self.dateComponents.weekOfYear == component.weekOfYear) return;
    
    self.dateComponents = component;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *date = [calendar dateFromComponents:component];
    
    self.firstDayOfTheWeek = [date firstDayOfTheWeek];
    
    [self reloadData];
}

- (void)selectIndexPath:(NSIndexPath *)indexPath {
    self.selectedIndexPath = indexPath;
    self.dateComponentsForSelectedDay = [self dateComponentsForIndexPath:self.selectedIndexPath];
    [self.collectionView selectItemAtIndexPath:self.selectedIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionLeft];
    [self updateCells];
}

- (void)selectDay:(NSInteger)day animated:(BOOL)animated {
    
    self.selectedIndexPath  = [self indexPathForDay:day];
    self.dateComponentsForSelectedDay = [self dateComponentsForIndexPath:self.selectedIndexPath];
    
    [self.collectionView selectItemAtIndexPath:self.selectedIndexPath animated:animated scrollPosition:UICollectionViewScrollPositionLeft];
   
    [self updateCells];
}

- (void)deselectDay:(NSInteger)day animated:(BOOL)animated {
    [self.collectionView deselectItemAtIndexPath:[self indexPathForDay:day] animated:animated];
}

#pragma mark - UICollectionViewLayouts

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ATCalendarDayCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor clearColor];
        
    NSInteger thisDay =      [[[self.firstDayOfTheWeek firstDayOfTheWeek] dateByAddingDays:indexPath.row] day];
 
    [self updateCell:cell withIndexPath:indexPath];
    
    cell.dayLabel.text = [@(thisDay) description];
    return cell;
}

- (void)updateCells {
    
    for (int i = 0; i < 7; i++) {
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
     
        ATCalendarDayCell *cell = (ATCalendarDayCell *)[self.collectionView cellForItemAtIndexPath:indexPath];
        [self updateCell:cell withIndexPath:indexPath];
        
    }
    
}

- (void)updateCell:(ATCalendarDayCell *)cell withIndexPath:(NSIndexPath *)indexPath {

    NSDateComponents *comp = [self dateComponentsForIndexPath:indexPath];
    
    if (comp.month != self.dateComponentsForSelectedDay.month) {
        
        [cell setDimmed:YES];
    
    } else {
    
        [cell setDimmed:NO];
    
    }

}



- (NSIndexPath *)indexPathForDay:(NSInteger)day {
    
    NSDate *date = [self.firstDayOfTheWeek firstDayOfTheWeek];
    
    NSInteger firstDay = [[self.firstDayOfTheWeek firstDayOfTheWeek] day];

    NSInteger numberOfDays = [self collectionView:self.collectionView numberOfItemsInSection:0];
    
    NSIndexPath *indexPath = nil;

    if (firstDay == day) {
            indexPath = [NSIndexPath indexPathForItem:0 inSection:0];
        return indexPath;
    }
    
    for (int i = 0; i < numberOfDays; i++) {
        NSInteger nextDay = [[date dateByAddingDays:i] day];
        if (day == nextDay) {
            indexPath = [NSIndexPath indexPathForItem:i inSection:0];
        }
    }
    
    return indexPath;
}


- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 7;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    __strong id <ATWeekCollectionViewControllerDelegate> delegate = self.delegate;
    
    
    
    NSDateComponents *selected = [self dateComponentsForIndexPath:indexPath];
    
    self.dateComponentsForSelectedDay = selected;
    self.selectedIndexPath = indexPath;
    [self updateCells];
    
    if ([delegate respondsToSelector:@selector(weekCollectionViewController:didSelectDateWithDateComponents:)]) {
        [delegate weekCollectionViewController:self didSelectDateWithDateComponents:selected];
    }
    
    
}

- (NSDateComponents *)dateComponentsForIndexPath:(NSIndexPath *)indexPath {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDate *firstDay = [self.firstDayOfTheWeek firstDayOfTheWeek];
    NSDateComponents *components = [[NSDateComponents alloc] init];
    components.day = indexPath.row;
    
    NSDate  *newDate = [calendar dateByAddingComponents:components toDate:firstDay options:0];
    
    NSDateComponents *selected = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitWeekday | NSCalendarUnitDay | NSCalendarUnitWeekOfMonth | NSCalendarUnitWeekOfYear fromDate:newDate];

    return selected;
}

- (NSDateComponents *)dateComponentsForSelectedIndexPath {
    return [self dateComponentsForIndexPath:[[self.collectionView indexPathsForSelectedItems] firstObject]];
}

@end
